package com.ophwc.gemini;

import android.content.Context;
import android.content.SharedPreferences;

import com.quickblox.sample.groupchatwebrtc.utils.Consts;

public class OPHWCSharedPrefs {
    public String getUserMobile;
    private SharedPreferences mPreference;
    private Context mContext;
    private static OPHWCSharedPrefs StratfitPreference;
    private static final String PREF_EMAIL = "userEmail";
    private static final String PREF_NAME = "userNamePref";
    private static final String PREF_FULL_NAME = "userFullNamePref";
    private static final String PREF_NO = "userNo";
    private static final String PREF_DIVID = "divIdPref";
    private static final String PREF_LOGIN_OFFLINE = "loginOffline";


    private OPHWCSharedPrefs(Context context) {
        mContext = context;
        mPreference = mContext.getSharedPreferences("Preferences",
                Context.MODE_PRIVATE);
    }

    public static OPHWCSharedPrefs getInstance(Context context) {
        if (StratfitPreference == null) {
            StratfitPreference = new OPHWCSharedPrefs(context);
        }
        return StratfitPreference;
    }
    public void putUserId(int userId) {
        mPreference.edit().putInt(Consts.PREF_LOGIN_USERID, userId).commit();
    }

    public int getUserId() {
        return mPreference.getInt(Consts.PREF_LOGIN_USERID, 0);
    }

        public void putDivisionId(int divId) {
        mPreference.edit().putInt(PREF_DIVID, divId).commit();
    }

    public int getDivisionId() {
        return mPreference.getInt(PREF_DIVID, 0);
    }

    public void putLoginOffline(int offId) {
        mPreference.edit().putInt(PREF_LOGIN_OFFLINE, offId).commit();
    }

    public int getLoginOffline() {
        return mPreference.getInt(PREF_LOGIN_OFFLINE, -1);
    }
    public void putUserEmail(String userEmail) {
        mPreference.edit().putString(PREF_EMAIL, userEmail).commit();
    }

    public String getUserEmail() {
        return mPreference.getString(PREF_EMAIL,"");
    }
    public void putUserName(String name) {
        mPreference.edit().putString(PREF_NAME, name).commit();
    }

    public String getUserName() {
        return mPreference.getString(PREF_NAME,"");
    }
    public void putMobileNo(String no) {
        mPreference.edit().putString(PREF_NO, no).commit();
    }

    public String getMobileNo() {
        return mPreference.getString(PREF_NO,"");
    }
    public String getFullName() {
        return mPreference.getString(PREF_FULL_NAME,"");
    }


    public void putUserFullName(String firstName) {
        mPreference.edit().putString(PREF_FULL_NAME, firstName).commit();
    }
}
