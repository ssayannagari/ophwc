package com.ophwc.gemini.admin.project;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectDivisionsList;
import com.ophwc.gemini.model.project.createProject.CreateProjectsModel;
import com.ophwc.gemini.model.project.createProject.UpdateProjectItem;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.UserTypes;
import com.ophwc.gemini.projects.LocationTaggingActivity;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManageProjectAdapter extends BaseAdapter {
        Context mcontext;
        String filterText;
        List<PrjectList> mProjectList = new ArrayList<>();
        List<DivisionList> mDivisionLists = new ArrayList<>();
        List<NatureWorkList> mNatureWorkLists = new ArrayList<>();
        List<String> mDivNames = new ArrayList<>();
        List<String> mNatureNames = new ArrayList<>();
    View vi;
        public ManageProjectAdapter(Context baseContext) {
            mcontext = baseContext;
        }

        public ManageProjectAdapter(Context baseContext, List<PrjectList> projectList) {
            mcontext = baseContext;
            mProjectList = projectList;
        }

        public ManageProjectAdapter(Context baseContext, List<PrjectList> projectList, String text, List<DivisionList> divisionLists, List<String> divisionsNames, List<NatureWorkList> natureWorkLists, List<String> natureOfWorkNames) {
            mcontext = baseContext;
            //mProjectList = projectList;
            filterText = text;
            mDivisionLists = divisionLists;
            mNatureWorkLists = natureWorkLists;
            mDivNames = divisionsNames;
            mNatureNames =natureOfWorkNames;
            if(text.equalsIgnoreCase("")){
                mProjectList = projectList;

            }else {
                for (PrjectList pl : projectList) {
                    if (pl.getProjectName().toLowerCase().contains(text.toLowerCase())||pl.getProjectDefination().toLowerCase().contains(text.toLowerCase())) {
                        mProjectList.add(pl);
                    }
                }
            }
        }

        @Override
        public int getCount() {
            return mProjectList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           vi =convertView;

            LayoutInflater inflator =(LayoutInflater.from(mcontext));
            if(convertView==null)
                vi = inflator.inflate(R.layout.projects_list_row, null);

            TextView title = (TextView)vi.findViewById(R.id.listview_item_title); // title
            TextView tvProjectLocation = (TextView)vi.findViewById(R.id.tvProjectLocation); //
            TextView tvAssignedTo = (TextView)vi.findViewById(R.id.tvAssignedTo);
            TextView tvProjectStatus = (TextView)vi.findViewById(R.id.tvProjectStatus);
            tvAssignedTo.setText("Project Def: "+mProjectList.get(position).getProjectDefination());
            //position++;

            title.setText(mProjectList.get(position).getProjectName());
            tvProjectLocation.setText(mProjectList.get(position).getProjectLocation());
            ProjectDivisionsList divisionList = mProjectList.get(position).getDivisions();
            tvProjectStatus.setText("Division : "+mProjectList.get(position).getDivisions().getDivisionName());
            vi.setTag(position);
            vi.setOnClickListener(ManageProjects.onProjClickListener);

            return vi;
        }
 /*       private void showEditProjectDialog(PrjectList prjectList) {

             Dialog dialog = new Dialog(mcontext.getApplicationContext());
            dialog.setContentView(R.layout.add_project_dialog);

            Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddProject);
            final EditText edtProjectName = (EditText) dialog.findViewById(R.id.edtAddProjName);
            final EditText edtProjLoc = (EditText) dialog.findViewById(R.id.edtAddProjLocation);
            final EditText edtProjDef = (EditText) dialog.findViewById(R.id.edtAddProjDesc);
            final Spinner divSpinner = (Spinner) dialog.findViewById(R.id.divCreateProjSpinner);

            edtProjectName.setText(prjectList.getProjectName());
            edtProjLoc.setText(prjectList.getProjectLocation());
            edtProjDef.setText(prjectList.getProjectDefination());

            Spinner natureSpinner = (Spinner) dialog.findViewById(R.id.divCreateNatureProj);
            btnAddUser.setText("Add Project");


            ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                    (mcontext, R.layout.spinner_item,mDivNames );
            // Drop down layout style - list view with radio button
            divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


            // attaching data adapter to spinner
            divSpinner.setAdapter(divNameAdapter);

            ArrayAdapter<String> natureNameAdapter = new ArrayAdapter<String>
                    (mcontext, R.layout.spinner_item, mNatureNames);
            // Drop down layout style - list view with radio button
            natureNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // attaching data adapter to spinner
            natureSpinner.setAdapter(natureNameAdapter);

            long dvId =1;
            dvId =  prjectList.getDivisions().getId();
            for(int i=0;i<mDivisionLists.size();i++){


                if(mDivisionLists.get(i).getId()==dvId){

                    divSpinner.setSelection(i);
                }
            }
            long natureId =1;
            natureId =  prjectList.getNatureOfProject().getId();
            for(int i=0;i<mNatureWorkLists.size();i++){


                if(mNatureWorkLists.get(i).getId()==natureId){

                    natureSpinner.setSelection(i);
                }
            }




            // if button is clicked, close the custom dialog
            btnAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int divCode = -1;
                    long natureCode = -1;
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = mDivisionLists.get(pos).getId();
                    int naturePos =  natureSpinner.getSelectedItemPosition();
                    natureCode = mNatureWorkLists.get(naturePos).getId();
                    String strProjName = !edtProjectName.getText().toString().trim().isEmpty()?edtProjectName.getText().toString():"";
                    String strProjLoc = !edtProjLoc.getText().toString().trim().isEmpty()?edtProjLoc.getText().toString():"";
                    String strProjDef = !edtProjDef.getText().toString().trim().isEmpty()?edtProjDef.getText().toString():"";


                    if(divCode==-1){
                        Toast.makeText(mcontext,"Please Select Division",Toast.LENGTH_SHORT).show();

                        return;

                    }

                    if(natureCode==-1){
                        Toast.makeText(mcontext,"Please Select Nature of work",Toast.LENGTH_SHORT).show();

                        return;

                    }

                    if(strProjName.length()==0){
                        Toast.makeText(mcontext,"Please Enter Project Name",Toast.LENGTH_SHORT).show();

                        return;
                    }


                    callEditProjectSercvice(prjectList.getId(),divCode,natureCode,strProjName,strProjLoc,strProjDef);


                    dialog.dismiss();
                    // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();
        }

        private void callEditProjectSercvice(Integer id, int divCode, long natureCode, String strProjName, String strProjLoc, String strProjDef) {

            ProgressDialog progressDialog = new ProgressDialog(mcontext);
            progressDialog.setMessage("Creating Project...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
            AppApi service = retrofit.create(AppApi.class);
            UserTypes userTypes = new UserTypes();
            userTypes.setId(OPHWCSharedPrefs.getInstance(mcontext).getUserId());
            Divisions divisions = new Divisions();
            divisions.setId(divCode);
            NatureOfProject nop = new NatureOfProject();
            nop.setId(natureCode);
            UpdateProjectItem createProjectItem = new UpdateProjectItem();
            createProjectItem.setCreatedUser(userTypes);
            createProjectItem.setDivisions(divisions);
            createProjectItem.setProjectName(strProjName);
            createProjectItem.setProjectLocation(strProjLoc);
            createProjectItem.setProjectDefination(strProjDef);
            createProjectItem.setNatureOfProject(nop);
            createProjectItem.setId(id);
            createProjectItem.setComments("");



            Call<CreateProjectsModel> call = service.UpdateProject(createProjectItem);
            //mLoginFormView.setVisibility(View.INVISIBLE);
            //showProgress(true);


            call.enqueue(new Callback<CreateProjectsModel>() {

                @Override
                public void onResponse(Call<CreateProjectsModel> call, Response<CreateProjectsModel> response) {
                    int code = response.code();
                    // String rMsg = response.body().getStatus();
                    Gson gson = new Gson();
                    // String json = gson.toJson(response.body());
                    Log.d("usercheck", code+"");
                    Log.d("usercheck", "");
                    switch (code) {
                        // 200 is success and user is registered
                        case 200:
                            int statusCode = response.body().statusCode;
                            if(statusCode==1){
                                Toast.makeText(mcontext,"Project has been updated successfully.",Toast.LENGTH_SHORT).show();

                            }else{
                                Toast.makeText(mcontext,""+response.body().statusMessage,Toast.LENGTH_SHORT).show();

                            }
                            // setAdapters();
                            // String status = response.body().getStatus();
                            if(progressDialog!=null)
                                progressDialog.dismiss();
                            //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();
                            try {
                                (new ManageProjects()).getAllProjectService();
                            }catch (Exception e){

                            }

                            break;
                        case 2:
                            if(progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                            break;
                        // code other than 200
                        default:
                            if(progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                            break;

                    }

                }



                @Override
                public void onFailure(Call<CreateProjectsModel> call, Throwable t) {
                    String tMsg = t.toString();
                    Log.d("check", tMsg);
                    if(progressDialog!=null){
                        progressDialog.dismiss();
                    }

                    Toast.makeText(mcontext,"Unable to create project",Toast.LENGTH_SHORT).show();

                    // mLoginFormView.setVisibility(View.VISIBLE);
                    //showProgress(false);

                }

            });

        }
    */}

