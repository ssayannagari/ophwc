package com.ophwc.gemini.admin.project;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;

import com.ophwc.gemini.model.project.createProject.CreateProjectItem;
import com.ophwc.gemini.model.project.createProject.CreateProjectsModel;
import com.ophwc.gemini.model.project.createProject.UpdateProjectItem;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.user.CreateUserModel;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.UserList;
import com.ophwc.gemini.model.user.UserTypes;
import com.ophwc.gemini.model.user.delete.DeleteUserItem;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.sample.groupchatwebrtc.activities.PermissionsActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManageProjects extends AppCompatActivity{
    List<PrjectList> projectList = new ArrayList<>();
    private static List<PrjectList> UnmuteprojectList = new ArrayList<>();
    ListView projectListView;
    SearchView edtProjSearch ;
    ManageProjectAdapter projectsListAdapter;
    FloatingActionButton addProjectFab;
    List<DivisionList> divisionLists =new ArrayList<>();
    List<UserList> mallUserList = new ArrayList<>();
    List<String> divisionsNames =new ArrayList<>();
    List<String> natureOfWorkNames =new ArrayList<>();
    List<NatureWorkList> natureWorkLists =new ArrayList<>();
    LinearLayout lnrNoUsers;
    static View.OnClickListener onProjClickListener;
    String filterText;
    AlertDialog alertDialog;

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("State","onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("State","onstop()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyBoard();
        Log.e("State","onResume()");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("State","onCreate()");
        setContentView(R.layout.activity_projects_list);

        projectListView = (ListView) findViewById(R.id.projects_list_view);
        edtProjSearch = (SearchView) findViewById(R.id.edtProjSearch);
        addProjectFab = (FloatingActionButton) findViewById(R.id.addProjectFab);
        lnrNoUsers = (LinearLayout) findViewById(R.id.lnrNoProjects);
        addProjectFab.setVisibility(View.VISIBLE);

        edtProjSearch.setQueryHint("Search projects");
        edtProjSearch.setIconifiedByDefault(false);

        edtProjSearch.clearFocus();
        getAllNatureOfWorks();
        getAllDivisionService(2,null);
        hideKeyBoard();
        onProjClickListener = new OnProjectClickListener(ManageProjects.this);
        edtProjSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
               filterText = newText;
                filter(filterText);

                return false;
            }
        });
        addProjectFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //addUser();
                getAllDivisionService(1, null);


            }
        });
     /*   projectListView.(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && addUserFab.getVisibility() == View.VISIBLE) {
                    addUserFab.hide();
                } else if (dy < 0 && addUserFab.getVisibility() != View.VISIBLE) {
                    addUserFab.show();
                }
            }
        });*/

        getAllProjectService();
        Toolbar projectstoolbar = (Toolbar) findViewById(R.id.projectstoolbar);
        setSupportActionBar(projectstoolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Manage Projects");



    /*    projectListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                boolean isLocationTagged =  projectList.get(position).getLangitude()==null||projectList.get(position).getLangitude()==null||
                        projectList.get(position).getLangitude().equals("")||projectList.get(position).getLatitude().equals("");
                if(isLocationTagged) {
                    startActivity(new Intent(ProjectsListActivity.this, LocationTaggingActivity.class)
                            .putExtra("projId", projectList.get(position).getId())
                            .putExtra("divId", getIntent().getStringExtra("divId"))
                            .putExtra("projName", projectList.get(position).getProjectName())
                            .putExtra("proLoc", projectList.get(position).getProjectLocation()));
                }else{
                    startActivity(new Intent(ProjectsListActivity.this, ImagesActivity.class)
                            .putExtra("projId", projectList.get(position).getId())
                            .putExtra("divId", getIntent().getStringExtra("divId"))
                            .putExtra("projName", projectList.get(position).getProjectName())
                            .putExtra("lat", projectList.get(position).getLatitude())
                            .putExtra("lng", projectList.get(position).getLangitude()));
                }
            }
        });*/
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        //if(imm.isActive())
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void getAllProjectService() {
        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();
        String id = "1";
        try{
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){

        }
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(ManageProjects.this);

        int  projUserid = 0;
        projUserid = ophwcSharedPrefs.getUserId();
        projectsItem.setId(1);

        //Call<ProjectsModel> call = service.getAllUserProjects(projectsItem);
        Call<ProjectsModel> call = service.getProjects();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        if(response.body().getStatusCode()==1) {
                            projectList.clear();
                            UnmuteprojectList.clear();
                            if (response.body().getPrjectList() != null) {
                                projectList = response.body().getPrjectList();
                                UnmuteprojectList = response.body().getPrjectList();
                                try {
                                    sortProjectList(projectList);
                                    sortProjectList(UnmuteprojectList);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                setAdapters("");
                            }
                        }else{
                            projectList.clear();
                            UnmuteprojectList.clear();
                            setAdapters("");
                            Toast.makeText(ManageProjects.this,"Unable to get projects",Toast.LENGTH_SHORT).show();

                        }
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(ManageProjects.this,"Unable to get projects",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to get projects",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void sortProjectList(List<PrjectList> projectList) {
        Collections.sort(projectList, new Comparator<PrjectList>() {
            @Override
            public int compare(PrjectList o1, PrjectList o2) {

                return o2.getId().compareTo(o1.getId());
            }


        });
    }

    private void setAdapters(String text) {
        if(projectList.size()>0) {
            projectListView.setVisibility(View.VISIBLE);
            lnrNoUsers.setVisibility(View.GONE);
            projectsListAdapter = new ManageProjectAdapter(getBaseContext(), projectList, text,divisionLists,divisionsNames,natureWorkLists,natureOfWorkNames);
            projectListView.setAdapter(projectsListAdapter);
        }else{
            projectListView.setVisibility(View.GONE);
            lnrNoUsers.setVisibility(View.VISIBLE);

        }
    }



    // Filter Class
    public void filter(String charText) {
        UnmuteprojectList.size();
        //  projectList.clear();
        charText = charText.toLowerCase(Locale.getDefault());
        setAdapters(charText);



        //projectsListAdapter.notifyDataSetChanged();
    }
    private List<String> getDivisionNames() {
        List<String> divName = new ArrayList<>();
        divisionsNames.clear();
        if(divisionLists!=null) {
            for (int i=0; i<divisionLists.size();i++) {
                if(divisionLists.get(i).getDivisionName()!=null) {
                    divisionsNames.add(divisionLists.get(i).getDivisionName());
                }
            }
        }

        return divisionsNames;
    }
  private List<String> getNaturkNames() {
        List<String> divName = new ArrayList<>();
        natureOfWorkNames.clear();
        if(natureWorkLists!=null) {
            for (int i=0; i<natureWorkLists.size();i++) {
                if(natureWorkLists.get(i).getName()!=null) {
                    natureOfWorkNames.add(natureWorkLists.get(i).getName());
                }
            }
        }

        return natureOfWorkNames;
    }

    private void getAllDivisionService(int i,Object obj) {
        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Getting divisions...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<divisionsModel> call = service.getDivisions();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<divisionsModel>() {

            @Override
            public void onResponse(Call<divisionsModel> call, Response<divisionsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                       if( response.body().getDivisionList()!=null) {
                           divisionLists.clear();
                           divisionLists = response.body().getDivisionList();

                           if (divisionLists.size() > 0) {
                               if (i == 1) {
                                   showAddUserDialog(i, getDivisionNames(), getNaturkNames());
                               } else {
                                   divisionsNames = getDivisionNames();
                               }

                           }
                       }else{
                           Toast.makeText(ManageProjects.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                           }

                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<divisionsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void callAddProjectSercvice(int divCode, long natureCode, String strProjName, String strProjLoc, String strProjDef) {
        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Creating Project...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        UserTypes userTypes = new UserTypes();
        userTypes.setId(OPHWCSharedPrefs.getInstance(ManageProjects.this).getUserId());
        Divisions divisions = new Divisions();
        divisions.setId(divCode);
        NatureOfProject nop = new NatureOfProject();
        nop.setId(natureCode);
        CreateProjectItem createProjectItem = new CreateProjectItem();
        createProjectItem.setCreatedUser(userTypes);
        createProjectItem.setDivisions(divisions);
        createProjectItem.setProjectName(strProjName);
        createProjectItem.setProjectLocation(strProjLoc);
        createProjectItem.setProjectDefination(strProjDef);
        createProjectItem.setNatureOfProject(nop);
        createProjectItem.setComments("");
        createProjectItem.setLangitude("");
        createProjectItem.setLangitude("");
        createProjectItem.setStatus("Y");


        Call<CreateProjectsModel> call = service.createProject(createProjectItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<CreateProjectsModel>() {

            @Override
            public void onResponse(Call<CreateProjectsModel> call, Response<CreateProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                       int statusCode = response.body().statusCode;
                       if(statusCode==1){
                           Toast.makeText(ManageProjects.this,"Project has been created successfully.",Toast.LENGTH_SHORT).show();

                       }else{
                           Toast.makeText(ManageProjects.this,""+response.body().statusMessage,Toast.LENGTH_SHORT).show();

                       }
                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();
                        getAllProjectService();

                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<CreateProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to create project",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }

    private void getAllNatureOfWorks() {
        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Getting Nature of projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<NatureOfWorkModel> call = service.getNatureOfWork();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<NatureOfWorkModel>() {

            @Override
            public void onResponse(Call<NatureOfWorkModel> call, Response<NatureOfWorkModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1){
                            natureWorkLists = response.body().getNatureWorkList();

                        }else{
                            Toast.makeText(ManageProjects.this,response.body().getStatusMessage()+"",Toast.LENGTH_SHORT).show();


                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<NatureOfWorkModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to get Nature of work",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

   private class OnProjectClickListener implements View.OnClickListener {
        private final Context context;

        private OnProjectClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            getAllDivisionService(2,null);
            String filter = edtProjSearch.getQuery().toString().toLowerCase();
            int pos = (Integer) v.getTag();
            List<PrjectList> myProjList = new ArrayList<>();
            if (filter.equals("")) {
                myProjList =projectList;
            } else {
                for (int i = 0; i < projectList.size(); i++) {
                    if (projectList.get(i).getProjectName().toLowerCase().contains(filter)) {
                      myProjList.add(projectList.get(i));
                    }
                }
                Log.e("CLICK", "filter " + filter + ", " + pos);
            }
           showEditProjectDialog(myProjList.get(pos));
        }
    }
    private void showAddUserDialog(int i, List<String> divisionNames, List<String> naturkNames) {

        final Dialog dialog = new Dialog(ManageProjects.this);
        dialog.setContentView(R.layout.add_project_dialog);

        Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddProject);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancelProjdlg);
        final TextInputEditText edtProjectName = (TextInputEditText) dialog.findViewById(R.id.edtAddProjName);
        final TextInputEditText edtProjLoc = (TextInputEditText) dialog.findViewById(R.id.edtAddProjLocation);
        final TextInputEditText edtProjDef = (TextInputEditText) dialog.findViewById(R.id.edtAddProjDesc);
        final Spinner divSpinner = (Spinner) dialog.findViewById(R.id.divCreateProjSpinner);

        Spinner natureSpinner = (Spinner) dialog.findViewById(R.id.divCreateNatureProj);
        btnAddUser.setText("Add Project");


        ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, divisionNames);
        // Drop down layout style - list view with radio button
        divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // attaching data adapter to spinner
        divSpinner.setAdapter(divNameAdapter);

        ArrayAdapter<String> natureNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, naturkNames);
        // Drop down layout style - list view with radio button
        natureNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // attaching data adapter to spinner
        natureSpinner.setAdapter(natureNameAdapter);


btnCancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(dialog!=null)
            if(dialog.isShowing())
                dialog.dismiss();
    }
});


        // if button is clicked, close the custom dialog
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int divCode = -1;
                long natureCode = -1;
                int pos =  divSpinner.getSelectedItemPosition();
                divCode = divisionLists.get(pos).getId();
                int naturePos =  natureSpinner.getSelectedItemPosition();
                natureCode = natureWorkLists.get(naturePos).getId();
                String strProjName = !edtProjectName.getText().toString().trim().isEmpty()?edtProjectName.getText().toString():"";
                String strProjLoc = !edtProjLoc.getText().toString().trim().isEmpty()?edtProjLoc.getText().toString():"";
                String strProjDef = !edtProjDef.getText().toString().trim().isEmpty()?edtProjDef.getText().toString():"";


                if(divCode==-1){
                    Toast.makeText(ManageProjects.this,"Please Select Division",Toast.LENGTH_SHORT).show();

                    return;

                }

                if(natureCode==-1){
                    Toast.makeText(ManageProjects.this,"Please Select Nature of work",Toast.LENGTH_SHORT).show();

                    return;

                }

                if(strProjName.length()==0){
                    Toast.makeText(ManageProjects.this,"Please Enter Project Name",Toast.LENGTH_SHORT).show();

                    return;
                }




                callAddProjectSercvice(divCode,natureCode,strProjName,strProjLoc,strProjDef);


                dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void showEditProjectDialog(PrjectList prjectList) {

        Dialog dialog = new Dialog(ManageProjects.this);
        dialog.setContentView(R.layout.add_project_dialog);

        Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddProject);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancelProjdlg);
        Button btnDeleteProj = (Button) dialog.findViewById(R.id.btnDeleteProj);
        btnDeleteProj.setVisibility(View.VISIBLE);

        final TextInputEditText edtProjectName = (TextInputEditText) dialog.findViewById(R.id.edtAddProjName);
        final TextInputEditText edtProjLoc = (TextInputEditText) dialog.findViewById(R.id.edtAddProjLocation);
        final TextInputEditText edtProjDef = (TextInputEditText) dialog.findViewById(R.id.edtAddProjDesc);
        final Spinner divSpinner = (Spinner) dialog.findViewById(R.id.divCreateProjSpinner);

        edtProjectName.setText(prjectList.getProjectName());
        edtProjLoc.setText(prjectList.getProjectLocation());
        edtProjDef.setText(prjectList.getProjectDefination());

        Spinner natureSpinner = (Spinner) dialog.findViewById(R.id.divCreateNatureProj);
        btnAddUser.setText("Update Project");


        ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                (ManageProjects.this, R.layout.spinner_item,divisionsNames );
        // Drop down layout style - list view with radio button
        divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // attaching data adapter to spinner
        divSpinner.setAdapter(divNameAdapter);

        ArrayAdapter<String> natureNameAdapter = new ArrayAdapter<String>
                (ManageProjects.this, R.layout.spinner_item, getNaturkNames());
        // Drop down layout style - list view with radio button
        natureNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        natureSpinner.setAdapter(natureNameAdapter);

        long dvId =1;
        dvId =  prjectList.getDivisions().getId();
        for(int i=0;i<divisionLists.size();i++){


            if(divisionLists.get(i).getId()==dvId){

                divSpinner.setSelection(i);
            }
        }
        long natureId =1;
        natureId =  prjectList.getNatureOfProject().getId();
        for(int i=0;i<natureWorkLists.size();i++){


            if(natureWorkLists.get(i).getId()==natureId){

                natureSpinner.setSelection(i);
            }
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null)
                    if(dialog.isShowing())
                        dialog.dismiss();
            }
        });

        btnDeleteProj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog(prjectList.getId(),prjectList.getProjectDefination());
                if(dialog!=null)
                    if(dialog.isShowing())
                        dialog.dismiss();
            }
        });


        // if button is clicked, close the custom dialog
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int divCode = -1;
                long natureCode = -1;
                int pos =  divSpinner.getSelectedItemPosition();
                divCode = divisionLists.get(pos).getId();
                int naturePos =  natureSpinner.getSelectedItemPosition();
                natureCode = divisionLists.get(naturePos).getId();
                String strProjName = !edtProjectName.getText().toString().trim().isEmpty()?edtProjectName.getText().toString():"";
                String strProjLoc = !edtProjLoc.getText().toString().trim().isEmpty()?edtProjLoc.getText().toString():"";
                String strProjDef = !edtProjDef.getText().toString().trim().isEmpty()?edtProjDef.getText().toString():"";


                if(divCode==-1){
                    Toast.makeText(ManageProjects.this,"Please Select Division",Toast.LENGTH_SHORT).show();

                    return;

                }

                if(natureCode==-1){
                    Toast.makeText(ManageProjects.this,"Please Select Nature of work",Toast.LENGTH_SHORT).show();

                    return;

                }

                if(strProjName.length()==0){
                    Toast.makeText(ManageProjects.this,"Please Enter Project Name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strProjDef.length()==0){
                    Toast.makeText(ManageProjects.this,"Please Enter Project Defination",Toast.LENGTH_SHORT).show();

                    return;
                }




                callEditProjectSercvice(prjectList.getId(),divCode,natureCode,strProjName,strProjLoc,strProjDef);


                dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
    private void showAlertDialog(Integer id, String projectDefination) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ManageProjects.this);
        alertDialogBuilder.setTitle("Delete "+projectDefination+"?");
        alertDialogBuilder.setMessage("Project will be removed from user(s) if assigned.\nDo you want to delete the project "+projectDefination);
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        alertDialog.dismiss();
                        callDeleteProjectService(id);
                    }
                }).setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                alertDialog.dismiss();
            }
        });



        alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    private void callDeleteProjectService(int projId) {
        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Creating Project...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        UserTypes userTypes = new UserTypes();
        userTypes.setId(OPHWCSharedPrefs.getInstance(ManageProjects.this).getUserId());
        Divisions divisions = new Divisions();
        divisions.setId(projId);



        Call<CreateProjectsModel> call = service.DeleteProject(divisions);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<CreateProjectsModel>() {

            @Override
            public void onResponse(Call<CreateProjectsModel> call, Response<CreateProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        int statusCode = response.body().statusCode;
                        if(statusCode==1){
                            getAllProjectService();
                            Toast.makeText(ManageProjects.this,"Project has been updated successfully.",Toast.LENGTH_SHORT).show();

                        }else{
                            getAllProjectService();
                            Toast.makeText(ManageProjects.this,""+response.body().statusMessage,Toast.LENGTH_SHORT).show();

                        }
                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();
                        try {
                            (new ManageProjects()).getAllProjectService();
                        }catch (Exception e){

                        }

                        break;
                    case 2:
                        getAllProjectService();
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        getAllProjectService();
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<CreateProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);

                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to create project",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });


    }

    private void callEditProjectSercvice(Integer id, int divCode, long natureCode, String strProjName, String strProjLoc, String strProjDef) {

        ProgressDialog progressDialog = new ProgressDialog(ManageProjects.this);
        progressDialog.setMessage("Creating Project...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        UserTypes userTypes = new UserTypes();
        userTypes.setId(OPHWCSharedPrefs.getInstance(ManageProjects.this).getUserId());
        Divisions divisions = new Divisions();
        divisions.setId(divCode);
        NatureOfProject nop = new NatureOfProject();
        nop.setId(natureCode);
        UpdateProjectItem createProjectItem = new UpdateProjectItem();
        createProjectItem.setCreatedUser(userTypes);
        createProjectItem.setDivisions(divisions);
        createProjectItem.setProjectName(strProjName);
        createProjectItem.setProjectLocation(strProjLoc);
        createProjectItem.setProjectDefination(strProjDef);
        createProjectItem.setNatureOfProject(nop);
        createProjectItem.setId(id);
        createProjectItem.setComments("");



        Call<CreateProjectsModel> call = service.UpdateProject(createProjectItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<CreateProjectsModel>() {

            @Override
            public void onResponse(Call<CreateProjectsModel> call, Response<CreateProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        int statusCode = response.body().statusCode;
                        if(statusCode==1){
                            getAllProjectService();
                            Toast.makeText(ManageProjects.this,"Project has been updated successfully.",Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(ManageProjects.this,""+response.body().statusMessage,Toast.LENGTH_SHORT).show();

                        }
                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();
                        try {
                            (new ManageProjects()).getAllProjectService();
                        }catch (Exception e){

                        }

                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<CreateProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageProjects.this,"Unable to create project",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }

}
