package com.ophwc.gemini.api;

import com.ophwc.gemini.model.floor.FloorWorkDetailsByFloorModel;
import com.ophwc.gemini.model.floor.FloorWorksList;
import com.ophwc.gemini.model.floor.FloorsList;
import com.ophwc.gemini.model.floor.updatefloor.FloorDetailsbyFloorIDItem;
import com.ophwc.gemini.model.project.projectBudget.budget.BudgetDetailModel;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.floor.FloorDetailsItem;
import com.ophwc.gemini.model.floor.FloorDetailsModel;
import com.ophwc.gemini.model.floor.updatefloor.UpdateFloorDetailsModel;
import com.ophwc.gemini.model.floor.updatefloor.UpdateFloorItem;
import com.ophwc.gemini.model.login.LoginItem;
import com.ophwc.gemini.model.login.LoginModel;
import com.ophwc.gemini.model.project.GetProjectByDivNatureItem;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.TaglocationItem;
import com.ophwc.gemini.model.project.TaglocationModel;
import com.ophwc.gemini.model.project.byNaturUser.GetProjByNatureAndUserItem;
import com.ophwc.gemini.model.project.byNature.GetProjectByNatureItem;
import com.ophwc.gemini.model.project.byNature.GetProjectsByNatureModel;
import com.ophwc.gemini.model.project.byProjId.GetProjectByIDModel;
import com.ophwc.gemini.model.project.createProject.CreateProjectItem;
import com.ophwc.gemini.model.project.createProject.CreateProjectsModel;
import com.ophwc.gemini.model.project.createProject.UpdateProjectItem;
import com.ophwc.gemini.model.project.imageUpload.DeleteImageItem;
import com.ophwc.gemini.model.project.imageUpload.ImageUploadModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorksWithProjCountModel;
import com.ophwc.gemini.model.project.projectBudget.ProjectBudgetItem;
import com.ophwc.gemini.model.project.projectBudget.budget.UpdateLastBudgetItem;
import com.ophwc.gemini.model.user.CreateUserItem;
import com.ophwc.gemini.model.user.CreateUserModel;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.GetAllUsersModel;
import com.ophwc.gemini.model.user.UsertypeModel;
import com.ophwc.gemini.model.user.delete.DeleteUserItem;
import com.ophwc.gemini.model.user.delete.DeleteUserModel;
import com.ophwc.gemini.model.user.update.UserUpdateItem;
import com.ophwc.gemini.model.user.update.UserUpdateModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by nareshema on 13/1/16.
 */

public interface AppApi {



    @POST("/ophwc/userService/validateUser")
    Call<LoginModel> login(@Body() LoginItem loginItem);

    @GET("/ophwc/userTypeService/getAllUserTypes")
    Call<UsertypeModel> getUsertypes();


    @POST("/ophwc/userService/createUser")
    Call<CreateUserModel> createUser(@Body() CreateUserItem createItem);

    @GET("/ophwc/userService/getAllUsers")
    Call<GetAllUsersModel> getUsers();

    @GET("/ophwc/divisionService/getAllDivisions")
    Call<divisionsModel> getDivisions();

    @POST("/ophwc/divisionService/getAllUserDivisions")
    Call<divisionsModel> getUserDivisions(@Body() ProjectsItem projectsItem);

    @POST("/ophwc/projects/getAllProjects")
    Call<ProjectsModel> getAllProjects(@Body() ProjectsItem projectsItem);
    @POST("/ophwc/projects/getProjectById")
    Call<GetProjectByIDModel> getProjectById(@Body() ProjectsItem projectsItem);

    @POST("/ophwc/projects/getProjectsByNatureAndUser")
    Call<ProjectsModel> getProjectsByNatureUser(@Body() GetProjByNatureAndUserItem projectsItem);

    @POST("/ophwc/projects/createProject")
    Call<CreateProjectsModel> createProject(@Body() CreateProjectItem projectsItem);

    @PUT("/ophwc/projects/updateProject")
    Call<CreateProjectsModel> UpdateProject(@Body() UpdateProjectItem projectsItem);

    @HTTP(method = "DELETE", path = "/ophwc/projects/deleteProjectById", hasBody = true)
    Call<CreateProjectsModel> DeleteProject(@Body Divisions object);

    @GET("/ophwc/projects/getProjects")
    Call<ProjectsModel> getProjects();

    @GET("/ophwc/natureWork/getAllNatureWorks")
    Call<NatureOfWorkModel> getNatureOfWork();

    @POST("/ophwc/natureWork/getAllNatureWorksWithProjCount")
    Call<NatureWorksWithProjCountModel> getAllNatureWorksWithProjCount(@Body() GetProjByNatureAndUserItem getProjByNatureAndUserItem);

    @POST("/ophwc/projects/getAllUserProjects")
    Call<ProjectsModel> getAllUserProjects(@Body() ProjectsItem projectsItem);


    @POST("/ophwc/siteVisit/setSiteLocation")
    Call<TaglocationModel> tagLocation(@Body() TaglocationItem taglocationItem);

    @HTTP(method = "DELETE", path = "/ophwc/siteVisit/deleteImage", hasBody = true)
    Call<ImageUploadModel> DeleteImage(@Body DeleteImageItem object);

    @Multipart
    @POST("/ophwc/siteVisit/setSiteCapturePhoto")
    Call<ImageUploadModel> uploadImage(@Part MultipartBody.Part image,
                                       @Part("divisionId") RequestBody divId, @Part("projectId") RequestBody projectId,
                                       @Part("conStatus") RequestBody consStatus, @Part("createdUser") RequestBody createdUser,
                                       @Part("comments") RequestBody comments);
    @PUT("/ophwc/userService/updateUser")
    Call<UserUpdateModel> updateUser(@Body() UserUpdateItem createUserItem);

    @PUT("/ophwc/userService/updateUserWithProjects")
    Call<UserUpdateModel> updateUserProj(@Body() UserUpdateItem createUserItem);


    @HTTP(method = "DELETE", path = "/ophwc/userService/deleteUser", hasBody = true)
    Call<DeleteUserModel> deleteObject(@Body DeleteUserItem object);

    @POST("/ophwc/projects/getProjectsByDivisionAndNature")
    Call<GetProjectsByNatureModel> getAllProjectsbyNature(@Body GetProjectByNatureItem projectsItem);

    @POST("/ophwc/projects/assignGetProjectsByDivisionAndNature")
    Call<GetProjectsByNatureModel> assignGetProjectsByDivisionAndNature(@Body GetProjectByDivNatureItem projectsItem);
  //Budget..........
  //new add 15052019...
  @POST("/ophwc/projects/addAmmount")
  Call<UserUpdateModel> updateProjectBudget(@Body() ProjectBudgetItem pbi);
    //new add 28062019...
  @POST("/ophwc/projects/getTotalAmntOfProject")
  Call<BudgetDetailModel> getBudgetDetails(@Body FloorDetailsItem floorDetailsItem);
     //24072019
  @PUT("OphwcService/ophwc/projects/updateAmmount")
  Call<UserUpdateModel> updateLastBudgetDetails(@Body UpdateLastBudgetItem ubi);
    //floor details............
    //new add 28062019...
    @POST("/ophwc/floorStatusService/getProjectFloorsWorksStatus")
    Call<FloorDetailsModel> getFloorDetails(@Body FloorDetailsItem floorDetailsItem);

    @POST("/ophwc/floorStatusService/saveProjectFloorStatusMultipleFloors")
    Call<UpdateFloorDetailsModel> updateFloorDetails(@Body UpdateFloorItem floorUpdateDetailsItem);

    @POST("/ophwc/floorStatusService/getProjectFloorsWorksStatusByFloor")
    Call<FloorWorkDetailsByFloorModel> getProjectFloorByFloor(@Body FloorDetailsbyFloorIDItem floorUpdateDetailsItem);
}

