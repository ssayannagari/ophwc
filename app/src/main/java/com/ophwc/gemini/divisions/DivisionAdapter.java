package com.ophwc.gemini.divisions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ophwc.gemini.R;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkListwithProj;

import java.util.List;

public class DivisionAdapter extends BaseAdapter {
    private Context mContext;
    private List<NatureWorkListwithProj> mDivisionLists;


    // Constructor
    public DivisionAdapter(Context c, List<NatureWorkListwithProj> divisionLists) {
        mContext = c;
        mDivisionLists = divisionLists;
    }



    public int getCount() {
        return mDivisionLists.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

       LayoutInflater inflator =(LayoutInflater.from(mContext));
       if (convertView==null)
       convertView =inflator.inflate(R.layout.divisions_grid_row,null);
        TextView tvRowDivName = (TextView) convertView.findViewById(R.id.tvRowDivName);
        TextView tvRowProjStatus = (TextView) convertView.findViewById(R.id.tvRowProjStatus);
        tvRowProjStatus.setVisibility(View.VISIBLE);
        tvRowProjStatus.setText(" " +mDivisionLists.get(position).getProjCount()+" Projects");

        tvRowDivName.setText(" "+mDivisionLists.get(position).getName());



        return convertView;
    }

    // Keep all Images in array

}