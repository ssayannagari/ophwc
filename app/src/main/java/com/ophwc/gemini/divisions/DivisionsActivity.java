package com.ophwc.gemini.divisions;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.byNaturUser.GetProjByNatureAndUserItem;
import com.ophwc.gemini.model.project.imageUpload.ImageUploadModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkListwithProj;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorksWithProjCountModel;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.GetAllUsersModel;
import com.ophwc.gemini.model.user.UserList;
import com.ophwc.gemini.offline.OfflineProjectsActivity;
import com.ophwc.gemini.offline.offlineProjObj;
import com.ophwc.gemini.projects.CaptureProjectStatus;
import com.ophwc.gemini.projects.ProjectsListActivity;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.projects.map.ProjectsMapsActivity;
import com.ophwc.gemini.superuser.SuperuserDivisions;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.groupchatwebrtc.activities.BaseActivity;
import com.quickblox.sample.groupchatwebrtc.activities.LoginActivity;
import com.quickblox.sample.groupchatwebrtc.activities.OpponentsActivity;
import com.quickblox.sample.groupchatwebrtc.activities.PermissionsActivity;
import com.quickblox.sample.groupchatwebrtc.activities.SplashActivity;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.services.CallService;
import com.quickblox.sample.groupchatwebrtc.services.CheckCallService;
import com.quickblox.sample.groupchatwebrtc.services.gcm.GcmPushListenerService;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.PermissionsChecker;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;
import com.quickblox.users.model.QBUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DivisionsActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

SharedPrefsHelper sharedPrefsHelper;
    DrawerLayout drawer;
    NavigationView navigationView;
    List<DivisionList> divisionLists = new ArrayList<>();
    GridView gridView;
    OPHWCSharedPrefs ophwcSharedPrefs;
    private PermissionsChecker checker;
    private QbUsersDbManager dbManager;
    List<NatureWorkListwithProj> natureWorkLists =new ArrayList<>();
    TextView tvDivSuperName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(DivisionsActivity.this);

        sharedPrefsHelper = SharedPrefsHelper.getInstance();
       // sharedPrefsHelper.save(Consts.PREF_LOGIN_AS,0);
       gridView = (GridView) findViewById(R.id.gridView);
        tvDivSuperName = (TextView) findViewById(R.id.tvDivSuperName);
        checker = new PermissionsChecker(getApplicationContext());
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());

        /*Intent itent = new Intent(this, GcmPushListenerService.class);
        startService(itent);*/
        /*startService(new Intent(this, CheckCallService.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, CheckCallService.class);
        PendingIntent pintent = PendingIntent
                .getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                5*1000, pintent);
        Log.e("QBUSER",sharedPrefsHelper.getQbUser().getFullName());
        Log.e("QBUSER",sharedPrefsHelper.getQbUser().getFileId()+"");*/

        CallService.start(DivisionsActivity.this,sharedPrefsHelper.getQbUser());
        initialiseToolbar();



        getAllDivisionService();


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //divisionLists.
                ophwcSharedPrefs.putDivisionId(divisionLists.get(0).getId());
                if (UsersUtils.isNetworkAvailable(DivisionsActivity.this)) {
                    startActivity(new Intent(DivisionsActivity.this, ProjectsListActivity.class)
                            .putExtra("divId", String.valueOf(divisionLists.get(0).getId()))
                            .putExtra("natureCode", natureWorkLists.get(position).getId()));
                }else{
                    Toast.makeText(DivisionsActivity.this,"No internet connection available",Toast.LENGTH_LONG).show();
                    Toast.makeText(DivisionsActivity.this,"Loading offline projects",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(DivisionsActivity.this, OfflineProjectsActivity.class));


                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        //loadUser();


    }
    private void checkPermission() {
        if (checker.lacksPermissions(Consts.PERMISSIONS)) {
            startPermissionsActivity(false);
        }
    }
    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, Consts.PERMISSIONS);
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nature of Project");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);
        TextView tvDrawerHeaderEmail = (TextView) headerView.findViewById(R.id.tvDrawerHeaderEmail);
        TextView tvDrawerHeaderName = (TextView) headerView.findViewById(R.id.tvDrawerHeaderName);
        navigationView.getMenu().findItem(R.id.nav_slideshow).setVisible(false);



       // tvDrawerHeaderEmail.setText((OPHWCSharedPrefs.getInstance(DivisionsActivity.this).getUserEmail()));

        tvDrawerHeaderName.setText(OPHWCSharedPrefs.getInstance(DivisionsActivity.this).getUserName());
        tvDrawerHeaderEmail.setText(OPHWCSharedPrefs.getInstance(DivisionsActivity.this).getUserEmail()+"\n"+"Version : "+UsersUtils.VERSION_NUMBER);

    }
    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(DivisionsActivity.this);
        try {
            List<offlineProjObj> offlineImages = qbUsersDbManager.getOfflineImage();
            if(offlineImages.size()>0){

                menu.findItem(R.id.action_sync).setVisible(true);

            }else{
                menu.findItem(R.id.action_sync).setVisible(false);
            }
        }catch (Exception e){
            menu.findItem(R.id.action_sync).setVisible(false);

            e.printStackTrace();

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            sharedPrefsHelper.save(Consts.PREF_LOGIN_AS,-1);
            //System.exit(0);
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        }
        if (id == R.id.action_sync) {
            progressDialog = new ProgressDialog(DivisionsActivity.this);
            progressDialog.setMessage("Uploading image...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        syncImages();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            OpponentsActivity.start(DivisionsActivity.this,false);
        } else if (id == R.id.nav_gallery) {


        } else if (id == R.id.nav_slideshow) {
            syncImages();

        } else if (id == R.id.nav_manage) {
            startActivity(new Intent(DivisionsActivity.this,ProjectsMapsActivity.class));
            //Toast.makeText(DivisionsActivity.this,"Under process",Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private void getAllNatureOfWorks() {
        ProgressDialog progressDialog = new ProgressDialog(DivisionsActivity.this);
        progressDialog.setMessage("Getting Nature of projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        GetProjByNatureAndUserItem  getProjByNatureAndUserItem = new GetProjByNatureAndUserItem();
        getProjByNatureAndUserItem.setUserId(ophwcSharedPrefs.getUserId());
        getProjByNatureAndUserItem.setId(ophwcSharedPrefs.getDivisionId());

        Call<NatureWorksWithProjCountModel> call = service.getAllNatureWorksWithProjCount(getProjByNatureAndUserItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<NatureWorksWithProjCountModel>() {

            @Override
            public void onResponse(Call<NatureWorksWithProjCountModel> call, Response<NatureWorksWithProjCountModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1) {
                            if (response.body().getNatureWorkList() != null) {
                                natureWorkLists = response.body().getNatureWorkList();
                                if (natureWorkLists.size() > 0) {
                                    // getNaturkNames();
                                    setAdapters();
                                }

                            } else {
                                Toast.makeText(DivisionsActivity.this, response.body().getStatusMessage() + "", Toast.LENGTH_SHORT).show();


                            }
                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }
                loadUser();

            }



            @Override
            public void onFailure(Call<NatureWorksWithProjCountModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                loadUser();

                Toast.makeText(DivisionsActivity.this,"Unable to get Nature of work",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllDivisionService() {
        ProgressDialog progressDialog = new ProgressDialog(DivisionsActivity.this);
        progressDialog.setMessage("Getting divisions...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(DivisionsActivity.this);
        int userId = ophwcSharedPrefs.getUserId();
        ProjectsItem projectsItem = new ProjectsItem();
        projectsItem.setId(userId);

        Call<divisionsModel> call = service.getUserDivisions(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<divisionsModel>() {

            @Override
            public void onResponse(Call<divisionsModel> call, Response<divisionsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if( response.body().getDivisionList()!=null) {
                            divisionLists.clear();
                            divisionLists = response.body().getDivisionList();
                            tvDivSuperName.setText(divisionLists.get(0).getDivisionName());
                            ophwcSharedPrefs.putDivisionId(divisionLists.get(0).getId());
                            callUserProjectsService();

                        }else{
                            Toast.makeText(DivisionsActivity.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                        }
                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }


            }



            @Override
            public void onFailure(Call<divisionsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }
                callUserProjectsService();

                Toast.makeText(DivisionsActivity.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void loadUser() {
        startLoadUsers();
        /*if(dbManager.getAllUsers().size()==0) {
            startLoadUsers();
        }else{
            checkPermission();
        }*/

    }
    private void startLoadUsers() {
        showProgressDialog(R.string.dlg_loading_opponents);
        String currentRoomName = SharedPrefsHelper.getInstance().get(Consts.PREF_CURREN_ROOM_NAME);
        requestExecutor.loadUsersByTag(currentRoomName, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                hideProgressDialog();
                dbManager.clearDivUserDB();
                dbManager.clearDB();
                Log.d("CALLOFF",result.size()+"");
                dbManager.saveAllUsers(result, true);
                checkPermission();
                //initUsersList();
            }

            @Override
            public void onError(QBResponseException responseException) {
                hideProgressDialog();
                checkPermission();
                showErrorSnackbar(R.string.loading_users_error, responseException, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startLoadUsers();
                    }
                });
            }
        });
    }
    private void callUserProjectsService() {
        ProgressDialog progressDialog = new ProgressDialog(DivisionsActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();
        String id = "1";
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(DivisionsActivity.this);

        int  projUserid = 0;
        projUserid = ophwcSharedPrefs.getUserId();
        projectsItem.setId(ophwcSharedPrefs.getUserId());

        Call<ProjectsModel> call = service.getAllUserProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try{
                            if(response.body().getPrjectList()!=null) {
                                if(response.body().getPrjectList().size()>0) {

                                    int i =0;
                                    int j=0;

                                    QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(DivisionsActivity.this);
                                    qbUsersDbManager.clearAllProjDB();
                                    qbUsersDbManager.saveAlluserProjects(response.body().getPrjectList());

                                }else{
                                    //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude()), 8.0f));

                                    Toast.makeText(DivisionsActivity.this,"No Projects available",Toast.LENGTH_SHORT).show();
                                }
                            }else{

                            }
                        }catch (Exception e){

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
               // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(DivisionsActivity.this,"No projects available",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }


        });
        getAllNatureOfWorks();
    }
    private void setAdapters() {
        gridView.setAdapter(new DivisionAdapter(this,natureWorkLists));
    }

    private void syncImages() {
        QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(DivisionsActivity.this);
        try {

            List<offlineProjObj> offlineImages = qbUsersDbManager.getOfflineImage();
            if(offlineImages.size()>0){
             uploadImage(offlineImages.get(0).getOfflineImage(),offlineImages.get(0).getImageId(),
                     offlineImages.get(0).getProjectName(),offlineImages.get(0).getProjId());
            }else{
                invalidateOptionsMenu();
                if(progressDialog!=null){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }
                Toast.makeText(DivisionsActivity.this,"All images are synced",Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            invalidateOptionsMenu();
            if(progressDialog!=null){
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
            e.printStackTrace();
            Toast.makeText(DivisionsActivity.this,"All images are synced",Toast.LENGTH_SHORT).show();
        }

    }
    ProgressDialog progressDialog;
    private void uploadImage(byte[] imageBytes,int imgId,String imagePath,int projId) {



        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();


        AppApi retrofitInterface = retrofit.create(AppApi.class);
        File file = new File(imagePath);
        Log.d("PATH", "Filename " + file.getName());
        //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
        String ts ="";
        try {
            Long tsLong = System.currentTimeMillis() / 1000;
            ts = tsLong.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        String divisionId = "";
        //String projId = "";
        try{

        }catch (Exception e){

        }
        OPHWCSharedPrefs osharedPrefsHelper = OPHWCSharedPrefs.getInstance(DivisionsActivity.this);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), "OPHWC_PROJECT"+ts);
        RequestBody divId = RequestBody.create(MediaType.parse("text/plain"), ""+osharedPrefsHelper.getDivisionId());
       // RequestBody divId = RequestBody.create(MediaType.parse("text/plain"), "1");
       RequestBody projectId = RequestBody.create(MediaType.parse("text/plain"), ""+projId);
       //RequestBody projectId = RequestBody.create(MediaType.parse("text/plain"), "1");
        //RequestBody consStatus = RequestBody.create(MediaType.parse("text/plain"), edtProjectStatus.getText().toString());
        RequestBody consStatus = RequestBody.create(MediaType.parse("text/plain"), "50");
        RequestBody createdUser = RequestBody.create(MediaType.parse("text/plain"), ""+osharedPrefsHelper.getUserId());
        //RequestBody createdUser = RequestBody.create(MediaType.parse("text/plain"), ""+1);
        // RequestBody comments = RequestBody.create(MediaType.parse("text/plain"), edtProjectComments.getText().toString());
        RequestBody comments = RequestBody.create(MediaType.parse("text/plain"), "Comments");
        //Log.e("PARAM1",getIntent().getStringExtra("divId")+" "+getIntent().getIntExtra("projId",-1)+" "+osharedPrefsHelper.getUserId()+" "+createdUser+" "+comments );
        Call<ImageUploadModel> call = retrofitInterface.uploadImage(body,divId,projectId,consStatus,createdUser,comments);
        //mProgressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<ImageUploadModel>() {
            @Override
            public void onResponse(Call<ImageUploadModel> call, Response<ImageUploadModel> response) {

                if (response.code()==200) {
                    Toast.makeText(DivisionsActivity.this,"Image uploaded successfully",Toast.LENGTH_SHORT).show();

                    ImageUploadModel responseBody = response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(response);
                    QbUsersDbManager.getInstance(DivisionsActivity.this).DeleteImage(imgId);
                    syncImages();


                    //// mBtImageShow.setVisibility(View.VISIBLE);
                    //mImageUrl = URL + responseBody.getPath();
                    //Snackbar.make(findViewById(R.id.content), responseBody.getMessage(),Snackbar.LENGTH_SHORT).show();

                } else {
                   // Toast.makeText(CaptureProjectStatus.this,"Failed to upload image, try again.",Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(Call<ImageUploadModel> call, Throwable t) {

                // mProgressBar.setVisibility(View.GONE);



                Log.d("Image", "onFailure: "+t.getLocalizedMessage());
            }

        });


    }
}

