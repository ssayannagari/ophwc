package com.ophwc.gemini.model;


public class UsersObj {
    String _userName, _userType, _mobileNo, _emailId, _profilePic,_password, fullName;
    int _userId, _userTypeCode;

    public UsersObj(int userId, String userName, String userType, int userTypeCode,
                    String emailId, String mobileNo, String profilePic, String password,String userfullName){
        this._mobileNo = mobileNo;
        this._userId = userId;
        this._userName = userName;
        this._userType = userType;
        this._userTypeCode = userTypeCode;
        this._emailId = emailId;
        this._profilePic = profilePic;
        this._password = password;
        this.fullName = userfullName;

    }

    public String getFullName() {
        return fullName;
    }

    public int getUserId() {
        return _userId;
    }

    public int getUserTypeCode() {
        return _userTypeCode;
    }

    public String getMobileNo() {
        return _mobileNo;
    }

    public String getUserName() {
        return _userName;
    }

    public String getEmailId() {
        return _emailId;
    }

    public String getUserType() {
        return _userType;
    }
    public String getProfilepic() {
        return _profilePic;
    }

    public String get_password() {
        return _password;
    }

}
