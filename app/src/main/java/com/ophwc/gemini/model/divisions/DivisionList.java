package com.ophwc.gemini.model.divisions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DivisionList {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("divisionName")
    @Expose
    public String divisionName;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("completedProjects")
    @Expose
    public Integer completedProjects;
    @SerializedName("noOfProjects")
    @Expose
    public Integer noOfProjects;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCompletedProjects() {
        return completedProjects;
    }

    public void setCompletedProjects(Integer completedProjects) {
        this.completedProjects = completedProjects;
    }

    public Integer getNoOfProjects() {
        return noOfProjects;
    }

    public void setNoOfProjects(Integer noOfProjects) {
        this.noOfProjects = noOfProjects;
    }
}
