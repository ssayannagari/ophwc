package com.ophwc.gemini.model.divisions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class divisionsModel {
    @SerializedName("statusCode")
    @Expose
    public Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    public String statusMessage;
    @SerializedName("divisionList")
    @Expose
    public List<DivisionList> divisionList = new ArrayList<>();
    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<DivisionList> getDivisionList() {
        return divisionList;
    }

    public void setDivisionList(List<DivisionList> divisionList) {
        this.divisionList = divisionList;
    }
}
