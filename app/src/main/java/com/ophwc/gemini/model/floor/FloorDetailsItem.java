package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FloorDetailsItem {
    @SerializedName("id")
    @Expose
    private long id;

    /**
     * No args constructor for use in serialization
     *
     */
    public FloorDetailsItem() {
    }

    /**
     *
     * @param id
     */
    public FloorDetailsItem(long id) {
        super();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
