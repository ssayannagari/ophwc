package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FloorDetailsModel {



    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("project")
    @Expose
    private Object project;
    @SerializedName("floor")
    @Expose
    private Object floor;
    @SerializedName("floorWork")
    @Expose
    private Object floorWork;
    @SerializedName("projectStatus")
    @Expose
    private Object projectStatus;
    @SerializedName("floorsList")
    @Expose
    private List<FloorsList> floorsList = null;
    @SerializedName("floorWorksList")
    @Expose
    private List<FloorWorksList> floorWorksList = null;
    @SerializedName("projectStatusList")
    @Expose
    private List<ProjectStatusList> projectStatusList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public FloorDetailsModel() {
    }

    /**
     *
     * @param floorWork
     * @param id
     * @param projectStatus
     * @param project
     * @param floor
     * @param projectStatusList
     * @param floorsList
     * @param floorWorksList
     */
    public FloorDetailsModel(Object id, Object project, Object floor, Object floorWork, Object projectStatus, List<FloorsList> floorsList, List<FloorWorksList> floorWorksList, List<ProjectStatusList> projectStatusList) {
        super();
        this.id = id;
        this.project = project;
        this.floor = floor;
        this.floorWork = floorWork;
        this.projectStatus = projectStatus;
        this.floorsList = floorsList;
        this.floorWorksList = floorWorksList;
        this.projectStatusList = projectStatusList;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getProject() {
        return project;
    }

    public void setProject(Object project) {
        this.project = project;
    }

    public Object getFloor() {
        return floor;
    }

    public void setFloor(Object floor) {
        this.floor = floor;
    }

    public Object getFloorWork() {
        return floorWork;
    }

    public void setFloorWork(Object floorWork) {
        this.floorWork = floorWork;
    }

    public Object getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(Object projectStatus) {
        this.projectStatus = projectStatus;
    }

    public List<FloorsList> getFloorsList() {
        return floorsList;
    }

    public void setFloorsList(List<FloorsList> floorsList) {
        this.floorsList = floorsList;
    }

    public List<FloorWorksList> getFloorWorksList() {
        return floorWorksList;
    }

    public void setFloorWorksList(List<FloorWorksList> floorWorksList) {
        this.floorWorksList = floorWorksList;
    }

    public List<ProjectStatusList> getProjectStatusList() {
        return projectStatusList;
    }

    public void setProjectStatusList(List<ProjectStatusList> projectStatusList) {
        this.projectStatusList = projectStatusList;
    }

}
