package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FloorWorkDetailsByFloorModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("floorNo")
    @Expose
    private int floorNo;
    @SerializedName("floorName")
    @Expose
    private String floorName;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("floorsWork")
    @Expose
    private List<FloorWorksList> floorsWork = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public FloorWorkDetailsByFloorModel() {
    }

    /**
     *
     * @param id
     * @param floorName
     * @param floorNo
     * @param status
     * @param floorsWork
     */
    public FloorWorkDetailsByFloorModel(int id, int floorNo, String floorName, Object status, List<FloorWorksList> floorsWork) {
        super();
        this.id = id;
        this.floorNo = floorNo;
        this.floorName = floorName;
        this.status = status;
        this.floorsWork = floorsWork;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloorNo() {
        return floorNo;
    }

    public void setFloorNo(int floorNo) {
        this.floorNo = floorNo;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public List<FloorWorksList> getFloorsWork() {
        return floorsWork;
    }

    public void setFloorsWork(List<FloorWorksList> floorsWork) {
        this.floorsWork = floorsWork;
    }

}
