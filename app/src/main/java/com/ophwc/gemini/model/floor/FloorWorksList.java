package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FloorWorksList {

@SerializedName("id")
@Expose
private int id;
@SerializedName("workType")
@Expose
private String workType;
@SerializedName("status")
@Expose
private String status;
@SerializedName("projectStatus")
@Expose
private Object projectStatus;

/**
* No args constructor for use in serialization
* 
*/
public FloorWorksList() {
}

/**
* 
* @param id
* @param projectStatus
* @param status
* @param workType
*/
public FloorWorksList(int id, String workType, String status, Object projectStatus) {
super();
this.id = id;
this.workType = workType;
this.status = status;
this.projectStatus = projectStatus;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getWorkType() {
return workType;
}

public void setWorkType(String workType) {
this.workType = workType;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Object getProjectStatus() {
return projectStatus;
}

public void setProjectStatus(Object projectStatus) {
this.projectStatus = projectStatus;
}

}