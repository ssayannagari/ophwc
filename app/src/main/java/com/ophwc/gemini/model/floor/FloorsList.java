package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FloorsList {

@SerializedName("id")
@Expose
private int id;
@SerializedName("floorNo")
@Expose
private int floorNo;
@SerializedName("floorName")
@Expose
private String floorName;
@SerializedName("status")
@Expose
private String status;
@SerializedName("floorsWork")
@Expose
private Object floorsWork;

/**
* No args constructor for use in serialization
* 
*/
public FloorsList() {
}

/**
* 
* @param id
* @param floorName
* @param floorNo
* @param status
* @param floorsWork
*/
public FloorsList(int id, int floorNo, String floorName, String status, Object floorsWork) {
super();
this.id = id;
this.floorNo = floorNo;
this.floorName = floorName;
this.status = status;
this.floorsWork = floorsWork;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public int getFloorNo() {
return floorNo;
}

public void setFloorNo(int floorNo) {
this.floorNo = floorNo;
}

public String getFloorName() {
return floorName;
}

public void setFloorName(String floorName) {
this.floorName = floorName;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public Object getFloorsWork() {
return floorsWork;
}

public void setFloorsWork(Object floorsWork) {
this.floorsWork = floorsWork;
}

}