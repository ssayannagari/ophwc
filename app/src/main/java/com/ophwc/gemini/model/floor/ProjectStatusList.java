package com.ophwc.gemini.model.floor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectStatusList {

@SerializedName("id")
@Expose
private int id;
@SerializedName("status")
@Expose
private String status;
@SerializedName("checkStatus")
@Expose
private String checkStatus;

/**
* No args constructor for use in serialization
* 
*/
public ProjectStatusList() {
}

/**
* 
* @param id
* @param checkStatus
* @param status
*/
public ProjectStatusList(int id, String status, String checkStatus) {
super();
this.id = id;
this.status = status;
this.checkStatus = checkStatus;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getCheckStatus() {
return checkStatus;
}

public void setCheckStatus(String checkStatus) {
this.checkStatus = checkStatus;
}

}