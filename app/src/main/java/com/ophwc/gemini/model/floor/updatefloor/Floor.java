package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Floor {

@SerializedName("id")
@Expose
private int id;

/**
* No args constructor for use in serialization
* 
*/
public Floor() {
}

/**
* 
* @param id
*/
public Floor(int id) {
super();
this.id = id;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

}