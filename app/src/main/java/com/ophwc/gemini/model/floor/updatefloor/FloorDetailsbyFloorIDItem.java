package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FloorDetailsbyFloorIDItem {
    @SerializedName("project")
@Expose
private Project project;
    @SerializedName("floor")
    @Expose
    private Floor floor;

    /**
     * No args constructor for use in serialization
     *
     */
    public FloorDetailsbyFloorIDItem() {
    }

    /**
     *
     * @param project
     * @param floor
     */
    public FloorDetailsbyFloorIDItem(Project project, Floor floor) {
        super();
        this.project = project;
        this.floor = floor;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

}