package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FloorWork {

@SerializedName("id")
@Expose
private int id;

/**
* No args constructor for use in serialization
* 
*/
public FloorWork() {
}

/**
* 
* @param id
*/
public FloorWork(int id) {
super();
this.id = id;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

}