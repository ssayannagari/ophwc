package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectFloorstatus {

@SerializedName("project")
@Expose
private Project project;
@SerializedName("floor")
@Expose
private Floor floor;
@SerializedName("floorWork")
@Expose
private FloorWork floorWork;
@SerializedName("projectStatus")
@Expose
private ProjectStatus projectStatus;

/**
* No args constructor for use in serialization
* 
*/
public ProjectFloorstatus() {
}

/**
* 
* @param floorWork
* @param projectStatus
* @param project
* @param floor
*/
public ProjectFloorstatus(Project project, Floor floor, FloorWork floorWork, ProjectStatus projectStatus) {
super();
this.project = project;
this.floor = floor;
this.floorWork = floorWork;
this.projectStatus = projectStatus;
}

public Project getProject() {
return project;
}

public void setProject(Project project) {
this.project = project;
}

public Floor getFloor() {
return floor;
}

public void setFloor(Floor floor) {
this.floor = floor;
}

public FloorWork getFloorWork() {
return floorWork;
}

public void setFloorWork(FloorWork floorWork) {
this.floorWork = floorWork;
}

public ProjectStatus getProjectStatus() {
return projectStatus;
}

public void setProjectStatus(ProjectStatus projectStatus) {
this.projectStatus = projectStatus;
}

}