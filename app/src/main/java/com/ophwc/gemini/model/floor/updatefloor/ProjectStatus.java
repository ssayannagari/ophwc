package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectStatus {

@SerializedName("id")
@Expose
private int id;

/**
* No args constructor for use in serialization
* 
*/
public ProjectStatus() {
}

/**
* 
* @param id
*/
public ProjectStatus(int id) {
super();
this.id = id;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

}