package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateFloorDetailsModel {

@SerializedName("statusCode")
@Expose
private int statusCode;
@SerializedName("statusMessage")
@Expose
private String statusMessage;

/**
* No args constructor for use in serialization
* 
*/
public UpdateFloorDetailsModel() {
}

/**
* 
* @param statusCode
* @param statusMessage
*/
public UpdateFloorDetailsModel(int statusCode, String statusMessage) {
super();
this.statusCode = statusCode;
this.statusMessage = statusMessage;
}

public int getStatusCode() {
return statusCode;
}

public void setStatusCode(int statusCode) {
this.statusCode = statusCode;
}

public String getStatusMessage() {
return statusMessage;
}

public void setStatusMessage(String statusMessage) {
this.statusMessage = statusMessage;
}

}