package com.ophwc.gemini.model.floor.updatefloor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateFloorItem {

    @SerializedName("projectFloorStatus")
    @Expose
    private List<ProjectFloorstatus> projectFloorStatus = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateFloorItem() {
    }

    /**
     *
     * @param projectFloorStatus
     */
    public UpdateFloorItem(List<ProjectFloorstatus> projectFloorStatus) {
        super();
        this.projectFloorStatus = projectFloorStatus;
    }

    public List<ProjectFloorstatus> getProjectFloorStatus() {
        return projectFloorStatus;
    }

    public void setProjectFloorStatus(List<ProjectFloorstatus> projectFloorStatus) {
        this.projectFloorStatus = projectFloorStatus;
    }

}