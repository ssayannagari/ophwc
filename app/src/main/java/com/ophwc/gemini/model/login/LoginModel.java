package com.ophwc.gemini.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.util.ArrayList;
import java.util.List;

public class LoginModel {


    @SerializedName("statusCode")
    @Expose
    private long statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("user")
    @Expose
    private UserList userList;

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginModel() {
    }

    /**
     *
     * @param statusCode
     * @param userList
     * @param statusMessage
     */
    public LoginModel(long statusCode, String statusMessage, UserList userList) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.userList = userList;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public UserList getUserList() {
        return userList;
    }

    public void setUserList(UserList userList) {
        this.userList = userList;
    }

}