package com.ophwc.gemini.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserList {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("userTypes")
    @Expose
    private UserTypes userTypes;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phoneNum")
    @Expose
    private String phoneNum;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("userImage")
    @Expose
    private Object userImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("natureList")
    @Expose
    private Object natureList;
    @SerializedName("projList")
    @Expose
    private Object projList;
    @SerializedName("projName")
    @Expose
    private Object projName;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserList() {
    }

    /**
     *
     * @param lastName
     * @param status
     * @param projList
     * @param userTypes
     * @param userImage
     * @param divisions
     * @param password
     * @param natureList
     * @param id
     * @param username
     * @param phoneNum
     * @param emailId
     * @param projName
     * @param firstName
     */
    public UserList(int id, Divisions divisions, UserTypes userTypes, String username, String password, String firstName, String lastName, String phoneNum, String emailId, Object userImage, String status, Object natureList, Object projList, Object projName) {
        super();
        this.id = id;
        this.divisions = divisions;
        this.userTypes = userTypes;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.emailId = emailId;
        this.userImage = userImage;
        this.status = status;
        this.natureList = natureList;
        this.projList = projList;
        this.projName = projName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    public UserTypes getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(UserTypes userTypes) {
        this.userTypes = userTypes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Object getUserImage() {
        return userImage;
    }

    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getNatureList() {
        return natureList;
    }

    public void setNatureList(Object natureList) {
        this.natureList = natureList;
    }

    public Object getProjList() {
        return projList;
    }

    public void setProjList(Object projList) {
        this.projList = projList;
    }

    public Object getProjName() {
        return projName;
    }

    public void setProjName(Object projName) {
        this.projName = projName;
    }

}
