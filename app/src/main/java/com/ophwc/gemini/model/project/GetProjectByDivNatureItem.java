package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;

import java.util.List;

public class GetProjectByDivNatureItem {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("natureOfProject")
    @Expose
    private List<NatureOfProject> natureOfProject = null;

    @SerializedName("userId")
    @Expose
    private int userId;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetProjectByDivNatureItem() {
    }

    /**
     *
     * @param id
     * @param natureOfProject
     */
    public GetProjectByDivNatureItem(int id, List<NatureOfProject> natureOfProject, int userId) {
        super();
        this.id = id;
        this.natureOfProject = natureOfProject;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<NatureOfProject> getNatureOfProject() {
        return natureOfProject;
    }

    public void setNatureOfProject(List<NatureOfProject> natureOfProject) {
        this.natureOfProject = natureOfProject;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}