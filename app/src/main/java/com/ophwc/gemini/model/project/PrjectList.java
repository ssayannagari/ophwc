package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;

import java.util.List;

public class PrjectList {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("divisions")
    @Expose
    private ProjectDivisionsList divisions;
   /* @SerializedName("client")
    @Expose
    private Client client;
    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("createdUser")
    @Expose
    private CreatedUser createdUser;
    @SerializedName("assignTo")
    @Expose
    private AssignTo assignTo;
    @SerializedName("projectName")*/
    @Expose
    private String projectName;
    @SerializedName("projectLocation")
    @Expose
    private String projectLocation;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("langitude")
    @Expose
    private String langitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("projectLogo")
    @Expose
    private Object projectLogo;
    @SerializedName("comments")
    @Expose
    private Object comments;

    @SerializedName("imagesPaths")
    @Expose
    private List<String> imagesPaths = null;

    @SerializedName("natureOfProject")
    @Expose
    private NatureWorkList natureOfProject;
    @SerializedName("projectDefination")
    @Expose
    private String projectDefination;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProjectDivisionsList getDivisions() {
        return divisions;
    }

    public void setDivisions(ProjectDivisionsList divisions) {
        this.divisions = divisions;
    }

   /* public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }



    public CreatedUser getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(CreatedUser createdUser) {
        this.createdUser = createdUser;
    }

    public AssignTo getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(AssignTo assignTo) {
        this.assignTo = assignTo;
    }*/

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectLocation() {
        return projectLocation;
    }

    public void setProjectLocation(String projectLocation) {
        this.projectLocation = projectLocation;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getProjectLogo() {
        return projectLogo;
    }

    public void setProjectLogo(Object projectLogo) {
        this.projectLogo = projectLogo;
    }

    public Object getComments() {
        return comments;
    }

    public void setComments(Object comments) {
        this.comments = comments;
    }
    public List<String> getImagesPaths() {
        return imagesPaths;
    }

    public void setImagesPaths(List<String> imagesPaths) {
        this.imagesPaths = imagesPaths;
    }
    public NatureWorkList getNatureOfProject() {
        return natureOfProject;
    }

    public void setNatureOfProject(NatureWorkList natureOfProject) {
        this.natureOfProject = natureOfProject;
    }

    public String getProjectDefination() {
        return projectDefination;
    }

    public void setProjectDefination(String projectDefination) {
        this.projectDefination = projectDefination;
    }

}
