package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectDivisionsList {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("divisionName")
    @Expose
    private String divisionName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("completedProjects")
    @Expose
    private long completedProjects;
    @SerializedName("noOfProjects")
    @Expose
    private long noOfProjects;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProjectDivisionsList() {
    }

    /**
     *
     * @param id
     * @param completedProjects
     * @param status
     * @param divisionName
     * @param noOfProjects
     */
    public ProjectDivisionsList(long id, String divisionName, String status, long completedProjects, long noOfProjects) {
        super();
        this.id = id;
        this.divisionName = divisionName;
        this.status = status;
        this.completedProjects = completedProjects;
        this.noOfProjects = noOfProjects;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCompletedProjects() {
        return completedProjects;
    }

    public void setCompletedProjects(long completedProjects) {
        this.completedProjects = completedProjects;
    }

    public long getNoOfProjects() {
        return noOfProjects;
    }

    public void setNoOfProjects(long noOfProjects) {
        this.noOfProjects = noOfProjects;
    }

}