package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProjectsModel {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("prjectList")
    @Expose
    private List<PrjectList> prjectList = new ArrayList<>();

    @SerializedName("divisions")
    @Expose
    private ProjectDivisionsList divisions;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<PrjectList> getPrjectList() {
        return prjectList;
    }

    public void setPrjectList(List<PrjectList> prjectList) {
        this.prjectList = prjectList;
    }

    public ProjectDivisionsList getDivisions() {
        return divisions;
    }

    public void setDivisions(ProjectDivisionsList divisions) {
        this.divisions = divisions;
    }

}