package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaglocationItem {
    @SerializedName("projects")
    @Expose
    private Projects projects;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("langitude")
    @Expose
    private String langitude;

    public Projects getProjects() {
        return projects;
    }

    public void setProjects(Projects projects) {
        this.projects = projects;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

}