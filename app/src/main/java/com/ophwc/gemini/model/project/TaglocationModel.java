package com.ophwc.gemini.model.project;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaglocationModel {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("projectName")
    @Expose
    private String projectName;
    @SerializedName("projectLocation")
    @Expose
    private String projectLocation;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("langitude")
    @Expose
    private String langitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("projectLogo")
    @Expose
    private String projectLogo;
    @SerializedName("comments")
    @Expose
    private String comments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectLocation() {
        return projectLocation;
    }

    public void setProjectLocation(String projectLocation) {
        this.projectLocation = projectLocation;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProjectLogo() {
        return projectLogo;
    }

    public void setProjectLogo(String projectLogo) {
        this.projectLogo = projectLogo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}