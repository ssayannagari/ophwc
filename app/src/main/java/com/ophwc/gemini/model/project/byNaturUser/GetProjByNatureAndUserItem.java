package com.ophwc.gemini.model.project.byNaturUser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProjByNatureAndUserItem {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("userId")
    @Expose
    private int userId;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetProjByNatureAndUserItem() {
    }

    /**
     *
     * @param id
     * @param userId
     */
    public GetProjByNatureAndUserItem(long id, int userId) {
        super();
        this.id = id;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}