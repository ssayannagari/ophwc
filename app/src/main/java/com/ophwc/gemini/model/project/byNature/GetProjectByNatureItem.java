package com.ophwc.gemini.model.project.byNature;

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;
        import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;

public class GetProjectByNatureItem {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("natureOfProject")
    @Expose
    private List<NatureOfProject> natureOfProject = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetProjectByNatureItem() {
    }

    /**
     *
     * @param id
     * @param natureOfProject
     */
    public GetProjectByNatureItem(long id, List<NatureOfProject> natureOfProject) {
        super();
        this.id = id;
        this.natureOfProject = natureOfProject;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<NatureOfProject> getNatureOfProject() {
        return natureOfProject;
    }

    public void setNatureOfProject(List<NatureOfProject> natureOfProject) {
        this.natureOfProject = natureOfProject;
    }

}