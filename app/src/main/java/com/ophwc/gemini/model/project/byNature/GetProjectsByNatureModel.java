package com.ophwc.gemini.model.project.byNature;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.PrjectList;

import java.util.List;

public class GetProjectsByNatureModel {

    @SerializedName("statusCode")
    @Expose
    private long statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("prjectList")
    @Expose
    private List<PrjectList> prjectList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetProjectsByNatureModel() {
    }

    /**
     *
     * @param prjectList
     * @param statusCode
     * @param statusMessage
     */
    public GetProjectsByNatureModel(long statusCode, String statusMessage, List<PrjectList> prjectList) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.prjectList = prjectList;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<PrjectList> getPrjectList() {
        return prjectList;
    }

    public void setPrjectList(List<PrjectList> prjectList) {
        this.prjectList = prjectList;
    }

}