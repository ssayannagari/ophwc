package com.ophwc.gemini.model.project.byProjId;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProjectByIDModel{

        @SerializedName("id")
        @Expose
        private long id;
        @SerializedName("clientName")
        @Expose
        private Object clientName;

        @SerializedName("assignTo")
        @Expose
        private String assignTo;

        @SerializedName("projectName")
        @Expose
        private String projectName;
        @SerializedName("projectDefination")
        @Expose
        private String projectDefination;
        @SerializedName("projectLocation")
        @Expose
        private String projectLocation;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("langitude")
        @Expose
        private String langitude;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("projectLogo")
        @Expose
        private String projectLogo;
        @SerializedName("comments")
        @Expose
        private String comments;
        @SerializedName("imagesPaths")
        @Expose
        private List<String> imagesPaths = null;


        /**
         * No args constructor for use in serialization
         *
         */
        public GetProjectByIDModel() {
        }

        /**
         *
         * @param assignTo
         *
         * @param status
         * @param projectLocation
         *
         * @param imagesPaths
         * @param id
         * @param langitude
         * @param clientName
         * @param projectDefination
         * @param latitude
         * @param projectName
         * @param imagesList
         * @param comments
         * @param projectLogo
         *
         */
        public GetProjectByIDModel(long id, Object clientName, String assignTo,  String projectName, String projectDefination, String projectLocation, String latitude, String langitude, String status, String projectLogo, String comments, List<String> imagesPaths, List<Object> imagesList) {
            super();
            this.id = id;
            this.clientName = clientName;

            this.assignTo = assignTo;

            this.projectName = projectName;
            this.projectDefination = projectDefination;
            this.projectLocation = projectLocation;
            this.latitude = latitude;
            this.langitude = langitude;
            this.status = status;
            this.projectLogo = projectLogo;
            this.comments = comments;
            this.imagesPaths = imagesPaths;

        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Object getClientName() {
            return clientName;
        }

        public void setClientName(Object clientName) {
            this.clientName = clientName;
        }


        public String getAssignTo() {
            return assignTo;
        }

        public void setAssignTo(String assignTo) {
            this.assignTo = assignTo;
        }



        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectDefination() {
            return projectDefination;
        }

        public void setProjectDefination(String projectDefination) {
            this.projectDefination = projectDefination;
        }

        public String getProjectLocation() {
            return projectLocation;
        }

        public void setProjectLocation(String projectLocation) {
            this.projectLocation = projectLocation;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLangitude() {
            return langitude;
        }

        public void setLangitude(String langitude) {
            this.langitude = langitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getProjectLogo() {
            return projectLogo;
        }

        public void setProjectLogo(String projectLogo) {
            this.projectLogo = projectLogo;
        }

        public String getComments() {
            return comments;
        }

        public void setComments(String comments) {
            this.comments = comments;
        }

        public List<String> getImagesPaths() {
            return imagesPaths;
        }

        public void setImagesPaths(List<String> imagesPaths) {
            this.imagesPaths = imagesPaths;
        }


}