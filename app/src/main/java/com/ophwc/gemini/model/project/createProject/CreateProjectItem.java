package com.ophwc.gemini.model.project.createProject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.UserTypes;

public class CreateProjectItem {

    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("createdUser")
    @Expose
    private UserTypes createdUser;
    @SerializedName("natureOfProject")
    @Expose
    private NatureOfProject natureOfProject;
    @SerializedName("projectName")
    @Expose
    private String projectName;
    @SerializedName("projectLocation")
    @Expose
    private String projectLocation;
    @SerializedName("projectDefination")
    @Expose
    private String projectDefination;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("langitude")
    @Expose
    private String langitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("comments")
    @Expose
    private String comments;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateProjectItem() {
    }

    /**
     *
     * @param langitude
     * @param natureOfProject
     * @param status
     * @param projectLocation
     * @param projectDefination
     * @param divisions
     * @param latitude
     * @param projectName
     * @param comments
     * @param createdUser
     */
    public CreateProjectItem(Divisions divisions, UserTypes createdUser, NatureOfProject natureOfProject, String projectName, String projectLocation, String projectDefination, String latitude, String langitude, String status, String comments) {
        super();
        this.divisions = divisions;
        this.createdUser = createdUser;
        this.natureOfProject = natureOfProject;
        this.projectName = projectName;
        this.projectLocation = projectLocation;
        this.projectDefination = projectDefination;
        this.latitude = latitude;
        this.langitude = langitude;
        this.status = status;
        this.comments = comments;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    public UserTypes getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(UserTypes createdUser) {
        this.createdUser = createdUser;
    }

    public NatureOfProject getNatureOfProject() {
        return natureOfProject;
    }

    public void setNatureOfProject(NatureOfProject natureOfProject) {
        this.natureOfProject = natureOfProject;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectLocation() {
        return projectLocation;
    }

    public void setProjectLocation(String projectLocation) {
        this.projectLocation = projectLocation;
    }

    public String getProjectDefination() {
        return projectDefination;
    }

    public void setProjectDefination(String projectDefination) {
        this.projectDefination = projectDefination;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLangitude() {
        return langitude;
    }

    public void setLangitude(String langitude) {
        this.langitude = langitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}