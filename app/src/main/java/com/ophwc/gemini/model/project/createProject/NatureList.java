package com.ophwc.gemini.model.project.createProject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NatureList {
    @SerializedName("id")
@Expose
private long id;
        @SerializedName("projList")
        @Expose
        private List<ProjList> projList = null;

        /**
         * No args constructor for use in serialization
         *
         */
        public NatureList() {
        }

        /**
         *
         * @param id
         * @param projList
         */
        public NatureList(long id, List<ProjList> projList) {
            super();
            this.id = id;
            this.projList = projList;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public List<ProjList> getProjList() {
            return projList;
        }

        public void setProjList(List<ProjList> projList) {
            this.projList = projList;
        }

}