package com.ophwc.gemini.model.project.createProject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjList {

    @SerializedName("id")
    @Expose
    private long id;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProjList() {
    }

    /**
     *
     * @param id
     */
    public ProjList(long id) {
        super();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}