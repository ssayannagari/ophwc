package com.ophwc.gemini.model.project.createProject;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.UserTypes;

public class UpdateProjectItem {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("createdUser")
    @Expose
    private UserTypes createdUser;
    @SerializedName("natureOfProject")
    @Expose
    private NatureOfProject natureOfProject;
    @SerializedName("projectDefination")
    @Expose
    private String projectDefination;
    @SerializedName("projectName")
    @Expose
    private String projectName;
    @SerializedName("projectLocation")
    @Expose
    private String projectLocation;
    @SerializedName("comments")
    @Expose
    private String comments;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateProjectItem() {
    }

    /**
     *
     * @param id
     * @param natureOfProject
     * @param projectLocation
     * @param projectDefination
     * @param divisions
     * @param projectName
     * @param comments
     * @param createdUser
     */
    public UpdateProjectItem(long id, Divisions divisions, UserTypes createdUser, NatureOfProject natureOfProject, String projectDefination, String projectName, String projectLocation, String comments) {
        super();
        this.id = id;
        this.divisions = divisions;
        this.createdUser = createdUser;
        this.natureOfProject = natureOfProject;
        this.projectDefination = projectDefination;
        this.projectName = projectName;
        this.projectLocation = projectLocation;
        this.comments = comments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    public UserTypes getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(UserTypes createdUser) {
        this.createdUser = createdUser;
    }

    public NatureOfProject getNatureOfProject() {
        return natureOfProject;
    }

    public void setNatureOfProject(NatureOfProject natureOfProject) {
        this.natureOfProject = natureOfProject;
    }

    public String getProjectDefination() {
        return projectDefination;
    }

    public void setProjectDefination(String projectDefination) {
        this.projectDefination = projectDefination;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectLocation() {
        return projectLocation;
    }

    public void setProjectLocation(String projectLocation) {
        this.projectLocation = projectLocation;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


}
