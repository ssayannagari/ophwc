package com.ophwc.gemini.model.project.imageUpload;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteImageItem {

    @SerializedName("imagePath")
    @Expose
    private String imagePath;

    /**
     * No args constructor for use in serialization
     *
     */
    public DeleteImageItem() {
    }

    /**
     *
     * @param imagePath
     */
    public DeleteImageItem(String imagePath) {
        super();
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}