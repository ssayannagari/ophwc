package com.ophwc.gemini.model.project.imageUpload;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.PrjectList;

import java.util.List;

public class ImageUploadModel {

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;


    /**
     * No args constructor for use in serialization
     *
     */
    public ImageUploadModel() {
    }

    /**
     *

     * @param statusCode
     * @param statusMessage
     */
    public ImageUploadModel(Integer statusCode, String statusMessage) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;

    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }



}
