package com.ophwc.gemini.model.project.natureOfWork;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NatureOfProject {
    @SerializedName("id")
    @Expose
    private long id;

    /**
     * No args constructor for use in serialization
     *
     */
    public NatureOfProject() {
    }

    /**
     *
     * @param id
     */
    public NatureOfProject(long id) {
        super();
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}


