package com.ophwc.gemini.model.project.natureOfWork;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NatureOfWorkModel {
    @SerializedName("statusCode")
@Expose
private long statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("natureWorkList")
    @Expose
    private List<NatureWorkList> natureWorkList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public NatureOfWorkModel() {
    }

    /**
     *
     * @param statusCode
     * @param natureWorkList
     * @param statusMessage
     */
    public NatureOfWorkModel(long statusCode, String statusMessage, List<NatureWorkList> natureWorkList) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.natureWorkList = natureWorkList;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<NatureWorkList> getNatureWorkList() {
        return natureWorkList;
    }

    public void setNatureWorkList(List<NatureWorkList> natureWorkList) {
        this.natureWorkList = natureWorkList;
    }

}

