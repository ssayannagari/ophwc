package com.ophwc.gemini.model.project.natureOfWork;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.createProject.ProjList;

public class NatureWorkList {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("projList")
    @Expose
    private ProjList projList;

    /**
     * No args constructor for use in serialization
     *
     */
    public NatureWorkList() {
    }

    /**
     *
     * @param id
     * @param status
     * @param projList
     * @param name
     */
    public NatureWorkList(long id, String name, String status, ProjList projList) {
        super();
        this.id = id;
        this.name = name;
        this.status = status;
        this.projList = projList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getProjList() {
        return projList;
    }

    public void setProjList(ProjList projList) {
        this.projList = projList;
    }

}
