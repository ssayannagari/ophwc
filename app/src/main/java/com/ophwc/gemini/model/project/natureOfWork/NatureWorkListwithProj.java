package com.ophwc.gemini.model.project.natureOfWork;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NatureWorkListwithProj {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("projCount")
    @Expose
    private long projCount;
    @SerializedName("projList")
    @Expose
    private Object projList;

    /**
     * No args constructor for use in serialization
     *
     */
    public NatureWorkListwithProj() {
    }

    /**
     *
     * @param id
     * @param projCount
     * @param status
     * @param projList
     * @param name
     */
    public NatureWorkListwithProj(long id, String name, String status, long projCount, Object projList) {
        super();
        this.id = id;
        this.name = name;
        this.status = status;
        this.projCount = projCount;
        this.projList = projList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getProjCount() {
        return projCount;
    }

    public void setProjCount(long projCount) {
        this.projCount = projCount;
    }

    public Object getProjList() {
        return projList;
    }

    public void setProjList(Object projList) {
        this.projList = projList;
    }

}
