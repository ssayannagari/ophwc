package com.ophwc.gemini.model.project.natureOfWork;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NatureWorksWithProjCountModel {

    @SerializedName("statusCode")
    @Expose
    private long statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("natureWorkList")
    @Expose
    private List<NatureWorkListwithProj> natureWorkList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public NatureWorksWithProjCountModel() {
    }

    /**
     *
     * @param statusCode
     * @param natureWorkList
     * @param statusMessage
     */
    public NatureWorksWithProjCountModel(long statusCode, String statusMessage, List<NatureWorkListwithProj> natureWorkList) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.natureWorkList = natureWorkList;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<NatureWorkListwithProj> getNatureWorkList() {
        return natureWorkList;
    }

    public void setNatureWorkList(List<NatureWorkListwithProj> natureWorkList) {
        this.natureWorkList = natureWorkList;
    }

}