package com.ophwc.gemini.model.project.projectBudget;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectBudgetItem {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("fromDate")
    @Expose
    private String fromDate;
    @SerializedName("toDate")
    @Expose
    private String toDate;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProjectBudgetItem() {
    }

    /**
     * @param id
     * @param amount
     * @param comments
     * @param fromDate
     * @param toDate
     */
    public ProjectBudgetItem(int id, String amount, String comments, String fromDate, String toDate) {
        super();
        this.id = id;
        this.amount = amount;
        this.comments = comments;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getComments() {
        return comments;
    }
    public String getFromDate() {
        return fromDate;
    } public String getToDate() {
        return toDate;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    public void setFromDate(String fmDt) {
        this.fromDate = fmDt;
    }public void setToDate(String tmDt) {
        this.toDate = tmDt;
    }

}