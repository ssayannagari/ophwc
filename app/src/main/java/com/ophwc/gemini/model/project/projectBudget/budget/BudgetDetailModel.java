package com.ophwc.gemini.model.project.projectBudget.budget;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BudgetDetailModel {

@SerializedName("id")
@Expose
private int id;
@SerializedName("project")
@Expose
private Object project;
@SerializedName("createdDate")
@Expose
private long createdDate;
@SerializedName("fromDate")
@Expose
private long fromDate;
@SerializedName("toDate")
@Expose
private long toDate;
@SerializedName("amount")
@Expose
private String amount;
@SerializedName("comments")
@Expose
private String comments;
@SerializedName("strFromDate")
@Expose
private Object strFromDate;
@SerializedName("strToDate")
@Expose
private Object strToDate;
@SerializedName("totalAmnt")
@Expose
private String totalAmnt;

/**
* No args constructor for use in serialization
* 
*/
public BudgetDetailModel() {
}

/**
* 
* @param amount
* @param id
* @param project
* @param fromDate
* @param totalAmnt
* @param strFromDate
* @param strToDate
* @param toDate
* @param createdDate
* @param comments
*/
public BudgetDetailModel(int id, Object project, long createdDate, long fromDate, long toDate, String amount, String comments, Object strFromDate, Object strToDate, String totalAmnt) {
super();
this.id = id;
this.project = project;
this.createdDate = createdDate;
this.fromDate = fromDate;
this.toDate = toDate;
this.amount = amount;
this.comments = comments;
this.strFromDate = strFromDate;
this.strToDate = strToDate;
this.totalAmnt = totalAmnt;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public Object getProject() {
return project;
}

public void setProject(Object project) {
this.project = project;
}

public long getCreatedDate() {
return createdDate;
}

public void setCreatedDate(long createdDate) {
this.createdDate = createdDate;
}

public long getFromDate() {
return fromDate;
}

public void setFromDate(long fromDate) {
this.fromDate = fromDate;
}

public long getToDate() {
return toDate;
}

public void setToDate(long toDate) {
this.toDate = toDate;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getComments() {
return comments;
}

public void setComments(String comments) {
this.comments = comments;
}

public Object getStrFromDate() {
return strFromDate;
}

public void setStrFromDate(Object strFromDate) {
this.strFromDate = strFromDate;
}

public Object getStrToDate() {
return strToDate;
}

public void setStrToDate(Object strToDate) {
this.strToDate = strToDate;
}

public String getTotalAmnt() {
return totalAmnt;
}

public void setTotalAmnt(String totalAmnt) {
this.totalAmnt = totalAmnt;
}

}