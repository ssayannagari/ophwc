package com.ophwc.gemini.model.project.projectBudget.budget;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.floor.updatefloor.Project;

public class UpdateLastBudgetItem {

@SerializedName("id")
@Expose
private int id;
@SerializedName("project")
@Expose
private Project project;
@SerializedName("fromDate")
@Expose
private long fromDate;
@SerializedName("toDate")
@Expose
private long toDate;
@SerializedName("amount")
@Expose
private String amount;
@SerializedName("comments")
@Expose
private String comments;

/**
* No args constructor for use in serialization
* 
*/
public UpdateLastBudgetItem() {
}

/**
* 
* @param amount
* @param id
* @param project
* @param fromDate
* @param toDate
* @param comments
*/
public UpdateLastBudgetItem(int id, Project project, long fromDate, long toDate, String amount, String comments) {
super();
this.id = id;
this.project = project;
this.fromDate = fromDate;
this.toDate = toDate;
this.amount = amount;
this.comments = comments;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public Project getProject() {
return project;
}

public void setProject(Project project) {
this.project = project;
}

public long getFromDate() {
return fromDate;
}

public void setFromDate(long fromDate) {
this.fromDate = fromDate;
}

public long getToDate() {
return toDate;
}

public void setToDate(long toDate) {
this.toDate = toDate;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getComments() {
return comments;
}

public void setComments(String comments) {
this.comments = comments;
}

}