package com.ophwc.gemini.model.user;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.createProject.NatureList;
import com.ophwc.gemini.model.user.Divisions;

import java.util.ArrayList;
import java.util.List;

public class CreateUserItem {
    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("userTypes")
    @Expose
    private UserTypes userTypes;
    @SerializedName("natureList")
    @Expose
    private List<NatureList> natureList = null;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phoneNum")
    @Expose
    private String phoneNum;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateUserItem() {
    }

    /**
     *
     * @param lastName
     * @param emailId
     * @param phoneNum
     * @param username
     * @param status
     * @param userTypes
     * @param divisions
     * @param firstName
     * @param password
     * @param natureList
     */
    public CreateUserItem(Divisions divisions, UserTypes userTypes, List<NatureList> natureList, String username, String password, String firstName, String lastName, String phoneNum, String emailId, String status) {
        super();
        this.divisions = divisions;
        this.userTypes = userTypes;
        this.natureList = natureList;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.emailId = emailId;
        this.status = status;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    public UserTypes getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(UserTypes userTypes) {
        this.userTypes = userTypes;
    }

    public List<NatureList> getNatureList() {
        return natureList;
    }

    public void setNatureList(List<NatureList> natureList) {
        this.natureList = natureList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;

}

}