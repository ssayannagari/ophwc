package com.ophwc.gemini.model.user;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectIDList {

    @SerializedName("id")
    @Expose
    private int id;

    /**
     * No args constructor for use in serialization
     *
     */
    public ProjectIDList() {
    }

    /**
     *
     * @param id
     */
    public ProjectIDList(int id) {
        super();
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}