package com.ophwc.gemini.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.createProject.ProjList;

import java.util.List;

public class UserList {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("divisions")
    @Expose
    private Divisions divisions;
    @SerializedName("userTypes")
    @Expose
    private UserTypes userTypes;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phoneNum")
    @Expose
    private String phoneNum;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("userImage")
    @Expose
    private String userImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("natureList")
    @Expose
    private Object natureList;
    @SerializedName("projList")
    @Expose
    private List<PrjectList> projList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserList() {
    }

    /**
     *
     * @param id
     * @param lastName
     * @param emailId
     * @param phoneNum
     * @param username
     * @param status
     * @param projList
     * @param userTypes
     * @param userImage
     * @param divisions
     * @param firstName
     * @param password
     * @param natureList
     */
    public UserList(int id, Divisions divisions, UserTypes userTypes, String username, String password, String firstName, String lastName, String phoneNum, String emailId, String userImage, String status, Object natureList, List<PrjectList> projList) {
        super();
        this.id = id;
        this.divisions = divisions;
        this.userTypes = userTypes;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNum = phoneNum;
        this.emailId = emailId;
        this.userImage = userImage;
        this.status = status;
        this.natureList = natureList;
        this.projList = projList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Divisions getDivisions() {
        return divisions;
    }

    public void setDivisions(Divisions divisions) {
        this.divisions = divisions;
    }

    public UserTypes getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(UserTypes userTypes) {
        this.userTypes = userTypes;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getNatureList() {
        return natureList;
    }

    public void setNatureList(Object natureList) {
        this.natureList = natureList;
    }

    public List<PrjectList> getProjList() {
        return projList;
    }

    public void setProjList(List<PrjectList> projList) {
        this.projList = projList;
    }

}