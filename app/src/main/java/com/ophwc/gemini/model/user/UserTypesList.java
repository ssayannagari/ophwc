package com.ophwc.gemini.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserTypesList {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("typeName")
    @Expose
    private String typeName;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * No args constructor for use in serialization
     *
     */
    public UserTypesList() {
    }

    /**
     *
     * @param typeName
     * @param id
     * @param status
     */
    public UserTypesList(int id, String typeName, String status) {
        super();
        this.id = id;
        this.typeName = typeName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
