package com.ophwc.gemini.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsertypeModel {

    @SerializedName("statusCode")
    @Expose
    private long statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;
    @SerializedName("userTypesList")
    @Expose
    private List<UserTypesList> userTypesList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public UsertypeModel() {
    }

    /**
     *
     * @param statusCode
     * @param userTypesList
     * @param statusMessage
     */
    public UsertypeModel(long statusCode, String statusMessage, List<UserTypesList> userTypesList) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.userTypesList = userTypesList;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public List<UserTypesList> getUserTypesList() {
        return userTypesList;
    }

    public void setUserTypesList(List<UserTypesList> userTypesList) {
        this.userTypesList = userTypesList;
    }


}
