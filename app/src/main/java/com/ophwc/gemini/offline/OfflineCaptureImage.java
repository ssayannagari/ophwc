package com.ophwc.gemini.offline;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.imageUpload.ImageUploadModel;
import com.ophwc.gemini.projects.CameraUtils;
import com.ophwc.gemini.projects.CaptureProjectStatus;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.quickblox.sample.groupchatwebrtc.activities.BaseActivity;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OfflineCaptureImage extends BaseActivity {

    // Activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int FROM_GALLERY_REQUEST_CODE = 101;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    // key to store image path in savedInstance state
    public static final String KEY_IMAGE_STORAGE_PATH = "image_path";

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    // Bitmap sampling size
    public static final int BITMAP_SAMPLE_SIZE = 1;

    // Gallery directory name to store the images or videos
    public static final String GALLERY_DIRECTORY_NAME = "Hello Camera";

    // Image and Video file extensions
    public static final String IMAGE_EXTENSION = "jpg";
    public static final String VIDEO_EXTENSION = "mp4";
    public static final int REQUEST_PERMISSIONS = 102;

    private static String imageStoragePath;

    private TextView txtDescription;
    private ImageView imgPreview;
    private VideoView videoPreview;
    private Button btnCapturePicture, btnRecordVideo, btnFromGallery;
    Bitmap stampedBitmap = null;
    boolean isCaptured = false;
    Uri fileUri = null;
    private EditText edtProjectStatus, edtProjectComments;
    int PERMISSION_ALL = 1;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private String mLastUpdateTime;
    private Boolean mRequestingLocationUpdates;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_project_status);
        //Toolbar toolbar = findViewById(R.id.capturetoolbar);
        //setSupportActionBar(toolbar);

        // Checking availability of the camera
        Toolbar tagLoctoolbar = (Toolbar) findViewById(R.id.capturetoolbar);
        setSupportActionBar(tagLoctoolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle("Update Project ");
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        if (!CameraUtils.isDeviceSupportCamera(getApplicationContext())) {
            Toast.makeText(getApplicationContext(),
                    "Sorry! Your device doesn't support camera",
                    Toast.LENGTH_LONG).show();
            // will close the app if the device doesn't have camera
            finish();
        }

        txtDescription = findViewById(R.id.txt_desc);
        imgPreview = findViewById(R.id.imgPreview);
        videoPreview = findViewById(R.id.videoPreview);
        btnCapturePicture = findViewById(R.id.btnCapturePicture);
        btnRecordVideo = findViewById(R.id.btnRecordVideo);
        btnFromGallery = findViewById(R.id.btnFromGallery);
        edtProjectComments = findViewById(R.id.edtProjectComments);
        edtProjectStatus = findViewById(R.id.edtProjectStatus);
        edtProjectComments.setVisibility(View.GONE);
        edtProjectStatus.setVisibility(View.GONE);
        toggleButtons(isCaptured);
        checkForPermissions();
        btnRecordVideo.setText("Save Image");

        /**
         * Capture image on button click
         */
        btnFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFromGallery();
            }
        });
        btnCapturePicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                captureImage();
               /* if (CameraUtils.checkPermissions(getApplicationContext())) {

                } else {
                    requestCameraPermission(MEDIA_TYPE_IMAGE);
                }*/
            }
        });

        /**
         * Record video on button click
         */
        btnRecordVideo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (stampedBitmap == null) {
                    Toast.makeText(OfflineCaptureImage.this, "No Image available to upload ", Toast.LENGTH_SHORT).show();

                    return;
                }
               /* if(edtProjectStatus.getText().toString().equals("")){
                    //Toast.makeText(CaptureProjectStatus.this,"Please enter project status",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(edtProjectComments.getText().toString().equals("")){
                    Toast.makeText(CaptureProjectStatus.this,"Please enter comment",Toast.LENGTH_SHORT).show();

                    return;
                }*/

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                stampedBitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                byte[] byteArray = stream.toByteArray();
                btnRecordVideo.setEnabled(false);
                btnRecordVideo.setFocusable(false);
               // uploadImage(byteArray);

                QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(OfflineCaptureImage.this);
                qbUsersDbManager.addOfflineImage(getIntent().getIntExtra("projId", -1),
                        OPHWCSharedPrefs.getInstance(OfflineCaptureImage.this).getDivisionId(),
                        byteArray,imageStoragePath);
                finish();
             /*   if (CameraUtils.checkPermissions(getApplicationContext())) {
                    //captureVideo();
                } else {
                    requestCameraPermission(MEDIA_TYPE_VIDEO);





                }*/
            }
        });

        // restoring storage image path from saved instance state
        // otherwise the path will be null on device rotation
        restoreFromBundle(savedInstanceState);
    }


    private void checkForPermissions() {

        if (ContextCompat.checkSelfPermission(OfflineCaptureImage.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (OfflineCaptureImage.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(OfflineCaptureImage.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(OfflineCaptureImage.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            init();
            startLocationUpdates();
            //}


        }
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                //updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("TAG", "All location settings are satisfied.");

                        // Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("TAG", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(OfflineCaptureImage.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("TAG", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("TAG", errorMessage);

                                Toast.makeText(OfflineCaptureImage.this, errorMessage, Toast.LENGTH_LONG).show();
                        }


                    }
                });
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }

    private void toggleButtons(boolean isCaptured) {
        if (isCaptured) {
            btnCapturePicture.setVisibility(View.GONE);
            btnFromGallery.setVisibility(View.GONE);
            btnRecordVideo.setVisibility(View.VISIBLE);
        } else {
            btnCapturePicture.setVisibility(View.VISIBLE);
            btnFromGallery.setVisibility(View.VISIBLE);
            btnRecordVideo.setVisibility(View.GONE);
        }
    }

    /**
     * Restoring store image path from saved instance state
     */
    private void restoreFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_IMAGE_STORAGE_PATH)) {
                imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
                if (!TextUtils.isEmpty(imageStoragePath)) {
                    if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + IMAGE_EXTENSION)) {
                        previewCapturedImage();
                    } else if (imageStoragePath.substring(imageStoragePath.lastIndexOf(".")).equals("." + VIDEO_EXTENSION)) {
                        previewVideo();
                    }
                }
            }
        }
    }

    /**
     * Requesting permissions using Dexter library
     */
    private void requestCameraPermission(final int type) {
        captureImage();
    }


    /**
     * Capturing Camera Image will launch camera app requested image capture
     */

    private void uploadFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        // intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Complete action using"), FROM_GALLERY_REQUEST_CODE);
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Saving stored image path to saved instance state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putString(KEY_IMAGE_STORAGE_PATH, imageStoragePath);
    }

    /**
     * Restoring image path from saved instance state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        imageStoragePath = savedInstanceState.getString(KEY_IMAGE_STORAGE_PATH);
    }

    /**
     * Launching camera app to record video
     */
   /* private void captureVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file

        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }*/

    /**
     * Activity result method will be called after closing the camera
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

                // successfully captured the image
                // display it in image view

                //previewCapturedImage();
                getCustomNote(CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

                // video successfully recorded
                // preview the recorded video
                previewVideo();
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to record video
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == FROM_GALLERY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Uri selectedImageUri1 = data.getData();
                imageStoragePath = getPath(selectedImageUri1);
                // Refreshing the gallery
                CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);

                // successfully captured the image
                // display it in image view
                getCustomNote(FROM_GALLERY_REQUEST_CODE);
                //  previewGallerImage(imageStoragePath);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    /**
     * Display image from gallery
     */
    public Bitmap drawText(String text, int textWidth, int color) {
        int finalHeight = getTextHeight(text.length());
        int finalWidth = getTextWidth(text.length());


        Bitmap b = Bitmap.createBitmap(finalWidth, finalHeight, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        //textPaint.setStrokeWidth(2f);
        textPaint.setColor(Color.parseColor("#E5EBE9E9"));
        textPaint.setTextSize(50f);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint, finalWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(50f);
        paint.setColor(Color.parseColor("#12ECECEC"));
        Log.e("text0",text.length()+"");


        // Draw background

        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }

    private int getTextWidth(int length) {
        if (length < 100) {
            return 1200;

        } else if (length < 120) {
            return 1300;

        } else if (length < 150) {
            return 1350;

        } else if (length < 170) {
            return 1400;

        } else {
            return 1400;
        }
    }

    private int getTextHeight(int length) {
        if(length<100){
            return 480;

        }else if(length<120){
            return 500;

        }else if(length<150){
            return 530;

        }else if(length<175){
            return 550;

        }else if(length<200){
            return 650;

        }else if(length<250){
            return 680;

        }else{
            return 700                                                                                                                                                                                                                                                                                                                ;
        }
    }

    public Bitmap combineImages(Bitmap background, Bitmap foreground, int length,int code) {

        int width = 0, height = 0;
        Bitmap cs;

        System.gc();

        width = background.getWidth();
        height =background.getHeight();
        try{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(code==CAMERA_CAPTURE_IMAGE_REQUEST_CODE){
                    foreground.setHeight(getTextHeight(tLenght));
                }else{
                    foreground.setHeight(250);
                }
            }}catch (Exception e){}

        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas comboImage = new Canvas(cs);
        // Bitmap background1 = Bitmap.createScaledBitmap(background, width, height, true);
        comboImage.drawBitmap(background, 0, 0, null);
        background.recycle();
        background=null;
        if(length<120) {
            //comboImage.drawBitmap(foreground, background.getWidth()/64 , (background.getHeight() * 5) / 6, null);
            comboImage.drawBitmap(foreground , 0 , comboImage.getHeight()-foreground.getHeight() , null);
        }else{
            comboImage.drawBitmap(foreground , 0 , comboImage.getHeight()-foreground.getHeight() , null);

            // comboImage.drawBitmap(foreground, background.getWidth()/64 , (background.getHeight() * 4) / 5, null);

        }
        comboImage.save();
        foreground.recycle();
        foreground =null;


        // comboImage.restore();

        return cs;
    }
    private Bitmap timestampItAndSave(int myRequestCode, Bitmap toEdit){
        System.gc();
        Bitmap dest = toEdit.copy(Bitmap.Config.ARGB_8888, true);
        toEdit.recycle();
        toEdit=null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
        String dateTime = sdf.format(Calendar.getInstance().getTime());
        // reading local time in the system
        String lat = getIntent().getStringExtra("lat");
        String lng = getIntent().getStringExtra("lng");
        if (mCurrentLocation != null) {
            lat = mCurrentLocation.getLatitude()+"";
            lng = mCurrentLocation.getLongitude()+"";
        }
        String projectName = getIntent().getStringExtra("projName")+"";
        String projectDef = getIntent().getStringExtra("projDef")+"";

        try {

            if(projectName.length() > 60){
                projectName = projectName.substring(0,55)+"...";
            }
            if(projectDef.length() > 60){
                projectDef = projectDef.substring(0,55)+"...";
            }
            if(customNote.length()>70){
                customNote = customNote.substring(0,65)+"...";
            }

        }catch (Exception e){

        }
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(OfflineCaptureImage.this);
        StringBuffer sb = new StringBuffer();
        sb.append("User name : "+ophwcSharedPrefs.getFullName());
        sb.append("\n");
        sb.append("Mobile No : "+ophwcSharedPrefs.getMobileNo());
        sb.append("\n");

        if (myRequestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            sb.append("Project: " + projectName);
            sb.append("\n");
            sb.append("Project: " + projectDef);
            sb.append("\n");
            sb.append("Time: " + dateTime);
            sb.append("\n");
            sb.append("Latitude: " + lat);
            sb.append("\n");
            sb.append("Longitude: " + lng);
            sb.append("\n");
            sb.append("Note: " + customNote);

        } else {
            sb.append("Note: " + customNote);

        }

        String textStr =sb.toString();
        tLenght =textStr.length();
        Log.e("TEXT0", tLenght+"");
        Bitmap foregroundbmp = drawText(textStr,850,Color.GRAY);

        Bitmap bitmapcombined = combineImages(dest,foregroundbmp,textStr.length(),myRequestCode);

        foregroundbmp.recycle();
        foregroundbmp=null;
        // int width=textStr.getMeasuredWidth();


      /*  try {
            dest.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/timestamped")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }*/
        return bitmapcombined;
    }
    String customNote="";
    int tLenght=120;

    public void getCustomNote(int cameraCode) {
        customNote = "Not available";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(OfflineCaptureImage.this);
        alertDialog.setTitle("Notes");
        alertDialog.setPositiveButton("SUBMIT", null);
        //sayWindows.setNegativeButton("cancel", null);
        alertDialog.setCancelable(false);
        //alertDialog.setMessage("Enter some notes");

        final EditText input = new EditText(OfflineCaptureImage.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setHint("Enter some note...");
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(70)});

        alertDialog.setView(input);
        final AlertDialog mAlertDialog = alertDialog.create();
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        customNote = input.getText().toString().trim();
                        if (customNote.equals("")) {
                            Toast.makeText(OfflineCaptureImage.this, "Please enter some note", Toast.LENGTH_SHORT).show();
                            //return;

                        } else if (customNote.length() > 70) {
                            Toast.makeText(OfflineCaptureImage.this, "Note should be less than 70 characters", Toast.LENGTH_SHORT).show();
                            // return;
                        } else {

                            if (cameraCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
                                previewCapturedImage();
                            else
                                previewGallerImage(imageStoragePath);

                            dialog.dismiss();

                        }
                    }
                });
            }
        });
        mAlertDialog.show();



       /* alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        customNote = "No comments";
                        if(cameraCode ==CAMERA_CAPTURE_IMAGE_REQUEST_CODE)
                            previewCapturedImage();
                        else
                            previewGallerImage(imageStoragePath);
                    }
                });*/

        // alertDialog.show();

        // return customNote;
    }

    private void previewCapturedImage() {
        try {
            // hide video preview
            txtDescription.setVisibility(View.GONE);
            videoPreview.setVisibility(View.GONE);

            imgPreview.setVisibility(View.VISIBLE);

            isCaptured = true;
            toggleButtons(isCaptured);

            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            //Bitmap bitmap1 = getRotationBitmap(imageStoragePath);
            File pictureFile = new File(imageStoragePath);
            if (pictureFile.exists()) {
                pictureFile.delete();
            }

            //imgPreview.setScaleType(ImageView.ScaleType.FIT_XY);
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                //Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                ExifInterface exif = new ExifInterface(pictureFile.toString());

                Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        bitmap = rotate(bitmap, 270);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        bitmap = rotate(bitmap, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        bitmap = rotate(bitmap, 90);
                        break;

                }
                fos.close();
            } catch (Exception e) {
            }
            stampedBitmap = timestampItAndSave(CAMERA_CAPTURE_IMAGE_REQUEST_CODE, bitmap);
            imgPreview.setImageBitmap(stampedBitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void previewGallerImage(String path) {
        try {
            // hide video preview
            txtDescription.setVisibility(View.GONE);
            videoPreview.setVisibility(View.GONE);

            imgPreview.setVisibility(View.VISIBLE);

            isCaptured = true;
            toggleButtons(isCaptured);


            Bitmap bitmap = CameraUtils.optimizeBitmap(BITMAP_SAMPLE_SIZE, imageStoragePath);
            //Bitmap bitmap1 = getRotationBitmap(imageStoragePath);


            //imgPreview.setScaleType(ImageView.ScaleType.FIT_XY);

            stampedBitmap = timestampItAndSave(FROM_GALLERY_REQUEST_CODE, bitmap);
            imgPreview.setImageBitmap(stampedBitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    private Bitmap getRotationBitmap(String imageStoragePath) {

        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageStoragePath, bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(imageStoragePath, opts);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imageStoragePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;

        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        Matrix matrix = new Matrix();
        matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);

        return rotatedBitmap;
    }

    /**
     * Displaying video in VideoView
     */
    private void previewVideo() {
        try {
            // hide image preview
            txtDescription.setVisibility(View.GONE);
            imgPreview.setVisibility(View.GONE);

            videoPreview.setVisibility(View.VISIBLE);
            videoPreview.setVideoPath(imageStoragePath);
            // start playing
            videoPreview.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    private void showPermissionsAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Permissions required!")
                .setMessage("Camera needs few permissions to work properly. Grant them in settings.")
                .setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        CameraUtils.openSettings(OfflineCaptureImage.this);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }


    private void uploadImage(byte[] imageBytes) {

        ProgressDialog progressDialog = new ProgressDialog(OfflineCaptureImage.this);
        progressDialog.setMessage("Uploading image...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Consts.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AppApi retrofitInterface = retrofit.create(AppApi.class);
        File file = new File(imageStoragePath);
        Log.d("PATH", "Filename " + file.getName());
        //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
        String ts = "";
        try {
            Long tsLong = System.currentTimeMillis() / 1000;
            ts = tsLong.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String divisionId = "";
        String projId = "";
        try {

        } catch (Exception e) {

        }
        OPHWCSharedPrefs osharedPrefsHelper = OPHWCSharedPrefs.getInstance(OfflineCaptureImage.this);

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), imageBytes);

        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), "OPHWC_PROJECT" + ts);
        RequestBody divId = RequestBody.create(MediaType.parse("text/plain"), getIntent().getStringExtra("divId"));
        //RequestBody divId = RequestBody.create(MediaType.parse("text/plain"), "1");
        RequestBody projectId = RequestBody.create(MediaType.parse("text/plain"), "" + getIntent().getIntExtra("projId", -1));
        //RequestBody projectId = RequestBody.create(MediaType.parse("text/plain"), ""+1);
        //RequestBody consStatus = RequestBody.create(MediaType.parse("text/plain"), edtProjectStatus.getText().toString());
        RequestBody consStatus = RequestBody.create(MediaType.parse("text/plain"), "50");
        RequestBody createdUser = RequestBody.create(MediaType.parse("text/plain"), "" + osharedPrefsHelper.getUserId());
        //RequestBody createdUser = RequestBody.create(MediaType.parse("text/plain"), ""+1);
        // RequestBody comments = RequestBody.create(MediaType.parse("text/plain"), edtProjectComments.getText().toString());
        RequestBody comments = RequestBody.create(MediaType.parse("text/plain"), "Comments");
        Log.e("PARAM1", getIntent().getStringExtra("divId") + " " + getIntent().getIntExtra("projId", -1) + " " + osharedPrefsHelper.getUserId() + " " + createdUser + " " + comments);
        Call<ImageUploadModel> call = retrofitInterface.uploadImage(body, divId, projectId, consStatus, createdUser, comments);
        //mProgressBar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<ImageUploadModel>() {
            @Override
            public void onResponse(Call<ImageUploadModel> call, Response<ImageUploadModel> response) {
                btnRecordVideo.setEnabled(true);
                btnRecordVideo.setFocusable(true);

                //mProgressBar.setVisibility(View.GONE);
                //Toast.makeText(CaptureProjectStatus.this,"Success "+response.code(),Toast.LENGTH_SHORT).show();

                if (response.code() == 200) {
                    Toast.makeText(OfflineCaptureImage.this, "Image uploaded successfully", Toast.LENGTH_SHORT).show();

                    ImageUploadModel responseBody = response.body();
                    Gson gson = new Gson();
                    String json = gson.toJson(response);

                    Log.d("check", json.toString());
                    finish();
                    startActivity(new Intent(OfflineCaptureImage.this, ImagesActivity.class)
                            .putExtra("projId", getIntent().getIntExtra("projId", -1))
                            .putExtra("divId", getIntent().getStringExtra("divId"))
                            .putExtra("projName", getIntent().getStringExtra("projName"))
                            .putExtra("lat", getIntent().getStringExtra("lat"))
                            .putExtra("lng", getIntent().getStringExtra("lng")));
                    //// mBtImageShow.setVisibility(View.VISIBLE);
                    //mImageUrl = URL + responseBody.getPath();
                    //Snackbar.make(findViewById(R.id.content), responseBody.getMessage(),Snackbar.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(OfflineCaptureImage.this, "Failed to upload image, try again.", Toast.LENGTH_SHORT).show();

                   /* ResponseBody errorBody = response.errorBody();

                    Gson gson = new Gson();

                    try {

                        Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        //Snackbar.make(findViewById(R.id.content), errorResponse.getMessage(),Snackbar.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
                if (progressDialog != null)
                    progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ImageUploadModel> call, Throwable t) {

                // mProgressBar.setVisibility(View.GONE);


                if (progressDialog != null)
                    progressDialog.dismiss();
                Log.d("Image", "onFailure: " + t.getLocalizedMessage());
            }

        });


    }


}