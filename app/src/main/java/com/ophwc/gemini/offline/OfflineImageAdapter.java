package com.ophwc.gemini.offline;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ophwc.gemini.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OfflineImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<byte[]> mImageLists;
    ImageLoader imageLoader;
    public OfflineImageAdapter(Context context, List<byte[]> imagesList) {
        this.mContext = context;
        this.mImageLists = imagesList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        // imageLoader.handleSlowNetwork(true);


    }

    @Override
    //public int getCount() {
    //return mImageLists.size();
    // }
    public int getCount() {
        return mImageLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflator =(LayoutInflater.from(mContext));
        if (convertView==null)
            convertView =inflator.inflate(R.layout.images_grid_row,null);
        ImageView imageGridRow = (ImageView)convertView.findViewById(R.id.imageGridRow);
        try {
            //  String imagePath ="http://164.100.141.155:8080/"+mImageLists.get(position);
            //String imagePath =Consts.BASE_URL+"/"+mImageLists.get(position);


            byte[] imagePath =mImageLists.get(position);
            //Log.e("Image",imagePath);
           Bitmap bmp =  BitmapFactory.decodeByteArray(imagePath, 0, imagePath.length);

            imageGridRow.setImageBitmap(bmp);
            //imageLoader.displayImage(imagePath,imageGridRow);

        }catch (Exception e){
            e.printStackTrace();
        }

        // tvRowDivName.setText("Division \n"+mImageLists.get(position));
        //tvRowProjStatus.setText("Projects "+mImageLists.get(position).getCompletedProjects()+"/"+mDivisionLists.get(position).getNoOfProjects());



        return convertView;
        //return null;
    }
}
