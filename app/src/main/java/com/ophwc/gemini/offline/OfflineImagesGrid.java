package com.ophwc.gemini.offline;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.byProjId.GetProjectByIDModel;
import com.ophwc.gemini.projects.CaptureProjectStatus;
import com.ophwc.gemini.projects.gallery.AllImagesGridActivity;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.projects.gallery.ImagesAdapter;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OfflineImagesGrid extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    SharedPrefsHelper sharedPrefsHelper;
    GridView gridView;
    FloatingActionButton addImagefab;
    List<byte[]> imagesList=new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    OPHWCSharedPrefs ophwcSharedPrefs;
    PrjectList prjectLists = new PrjectList();
    TextView tvNoImagesPager;
    AlertDialog alertDialog;
    GetProjectByIDModel getProjectByIDModel;
    List<offlineProjObj> offlineImageslist = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_proj_nfo, menu);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(OfflineImagesGrid.this);
       /* if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=0){
            menu.findItem(R.id.menu_delete_image).setVisible(false);

        }else{
            menu.findItem(R.id.menu_delete_image).setVisible(true);

        }*/
        menu.findItem(R.id.menu_delete_image).setVisible(false);
        menu.findItem(R.id.menu_share_image).setVisible(false);
        menu.findItem(R.id.menu_projInfo).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_projInfo:
               // showAlertDialog();
                break;
            case R.id.menu_delete_image:
                //showDeleteAlertDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_images_grid_layout);
        initialiseToolbar();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();

        gridView = (GridView) findViewById(R.id.gridViewImages);
        tvNoImagesPager = (TextView) findViewById(R.id.tvNoImagesPager);
        addImagefab = (FloatingActionButton) findViewById(R.id.addImagefab);

        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(OfflineImagesGrid.this);
        if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=0&&sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=4){
            addImagefab.setVisibility(View.GONE);
        }else{
            addImagefab.setVisibility(View.VISIBLE);
        }
        addImagefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isGpsEnabled()) {
                    startCaptureActivity();
                }else{
                    setEnableLocation();
                }

            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //divisionLists.
              /*  startActivity(new Intent(OfflineImagesGrid.this,ImagesActivity.class).putExtra("projId",getIntent().getIntExtra("projId",0))
                        .putExtra("imagePos",position));*/
              Toast.makeText(OfflineImagesGrid.this,"Full view is not available in offline",Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseToolbar();
        //getAllImagesService();
        getAllImagesByProjectService();
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.imagestoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("projName")+" Images");




    }
    @Override
    public void onBackPressed() {

    }

    private void getAllImagesByProjectService() {
        int projId = getIntent().getIntExtra("projId",0);
        imagesList.clear();
        QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(OfflineImagesGrid.this);

        try {
            offlineImageslist = qbUsersDbManager.getOfflineImage();
            if (offlineImageslist != null) {
             for(offlineProjObj offlineProjObj :offlineImageslist){
                 if(offlineProjObj.getProjId()==projId){
                     imagesList.add(offlineProjObj.getOfflineImage());
                 }
             }
             if(imagesList.size()>0){
                 setAdapters();
             }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    private void setAdapters() {
        try {
            if(imagesList!=null&&imagesList.size()>0) {
                tvNoImagesPager.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);

                gridView.setAdapter(new OfflineImageAdapter(OfflineImagesGrid.this, imagesList));

            }else{
                gridView.setVisibility(View.GONE);
                tvNoImagesPager.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            gridView.setVisibility(View.GONE);
            tvNoImagesPager.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public boolean isGpsEnabled(){
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            return gps_enabled;
        } catch(Exception ex) {
            return false;
        }


    }
    public void setEnableLocation(){
        LocationRequest mLocationRequest;
        LocationSettingsRequest mLocationSettingsRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        builder.setAlwaysShow(true);
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 4, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(googleApiClient, mLocationRequest, this);
        }catch (Exception e){}

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi
                        .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied.
                        // You can initialize location requests here.
                   /* int permissionLocation = ContextCompat
                            .checkSelfPermission(AllImagesGridActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                      Location mCurrentLocation = LocationServices.FusedLocationApi
                                .getLastLocation(googleApiClient);
                    }*/
                        startCaptureActivity();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied.
                        // But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            // Ask to turn on GPS automatically
                            status.startResolutionForResult(OfflineImagesGrid.this,
                                    1201);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied.
                        // However, we have no way
                        // to fix the
                        // settings so we won't show the dialog.
                        // finish();
                        break;
                }
            }
        });
    }

    private void startCaptureActivity() {
        startActivity(new Intent(OfflineImagesGrid.this, OfflineCaptureImage.class)
                .putExtra("projId", getIntent().getIntExtra("projId",-1))
                .putExtra("divId", ""+ophwcSharedPrefs.getDivisionId())
                .putExtra("projName", getIntent().getStringExtra("projName"))
                .putExtra("projDef", getIntent().getStringExtra("projDef"))

                .putExtra("lat", getIntent().getStringExtra("lat"))
                .putExtra("lng", getIntent().getStringExtra("lng")));
    }
}

