package com.ophwc.gemini.offline;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.ophwc.gemini.R;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.projects.ProjectsListAdapter;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class OfflineProjectsActivity extends AppCompatActivity {

    List<PrjectList> projectList = new ArrayList<>();
    private static List<PrjectList> UnmuteprojectList = new ArrayList<>();
    ListView projectListView;
    SearchView edtProjSearch ;
    ProjectsListAdapter projectsListAdapter;
    LinearLayout lnrNoProjects;
    TextView tvNoProjectsAvailable;
    QbUsersDbManager qbUsersDbManager;

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("State","onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("State","onstop()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyBoard();

        LoadProjectsfromDB();
        Log.e("State","onResume()");
    }
    private void LoadProjectsfromDB() {
        projectList.clear();
        UnmuteprojectList.clear();
        projectList = qbUsersDbManager.getAllProjects();
        if(projectList!=null&&projectList.size()>0) {

            UnmuteprojectList = projectList;


            setAdapters("");
        }else{

        }
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("State","onCreate()");
        setContentView(R.layout.activity_projects_list);

        qbUsersDbManager = QbUsersDbManager.getInstance(OfflineProjectsActivity.this);

        projectListView = (ListView) findViewById(R.id.projects_list_view);
        edtProjSearch = (SearchView) findViewById(R.id.edtProjSearch);
        lnrNoProjects = (LinearLayout) findViewById(R.id.lnrNoProjects);
        tvNoProjectsAvailable = (TextView) findViewById(R.id.tvNoProjectsAvailable);

        edtProjSearch.setQueryHint("Search projects");
        edtProjSearch.setIconifiedByDefault(false);

        edtProjSearch.clearFocus();
        hideKeyBoard();
        edtProjSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                filter(text);
                return false;
            }
        });

        Toolbar projectstoolbar = (Toolbar) findViewById(R.id.projectstoolbar);
        setSupportActionBar(projectstoolbar);
        ActionBar actionBar = getSupportActionBar();

        getSupportActionBar().setTitle("Offline Projects");




    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        //if(imm.isActive())
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }




    private void setAdapters(String text) {
        if(projectList!=null) {
            if(projectList.size()>0) {
                projectListView.setVisibility(View.VISIBLE);
                lnrNoProjects.setVisibility(View.GONE);
                projectsListAdapter = new ProjectsListAdapter(getBaseContext(), projectList, text);
                projectListView.setAdapter(projectsListAdapter);
            }else{
                projectListView.setVisibility(View.GONE);
                lnrNoProjects.setVisibility(View.VISIBLE);
                tvNoProjectsAvailable.setText("No projects available");
            }
        }else{
            projectListView.setVisibility(View.GONE);
            lnrNoProjects.setVisibility(View.VISIBLE);
            tvNoProjectsAvailable.setText("No projects available");
        }
    }



    // Filter Class
    public void filter(String charText) {
        UnmuteprojectList.size();
        //  projectList.clear();
        charText = charText.toLowerCase(Locale.getDefault());
        setAdapters(charText);



        //projectsListAdapter.notifyDataSetChanged();
    }
}
