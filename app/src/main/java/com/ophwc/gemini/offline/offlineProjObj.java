package com.ophwc.gemini.offline;

public class offlineProjObj {
    String projectName,projectDef, projectLoc;
    int projId,divisionId,imageId;
    byte[] offlineImage;

    public offlineProjObj(String name,String def, String loc, int projid){
        this.projectDef = def;
        this.projectLoc = loc;
        this.projectName = name;
        this.projId = projid;

    }
    public offlineProjObj(int imgId,int projid,int divId, byte[] imagebytes, String imageName){
        this.projId = projid;
        this.imageId = imgId;
        this.offlineImage = imagebytes;
        this.projectName = imageName;
        this.divisionId = divId;
    }

    public int getDivisionId() {
        return divisionId;
    }

    public int getImageId() {
        return imageId;
    }

    public int getProjId() {
        return projId;
    }

    public byte[] getOfflineImage() {
        return offlineImage;
    }

    public String getProjectDef() {
        return projectDef;
    }

    public String getProjectLoc() {
        return projectLoc;
    }

    public String getProjectName() {
        return projectName;
    }
}
