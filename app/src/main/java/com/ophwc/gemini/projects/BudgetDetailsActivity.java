package com.ophwc.gemini.projects;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.App;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.floor.FloorDetailsItem;
import com.ophwc.gemini.model.floor.updatefloor.Project;
import com.ophwc.gemini.model.project.projectBudget.ProjectBudgetItem;
import com.ophwc.gemini.model.project.projectBudget.budget.BudgetDetailModel;
import com.ophwc.gemini.model.project.projectBudget.budget.UpdateLastBudgetItem;
import com.ophwc.gemini.model.user.update.UserUpdateModel;
import com.ophwc.gemini.projects.gallery.AllImagesGridActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BudgetDetailsActivity extends AppCompatActivity {
    String projBudget = "";
    String budgetDetails = "";
    Calendar myCal = Calendar.getInstance();
    Calendar toCal = Calendar.getInstance();
    long fromMillis =0, toMillis = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.budgetdetailsactivity);
        callBudgetDetailsService();
    }

    private void callUpdateBudgetService() {
        ProgressDialog progressDialog = new ProgressDialog(BudgetDetailsActivity.this);
        progressDialog.setMessage("Updating details...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectBudgetItem pbi = new ProjectBudgetItem();
        pbi.setAmount(projBudget);
        pbi.setComments(budgetDetails);
        pbi.setFromDate(""+fromMillis);
        pbi.setToDate(""+toMillis);
        pbi.setId(getIntent().getIntExtra("projId", 0));


        Call<UserUpdateModel> call = service.updateProjectBudget(pbi);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UserUpdateModel>() {

            @Override
            public void onResponse(Call<UserUpdateModel> call, Response<UserUpdateModel> response) {

                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code + "");
                Log.d("usercheck", "" + json);
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        // TODO: Handle Success message

                        int resCode = response.body().getStatusCode();
                        Toast.makeText(BudgetDetailsActivity.this,"Successfully updated details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        finish();
                        break;
                    case 2:
                        Toast.makeText(BudgetDetailsActivity.this,"Unable to update details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        finish(); // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        Toast.makeText(BudgetDetailsActivity.this,"Unable to update details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        finish();  // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }


            }


            @Override
            public void onFailure(Call<UserUpdateModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Toast.makeText(BudgetDetailsActivity.this, "Unable to update details", Toast.LENGTH_SHORT).show();
                finish();
                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }
    private void callUpdateLastBudgetService(String edtlastBudget) {
        ProgressDialog progressDialog = new ProgressDialog(BudgetDetailsActivity.this);
        progressDialog.setMessage("Updating details...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        UpdateLastBudgetItem pbi = new UpdateLastBudgetItem();
        pbi.setAmount(edtlastBudget);
        pbi.setComments(lastComment);
        pbi.setFromDate(lastFromedateMilli);
        pbi.setToDate(lastTodateMilli);
        pbi.setId(lastBudgetId);
        Project project = new Project(getIntent().getIntExtra("projId", 0));
        pbi.setProject(project);


        Call<UserUpdateModel> call = service.updateLastBudgetDetails(pbi);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UserUpdateModel>() {

            @Override
            public void onResponse(Call<UserUpdateModel> call, Response<UserUpdateModel> response) {

                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code + "");
                Log.d("usercheck", "" + json);
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        // TODO: Handle Success message

                        int resCode = response.body().getStatusCode();
                        Toast.makeText(BudgetDetailsActivity.this,"Successfully updated details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        finish();
                        break;
                    case 2:
                        Toast.makeText(BudgetDetailsActivity.this,"Unable to update details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                         finish();
                        break;
                    // code other than 200
                    default:
                        Toast.makeText(BudgetDetailsActivity.this,"Unable to update details",Toast.LENGTH_LONG).show();

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        finish();
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }


            }


            @Override
            public void onFailure(Call<UserUpdateModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                finish();
                Toast.makeText(BudgetDetailsActivity.this, "Unable to update details", Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }
    public void getBudgetDetails() {
        myCal = Calendar.getInstance();
        toCal = Calendar.getInstance();
        fromMillis =0;
        toMillis = 0;
        projBudget = "";
        budgetDetails = "";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BudgetDetailsActivity.this);
        alertDialog.setTitle("Update Deatails");
        alertDialog.setPositiveButton("SUBMIT", null);
        alertDialog.setNegativeButton("Cancel", null);
        alertDialog.setCancelable(false);
        //alertDialog.setMessage("Enter some notes");

        LinearLayout layout = new LinearLayout(BudgetDetailsActivity.this);
        ScrollView scrollView = new ScrollView(BudgetDetailsActivity.this);
        LinearLayout textlayout = new LinearLayout(BudgetDetailsActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        textlayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(10, 25, 10, 10);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        params2.setMargins(0, 25, 0, 0);
        textlayout.setLayoutParams(params);
        final TextView tvlastTv = new TextView(BudgetDetailsActivity.this);
        final EditText tvlastAmount = new EditText(BudgetDetailsActivity.this);
        final TextView tvtotalAmount = new TextView(BudgetDetailsActivity.this);
        final TextView tvlastUpdate = new TextView(BudgetDetailsActivity.this);
        final Button btnUpdateLast = new Button(BudgetDetailsActivity.this);
        final EditText budget = new EditText(BudgetDetailsActivity.this);
        final EditText input = new EditText(BudgetDetailsActivity.this);
        final EditText fromDate = new EditText(BudgetDetailsActivity.this);
        final EditText toDate = new EditText(BudgetDetailsActivity.this);

        input.setHint("Enter some note...");
        btnUpdateLast.setText("Update amount");
        tvlastTv.setLayoutParams(params2);
        tvlastAmount.setHint("Enter last update amount...");
        tvlastAmount.setInputType(InputType.TYPE_CLASS_NUMBER);
        budget.setHint("Enter amount...");
        budget.setInputType(InputType.TYPE_CLASS_NUMBER);
        fromDate.setHint("Enter From Date...");
        toDate.setHint("Enter To Date...");
        tvlastTv.setHint("Last updated amount:");

        tvlastAmount.setTextSize(18f);
        tvtotalAmount.setTextSize(18f);
        tvlastUpdate.setTextSize(18f);

        budget.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        budget.setSingleLine(true);
        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
       // input.setSingleLine(true);

        //fromDate.setEnabled(false);
        fromDate.setFocusable(false);
        fromDate.setClickable(true);

        fromDate.setInputType(InputType.TYPE_NULL);
        fromDate.setTextIsSelectable(false);

        //toDate.setEnabled(false);
        toDate.setFocusable(false);
        toDate.setClickable(true);

        toDate.setTextIsSelectable(false);

        budget.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
        input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(200)});
        textlayout.addView(tvtotalAmount);
        textlayout.addView(tvlastTv);
        textlayout.addView(tvlastAmount);
        textlayout.addView(tvlastUpdate);
        textlayout.addView(btnUpdateLast);

        layout.addView(textlayout);
        layout.addView(budget);
        layout.addView(input);
        layout.addView(fromDate);
        layout.addView(toDate);
        scrollView.addView(layout);
        btnUpdateLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lastBudget.equals(tvlastAmount.getText().toString())){
                    callUpdateLastBudgetService(tvlastAmount.getText().toString());
                }
            }
        });
        alertDialog.setView(scrollView);
        String lastUpdateDateStr = "NA";
        if(lastUpdateddateMilli>0) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(lastUpdateddateMilli);
            lastUpdateDateStr = formatter.format(calendar.getTime());
        }

        tvtotalAmount.setText("Total amount : "+totalBudget);
        tvlastAmount.setText(""+lastBudget);
        tvlastUpdate.setText("Last updated on : "+lastUpdateDateStr);

        final AlertDialog mAlertDialog2 = alertDialog.create();
        mAlertDialog2.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                fromDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Get Current Date

                        int mYear = myCal.get(Calendar.YEAR);
                        int mMonth = myCal.get(Calendar.MONTH);
                        int mDay = myCal.get(Calendar.DAY_OF_MONTH);


                        DatePickerDialog datePickerDialog = new DatePickerDialog(BudgetDetailsActivity.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        int mnth = monthOfYear+1;

                                        fromDate.setText((dayOfMonth<10?"0"+dayOfMonth:dayOfMonth) + "-" +(mnth<10?"0"+ mnth:mnth )+ "-" + year);
                                        myCal.set(Calendar.YEAR, year);
                                        myCal.set(Calendar.MONTH, monthOfYear);
                                        myCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                        fromMillis = myCal.getTimeInMillis();
                                        toDate.setText("");
                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis()+ (1000 * 60 * 60));
                        datePickerDialog.show();
                    }

                });

                toDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int mYear = myCal.get(Calendar.YEAR);
                        int mMonth = myCal.get(Calendar.MONTH);
                        int mDay = myCal.get(Calendar.DAY_OF_MONTH);


                        DatePickerDialog datePickerDialog = new DatePickerDialog(BudgetDetailsActivity.this,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {
                                        int mnth = monthOfYear+1;

                                        toDate.setText((dayOfMonth<10?"0"+dayOfMonth:dayOfMonth) + "-" +(mnth<10?"0"+ mnth:mnth )+ "-" + year);

                                        toCal.set(Calendar.YEAR, year);
                                        toCal.set(Calendar.MONTH, monthOfYear);
                                        toCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                        toMillis = toCal.getTimeInMillis();
                                    }
                                }, mYear, mMonth, mDay);
                        Calendar toCall = Calendar.getInstance();
                        datePickerDialog.getDatePicker().setMinDate(myCal.getTimeInMillis());
                        //datePickerDialog.
                        int maxDay1 = myCal.getActualMaximum(Calendar.DAY_OF_MONTH);
                        /*toCall.set(Calendar.YEAR,myCal.getTime().getYear());
                        toCall.set(Calendar.MONTH,myCal.getTime().getMonth());
                        toCall.set(Calendar.DAY_OF_MONTH, maxDay1);*/
                        Date maxdate = new Date();
                        maxdate.setDate(maxDay1);
                        maxdate.setMonth(myCal.getTime().getMonth());
                        maxdate.setYear(myCal.getTime().getYear());
                        toCall.setTime(maxdate);
                        if((toCall.getTimeInMillis())<Calendar.getInstance().getTimeInMillis()) {

                            datePickerDialog.getDatePicker().setMaxDate(toCall.getTimeInMillis() + (1000 * 60 * 60));
                        }else{
                            datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis() + (1000 * 60 * 60));
                        }
                        if(fromMillis==0|| fromDate.getText().toString().trim().isEmpty()){
                            Toast.makeText(BudgetDetailsActivity.this,"Select From Date",Toast.LENGTH_LONG).show();
                        }else {
                            datePickerDialog.show();
                        }
                    }
                });
                Button b = mAlertDialog2.getButton(AlertDialog.BUTTON_POSITIVE);
                Button neg = mAlertDialog2.getButton(AlertDialog.BUTTON_NEGATIVE);
                neg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                    }
                });
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        projBudget = budget.getText().toString().trim();
                        budgetDetails = input.getText().toString().trim();
                        if (projBudget.equals("")) {
                            Toast.makeText(BudgetDetailsActivity.this, "Please enter amount", Toast.LENGTH_SHORT).show();
                            return;

                        } else if (budgetDetails.equals("")) {
                            Toast.makeText(BudgetDetailsActivity.this, "Please enter some note", Toast.LENGTH_SHORT).show();
                            return;

                        } else if (budgetDetails.length() > 200) {
                            Toast.makeText(BudgetDetailsActivity.this, "Note should be less than 70 characters", Toast.LENGTH_SHORT).show();
                            return;
                        }else if (fromMillis==0) {
                            Toast.makeText(BudgetDetailsActivity.this, "Please select From Date", Toast.LENGTH_SHORT).show();
                            return;
                        }else if (toMillis==0) {
                            Toast.makeText(BudgetDetailsActivity.this, "Please select To Date", Toast.LENGTH_SHORT).show();
                            return;
                        }  else {
                                callUpdateBudgetService();
                            dialog.dismiss();

                        }
                    }
                });

            }
        });
        mAlertDialog2.show();




    }
    int lastBudgetId =0;
    String totalBudget ="NA";
    String lastComment ="NA";
    String lastBudget = "NA";
    long lastUpdateddateMilli =0;
    long lastFromedateMilli= 0;
    long lastTodateMilli = 0;
    private void callBudgetDetailsService() {
        ProgressDialog progressDialog = new ProgressDialog(BudgetDetailsActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        FloorDetailsItem fdi = new FloorDetailsItem(getIntent().getIntExtra("projId",0));


        Call<BudgetDetailModel> call = service.getBudgetDetails(fdi);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<BudgetDetailModel>() {

            @Override
            public void onResponse(Call<BudgetDetailModel> call, Response<BudgetDetailModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        lastBudgetId = response.body().getId();
                        lastComment = response.body().getComments();
                        totalBudget = response.body().getTotalAmnt()==null?"0":response.body().getTotalAmnt();
                        lastBudget = response.body().getAmount()==null?"0":response.body().getAmount();
                        lastUpdateddateMilli = response.body().getCreatedDate()==0?0:response.body().getCreatedDate();
                        lastFromedateMilli = response.body().getFromDate()==0?0:response.body().getFromDate();
                        lastTodateMilli = response.body().getToDate()==0?0:response.body().getToDate();
                        getBudgetDetails();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<BudgetDetailModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(BudgetDetailsActivity.this,"Budget details not available",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);
                getBudgetDetails();
            }

        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
