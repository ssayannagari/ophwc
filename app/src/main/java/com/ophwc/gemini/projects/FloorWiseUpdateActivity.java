package com.ophwc.gemini.projects;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.google.gson.Gson;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.floor.FloorDetailsItem;
import com.ophwc.gemini.model.floor.FloorDetailsModel;
import com.ophwc.gemini.model.floor.FloorWorkDetailsByFloorModel;
import com.ophwc.gemini.model.floor.FloorWorksList;
import com.ophwc.gemini.model.floor.FloorsList;
import com.ophwc.gemini.model.floor.ProjectStatusList;
import com.ophwc.gemini.model.floor.updatefloor.Floor;
import com.ophwc.gemini.model.floor.updatefloor.FloorDetailsbyFloorIDItem;
import com.ophwc.gemini.model.floor.updatefloor.FloorWork;
import com.ophwc.gemini.model.floor.updatefloor.Project;
import com.ophwc.gemini.model.floor.updatefloor.ProjectFloorstatus;
import com.ophwc.gemini.model.floor.updatefloor.ProjectStatus;
import com.ophwc.gemini.model.floor.updatefloor.UpdateFloorDetailsModel;
import com.ophwc.gemini.model.floor.updatefloor.UpdateFloorItem;

import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FloorWiseUpdateActivity extends AppCompatActivity {
    SharedPrefsHelper sharedPrefsHelper;
    List<FloorsList> floorLists =new ArrayList<>();
    List<String> floorNames = new ArrayList<>();
    List<FloorWorksList> floorWorksLists =new ArrayList<>();
    List<FloorWorksList> gFloorWorksLists =new ArrayList<>();
    List<String> floorWorksNames =new ArrayList<>();
    List<ProjectStatusList> projectStatusLists =new ArrayList<>();
    List<String> projectStatusNames =new ArrayList<>();

    List<Integer> floorIds = new ArrayList<>();
    List<Integer> floorWorkIds = new ArrayList<>();
    List<Integer> projStatusIds = new ArrayList<>();

    int iii =0;
    int jjj =0;
    int projId = 0;
    MultiSelectSpinner floorWorksSpinner;
    Spinner projStatusSpinner,floorSpinner;

    TextView tvProjectSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
projId = getIntent().getIntExtra("projId", 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProjectFloorsWorksStatus();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void showAddUserDialog() {

        final Dialog dialog = new Dialog(FloorWiseUpdateActivity.this);
        dialog.setContentView(R.layout.floor_update_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);


        Button btnAddUser = (Button) dialog.findViewById(R.id.btnUpdateFloor);
        Button cancelDilog = (Button) dialog.findViewById(R.id.btnFloorCanceldlg);
        // edtUserName.setFilters(new InputFilter[] { filter });
        ///edtUserPassword.setFilters(new InputFilter[] { filter });


        floorSpinner = (Spinner) dialog.findViewById(R.id.floorSpinner);
        floorWorksSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.floorWorkSpinner);
        projStatusSpinner = (Spinner) dialog.findViewById(R.id.floorStatusSpinner);

       // btnAddUser.setText("Add User");

        cancelDilog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null)
                    if(dialog.isShowing())
                        dialog.dismiss();
                finish();
            }

        });

          floorNames = getFloorNames();

        ArrayAdapter<String> adapter = new ArrayAdapter <String>(FloorWiseUpdateActivity.this, android.R.layout.simple_list_item_1, floorNames);

        floorSpinner.setAdapter(adapter);
        floorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position>=0) {
                    floorIds.add(floorLists.get(position ).getId());
                    callFloorWorksStatusbyFloor(floorLists.get(position).getId());

                }else{
                    floorWorksLists.clear();
                    floorIds.clear();
                    floorWorkIds.clear();
                  //  setFloorWorkAdapter(true);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        projectStatusNames = getProjectStatusNames();

        ArrayAdapter<String> adapter3 = new ArrayAdapter <String>(FloorWiseUpdateActivity.this, android.R.layout.simple_spinner_dropdown_item, projectStatusNames);
        projStatusSpinner.setAdapter(adapter3);
        projStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                projStatusIds.clear();
                if(position>0) {
                    projStatusIds.add(projectStatusLists.get(position - 1).getId());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       /* projStatusSpinner.setAdapter(adapter3).setSelectAll(false).setTitle("Select Status")
                .setMinSelectedItems(1).setListener( new BaseMultiSelectSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                projStatusIds.clear();
                for(int i = 0; i<selected.length;i++){
                    if(selected[i]){

                        projStatusIds.add(projectStatusLists.get(i).getId());

                    }



                }
               // getAllProjectByNatureService(1,divCode,0);

            }
        });*/
        // if button is clicked, close the custom dialog
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(floorIds.size()==0){
                    Toast.makeText(FloorWiseUpdateActivity.this,"Please select Floor",Toast.LENGTH_SHORT).show();
                    return;
                } if(floorWorkIds.size()==0){
                    Toast.makeText(FloorWiseUpdateActivity.this,"Please select Floor Work",Toast.LENGTH_SHORT).show();
                    return;
                } if(projStatusIds.size()==0){
                    Toast.makeText(FloorWiseUpdateActivity.this,"Please select Status",Toast.LENGTH_SHORT).show();
                    return;
                }

                updateFloorDetails();
                   dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
    AlertDialog alertDialog;
    private void showCompltedDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FloorWiseUpdateActivity.this);

        alertDialogBuilder.setMessage("All Floor works are completed");
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();
                        finish();
                    }
                });


        alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }



    private void setFloorWorkAdapter(boolean isGfloor){

        floorWorksNames = getFloorWorksNames();
    ArrayAdapter<String> adapter2 = new ArrayAdapter <String>(FloorWiseUpdateActivity.this, android.R.layout.simple_list_item_multiple_choice, floorWorksNames);

    floorWorksSpinner.setListAdapter(adapter2).setSelectAll(false).setAllCheckedText("Selected All").setTitle("Select Work(s)")
            .setMinSelectedItems(1).setListener( new BaseMultiSelectSpinner.MultiSpinnerListener() {
        @Override
        public void onItemsSelected(boolean[] selected) {
            floorWorkIds.clear();
            for(int i = 0; i<selected.length;i++){
                if(selected[i]){

                        floorWorkIds.add(floorWorksLists.get(i).getId());

                }



            }
            // getAllProjectByNatureService(1,divCode,0);

        }
    });
}

    private List<String> getFloorNames() {

        floorNames.clear();
        //floorNames.add("");
        if(floorLists!=null) {
            for (int i=0; i<floorLists.size();i++) {
                if(floorLists.get(i).getFloorName()!=null) {
                    floorNames.add(floorLists.get(i).getFloorName());
                }
            }
        }

        return floorNames;
    }
    private List<String> getFloorWorksNames() {
        floorWorksNames.clear();
        if(floorWorksLists!=null) {
            for (int i=0; i<floorWorksLists.size();i++) {
                if(floorWorksLists.get(i).getWorkType()!=null) {
                    floorWorksNames.add(floorWorksLists.get(i).getWorkType());
                }
            }
        }

        return floorWorksNames;
    }
    private List<String> getProjectStatusNames() {

        projectStatusNames.clear();
        projectStatusNames.add("");
        if(projectStatusLists!=null) {
            for (int i=0; i<projectStatusLists.size();i++) {
                if(projectStatusLists.get(i).getStatus()!=null) {
                    projectStatusNames.add(projectStatusLists.get(i).getStatus());
                }
            }
        }

        return projectStatusNames;
    }

    private void updateFloorDetails(){
        ProgressDialog progressDialog = new ProgressDialog(FloorWiseUpdateActivity.this);
        progressDialog.setMessage("Updating details...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();



        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        UpdateFloorItem ufi = new UpdateFloorItem();
        List<ProjectFloorstatus> pfsList = new ArrayList<>();
        for(int i=0 ; i<floorIds.size();i++){
            for(int j=0; j<floorWorkIds.size();j++){
                Log.d("projId",""+projId);

                pfsList.add(new ProjectFloorstatus(new Project(projId),new Floor(floorIds.get(i)),new FloorWork(floorWorkIds.get(j)),new ProjectStatus(projStatusIds.get(0))));
            }
        }
        ufi.setProjectFloorStatus(pfsList);

        Call<UpdateFloorDetailsModel> call = service.updateFloorDetails(ufi);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UpdateFloorDetailsModel>() {

            @Override
            public void onResponse(Call<UpdateFloorDetailsModel> call, Response<UpdateFloorDetailsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        Toast.makeText(FloorWiseUpdateActivity.this,"Updated successfully",Toast.LENGTH_SHORT).show();

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }
                finish();

            }



            @Override
            public void onFailure(Call<UpdateFloorDetailsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(FloorWiseUpdateActivity.this,"Unable to connect",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getProjectFloorsWorksStatus() {
        ProgressDialog progressDialog = new ProgressDialog(FloorWiseUpdateActivity.this);
        progressDialog.setMessage("Getting details...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        FloorDetailsItem projectsItem = new FloorDetailsItem();


        projectsItem.setId(projId);



        Call<FloorDetailsModel> call = service.getFloorDetails(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<FloorDetailsModel>() {

            @Override
            public void onResponse(Call<FloorDetailsModel> call, Response<FloorDetailsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("floor wise", ""+json);
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try {

                            if(response.body().getFloorsList()!=null) {
                               floorLists.clear();
                               List<FloorsList> tempFl = response.body().getFloorsList();
                               for(FloorsList fl:tempFl){
                                   if(!fl.getStatus().equals("C")){
                                       floorLists.add(fl);
                                   }
                               }

                            }else{

                            }

                            if(response.body().getProjectStatusList()!=null) {
                               projectStatusLists.clear();
                               projectStatusLists = response.body().getProjectStatusList();


                            }else{
                                Toast.makeText(FloorWiseUpdateActivity.this, "No details available for the selection", Toast.LENGTH_LONG).show();

                            }
                            if(floorLists.size()>0) {

                                showAddUserDialog();
                            }else{
                                showCompltedDialog();
                                return;
                            }
                        } catch (Exception e) {
                            Toast.makeText(FloorWiseUpdateActivity.this, "No details available for the selection", Toast.LENGTH_LONG).show();

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<FloorDetailsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(FloorWiseUpdateActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void callFloorWorksStatusbyFloor(int id) {
        ProgressDialog progressDialog = new ProgressDialog(FloorWiseUpdateActivity.this);
        progressDialog.setMessage("Getting details...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        Project projectsItem = new Project();


        projectsItem.setId(projId);

        Floor floor = new Floor(id);
        FloorDetailsbyFloorIDItem floorIDItem = new FloorDetailsbyFloorIDItem(projectsItem,floor);



        Call<FloorWorkDetailsByFloorModel> call = service.getProjectFloorByFloor(floorIDItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<FloorWorkDetailsByFloorModel>() {

            @Override
            public void onResponse(Call<FloorWorkDetailsByFloorModel> call, Response<FloorWorkDetailsByFloorModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("floor wise", ""+json);
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try {

                            if(response.body().getFloorsWork()!=null) {
                                floorWorksLists.clear();
                                List<FloorWorksList> tempFl = response.body().getFloorsWork();
                                for(FloorWorksList fl:tempFl){
                                    if(!fl.getStatus().equals("C")){
                                        floorWorksLists.add(fl);
                                    }
                                }
                                setFloorWorkAdapter(true);
                            }else{
                                Toast.makeText(FloorWiseUpdateActivity.this, "No details available for the selection", Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(FloorWiseUpdateActivity.this, "No details available for the selection", Toast.LENGTH_LONG).show();

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<FloorWorkDetailsByFloorModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(FloorWiseUpdateActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

}
