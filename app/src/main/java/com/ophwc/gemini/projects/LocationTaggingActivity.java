package com.ophwc.gemini.projects;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.Projects;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.TaglocationItem;
import com.ophwc.gemini.model.project.TaglocationModel;
import com.quickblox.sample.groupchatwebrtc.activities.PermissionsActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;


import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationTaggingActivity extends AppCompatActivity {
    private String mLastUpdateTime;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private TextView tvLocProjlat,tvLocProjlong,tvLocProjAddress;
    int projId = -1;
    private static final int REQUEST_PERMISSIONS = 101;
    double mLatitude = 0.0, mLongitude = 0.0;
    private Button btnSaveLocation;
    int projectid = -1;
    String projName = "";
    String projLocation = "";
    String projDefination = "";

    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_location);
        tvLocProjlat = (TextView) findViewById(R.id.tvLocProjlat);
        tvLocProjlong = (TextView) findViewById(R.id.tvLocProjlong);
        tvLocProjAddress = (TextView) findViewById(R.id.tvLocProjAddress);
        TextView tvLocProjName = (TextView) findViewById(R.id.tvLocProjName);
        TextView tvLocProjLoc = (TextView) findViewById(R.id.tvLocProjLocation);
        btnSaveLocation = (Button) findViewById(R.id.btnSaveLocation);
        btnSaveLocation.setEnabled(false);
        btnSaveLocation.setFocusable(false);
        projId = getIntent().getIntExtra("projId",-1);
        projName = getIntent().getStringExtra("projName");
        projDefination = getIntent().getStringExtra("projDef");
        projLocation = getIntent().getStringExtra("projLoc");
        try {
            tvLocProjLoc.setText(projLocation);
            tvLocProjName.setText(projName);
        }catch (Exception e){
            e.printStackTrace();
        }

        Toolbar tagLoctoolbar = (Toolbar) findViewById(R.id.tagLoctoolbar);
        setSupportActionBar(tagLoctoolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setTitle("Tag Location");
        checkForPermissions();

        //startPermissionsActivity(false);
        //init();
        btnSaveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    tagLocationService();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        tvLocProjAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lat = tvLocProjlat.getText().toString();
                String lngt = tvLocProjlong.getText().toString();
                //.putExtra("lat",lat).putExtra("lng",lngt));
                //startActivity(new Intent(LocationTaggingActivity.this,CaptureProjectStatus.class).putExtra("lat",lat).putExtra("lng",lngt));
            }
        });
        // restore the values from saved instance state
        //restoreValuesFromBundle(savedInstanceState);
    }

    private void checkForPermissions() {

        if (ContextCompat.checkSelfPermission(LocationTaggingActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (LocationTaggingActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(LocationTaggingActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(LocationTaggingActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
            init();
            startLocationUpdates();
            //}

            updateLocationUI();
        }
    }

    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, Consts.LOCATION_PERMISSIONS);
    }
    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }


    /**
     * Update the UI displaying the location data
     * and toggling the buttons
     */
    private void updateLocationUI() {
        if (mCurrentLocation != null) {

            tvLocProjlat.setText(": " + mCurrentLocation.getLatitude() );
            tvLocProjlong.setText(": " + mCurrentLocation.getLongitude());
            getAddress(mCurrentLocation.getLongitude(),mCurrentLocation.getLatitude());


            // giving a blink animation on TextView
            tvLocProjlat.setAlpha(0);
            tvLocProjlat.animate().alpha(1).setDuration(300);
            mLatitude = mCurrentLocation.getLatitude();
            mLongitude = mCurrentLocation.getLongitude();
            btnSaveLocation.setEnabled(true);
            btnSaveLocation.setFocusable(true);

            // location last updated time
           // txtUpdatedOn.setText("Last updated on: " + mLastUpdateTime);
        }

        //toggleButtons();
    }

    public String getAddress(double longitude, double latitude){

        Geocoder geocoder;
        String address="";
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            //AddToDatabase(latitude,longitude,address,city,state);
            tvLocProjAddress.setText(address);


        }catch (Exception e){

            e.printStackTrace();
            tvLocProjAddress.setText("Address not found");
            //tvLocProjAddress.setVisibility(View.INVISIBLE);

        }
        return address;}

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
       // outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        //outState.putParcelable("last_known_location", mCurrentLocation);
        //outState.putString("last_updated_on", mLastUpdateTime);

    }



    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("TAG", "All location settings are satisfied.");

                       // Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("TAG", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(LocationTaggingActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("TAG", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("TAG", errorMessage);

                                Toast.makeText(LocationTaggingActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    public void startLocationButtonClick() {
        // Requesting ACCESS_FINE_LOCATION using Dexter library
        startLocationUpdates();
      /*  Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();*/
    }

  /*  @OnClick(R.id.btn_stop_location_updates)
    public void stopLocationButtonClick() {
        mRequestingLocationUpdates = false;
        stopLocationUpdates();
    }
*/
    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                        //toggleButtons();
                    }
                });
    }


    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
      //  if (mRequestingLocationUpdates && checkPermissions()) {
            /*startLocationUpdates();
        //}

        updateLocationUI();*/

    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onPause() {
        super.onPause();

       /* if (mRequestingLocationUpdates) {
            // pausing location updates
            stopLocationUpdates();
        }*/
    }

        @Override
        public void onRequestPermissionsResult(int requestCode,@NonNull String permissions[],@NonNull int[] grantResults) {
            switch (requestCode) {
                case REQUEST_PERMISSIONS: {
                    if ((grantResults.length > 0) && (grantResults[0]) == PackageManager.PERMISSION_GRANTED) {
                        //Call whatever you want
                        init();
                        startLocationUpdates();
                        //}

                        updateLocationUI();
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "Enable Permissions from settings",
                                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" + getPackageName()));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                        startActivity(intent);
                                    }
                                }).show();
                    }
                    return;
                }
            }
        }


    private void tagLocationService() {
        ProgressDialog progressDialog = new ProgressDialog(LocationTaggingActivity.this);
        progressDialog.setMessage("Updating location...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        TaglocationItem taglocationItem = new TaglocationItem();

        try {
            projectid = getIntent().getIntExtra("projId",-1);
            projName = getIntent().getStringExtra("projName");
        }catch (Exception e){
            if(progressDialog!=null)
                progressDialog.dismiss();
            Toast.makeText(LocationTaggingActivity.this,"Unable to update location",Toast.LENGTH_SHORT).show();

        };
        Projects projects = new Projects();
        projects.setId(projId);
        taglocationItem.setProjects(projects);
        taglocationItem.setLangitude(String.valueOf(mLongitude));
        taglocationItem.setLatitude(String.valueOf(mLatitude));

        Call<TaglocationModel> call = service.tagLocation(taglocationItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<TaglocationModel>() {

            @Override
            public void onResponse(Call<TaglocationModel> call, Response<TaglocationModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        String lat = tvLocProjlat.getText().toString();
                        String lngt = tvLocProjlong.getText().toString();
                        StringBuffer sb = new StringBuffer();
                        sb.append(getIntent().getStringExtra("projName"));
                        sb.append("\n");
                        sb.append("Definition : " + projDefination);



                        Toast.makeText(LocationTaggingActivity.this,"Location saved successfully",Toast.LENGTH_SHORT).show();
                        //startActivity(new Intent(LocationTaggingActivity.this, ProjectsListActivity.class));
                        startActivity(new Intent(LocationTaggingActivity.this, CaptureProjectStatus.class)
                                .putExtra("projId", projectid)
                                .putExtra("divId", ""+OPHWCSharedPrefs.getInstance(LocationTaggingActivity.this).getDivisionId())
                                .putExtra("projName", getIntent().getStringExtra("projName"))
                                .putExtra("projDef", projDefination)
                                .putExtra("lat",lat)
                                .putExtra("lng",lngt));
                        finish();

                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<TaglocationModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(LocationTaggingActivity.this,"Unable to update location",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
}