package com.ophwc.gemini.projects.gallery;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.divsuperuser.SuperUserImagesGrid;
import com.ophwc.gemini.model.floor.FloorDetailsItem;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.byProjId.GetProjectByIDModel;
import com.ophwc.gemini.model.project.projectBudget.ProjectBudgetItem;
import com.ophwc.gemini.model.project.projectBudget.budget.BudgetDetailModel;
import com.ophwc.gemini.model.user.update.UserUpdateModel;
import com.ophwc.gemini.projects.BudgetDetailsActivity;
import com.ophwc.gemini.projects.CaptureProjectStatus;
import com.ophwc.gemini.projects.FloorWiseUpdateActivity;
import com.ophwc.gemini.projects.map.ProjectsMapsActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllImagesGridActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    SharedPrefsHelper sharedPrefsHelper;
    GridView gridView;
    FloatingActionButton addImagefab;
    List<String> imagesList = new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    OPHWCSharedPrefs ophwcSharedPrefs;
    PrjectList prjectLists = new PrjectList();
    TextView tvNoImagesPager;
    AlertDialog alertDialog;
    GetProjectByIDModel getProjectByIDModel;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_proj_nfo, menu);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(AllImagesGridActivity.this);
       /* if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=0){
            menu.findItem(R.id.menu_delete_image).setVisible(false);

        }else{
            menu.findItem(R.id.menu_delete_image).setVisible(true);

        }*/
        if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)==0){
            menu.findItem(R.id.menu_proj_loc).setVisible(false);

        }
        menu.findItem(R.id.menu_delete_image).setVisible(false);
        menu.findItem(R.id.menu_share_image).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.menu_projInfo:
                showAlertDialog();
                break;
                case R.id.menu_project_budget:
                    startActivity(new Intent(AllImagesGridActivity.this,BudgetDetailsActivity.class).putExtra("projId",getIntent().getIntExtra("projId",0)));
                break;
            case R.id.menu_delete_image:
                //showDeleteAlertDialog();
                break;
            case R.id.menu_project_floor:
                startActivity(new Intent(AllImagesGridActivity.this,FloorWiseUpdateActivity.class).putExtra("projId",getIntent().getIntExtra("projId", 0)));
                break;
            case R.id.menu_proj_loc:
                startActivity(new Intent(AllImagesGridActivity.this, ProjectsMapsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("isProject", true)
                        .putExtra("lat", getProjectByIDModel.getLatitude())
                        .putExtra("lon", getProjectByIDModel.getLangitude()));
                //showDeleteAlertDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


        String projName = getIntent().getStringExtra("projName");
        String lat = getIntent().getStringExtra("lat");
        String lng = getIntent().getStringExtra("lng");
        StringBuffer sb = new StringBuffer();
        sb.append("Project : " + getProjectByIDModel.getProjectName());
        sb.append("\n");
        sb.append("Defination : " + getProjectByIDModel.getProjectDefination());
        sb.append("\n");
        sb.append("\n");
        sb.append("\n");
        sb.append("Latitude : " + lat);
        sb.append("\n");
        sb.append("\n");
        sb.append("Longitude " + lng);


        alertDialogBuilder.setMessage(sb.toString());
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (alertDialog.isShowing())
                            alertDialog.dismiss();
                    }
                });


        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_images_grid_layout);
        initialiseToolbar();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();

        gridView = (GridView) findViewById(R.id.gridViewImages);
        tvNoImagesPager = (TextView) findViewById(R.id.tvNoImagesPager);
        addImagefab = (FloatingActionButton) findViewById(R.id.addImagefab);

        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(AllImagesGridActivity.this);
        if (sharedPrefsHelper.get(Consts.PREF_LOGIN_AS, -1) != 0) {
            addImagefab.setVisibility(View.GONE);
        } else {
            addImagefab.setVisibility(View.VISIBLE);
        }
        checkForPermissions();
        addImagefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isGpsEnabled()) {
                    startCaptureActivity();
                } else {
                    setEnableLocation();
                }
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //divisionLists.
                startActivity(new Intent(AllImagesGridActivity.this, ImagesActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("projId", getIntent().getIntExtra("projId", 0))
                        .putExtra("imagePos", position));
            }
        });


    }

    public void startCaptureActivity() {
        StringBuffer sb = new StringBuffer();
        sb.append(getProjectByIDModel.getProjectName());
        sb.append("\n");
        try {
            sb.append("Definition : " + getProjectByIDModel.getProjectDefination());
        } catch (Exception e) {
        }
        ;

        startActivity(new Intent(AllImagesGridActivity.this, CaptureProjectStatus.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra("projId", getIntent().getIntExtra("projId", -1))
                .putExtra("divId", "" + ophwcSharedPrefs.getDivisionId())
                .putExtra("projName", getProjectByIDModel.getProjectName())
                .putExtra("projDef", getProjectByIDModel.getProjectDefination())
                .putExtra("lat", getIntent().getStringExtra("lat"))
                .putExtra("lng", getIntent().getStringExtra("lng")));
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseToolbar();
        //getAllImagesService();
        getAllImagesByProjectService();
    }
    public static final int REQUEST_PERMISSIONS = 113;
    private void checkForPermissions() {

        if (ContextCompat.checkSelfPermission(AllImagesGridActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (AllImagesGridActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(AllImagesGridActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(AllImagesGridActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want

            //}


        }
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.imagestoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("projName") + " Images");


    }

    @Override
    public void onBackPressed() {

    }

    private void getAllImagesByProjectService() {
        ProgressDialog progressDialog = new ProgressDialog(AllImagesGridActivity.this);
        progressDialog.setMessage("Getting images...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();


        String id = "1";
       /* try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        //projectsItem.setId(ophwcSharedPrefs.getDivisionId());
        projectsItem.setId(getIntent().getIntExtra("projId", 0));
        Log.e("ProjID", "" + getIntent().getIntExtra("projId", 0));

        Call<GetProjectByIDModel> call = service.getProjectById(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetProjectByIDModel>() {

            @Override
            public void onResponse(Call<GetProjectByIDModel> call, Response<GetProjectByIDModel> response) {

                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code + "");
                Log.d("usercheck", "" + json);
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        imagesList = new ArrayList<>();
                        imagesList.clear();
                        try {
                            if (response.body() != null) {
                                getProjectByIDModel = response.body();
                                if (response.body().getImagesPaths() != null) {
                                    if (response.body().getImagesPaths().size() > 0) {

                                        //prjectLists = myprjectList;
                                        imagesList = response.body().getImagesPaths();
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //imagesList =

                        setAdapters();
                        // String status = response.body().getStatus();
                        if (progressDialog != null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }


            }


            @Override
            public void onFailure(Call<GetProjectByIDModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Toast.makeText(AllImagesGridActivity.this, "Unable to get Project details", Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public boolean isGpsEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            return gps_enabled;
        } catch (Exception ex) {
            return false;
        }


    }

    GoogleApiClient googleApiClient;
    public void setEnableLocation() {
        LocationRequest mLocationRequest;
        LocationSettingsRequest mLocationSettingsRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        builder.setAlwaysShow(true);
        if(googleApiClient==null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, 0, this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            if(!googleApiClient.isConnected())
            googleApiClient.connect();
        }
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                checkForPermissions();
                return;
            }
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(googleApiClient, mLocationRequest, this);
        } catch (Exception e) {
        }

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi
                        .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied.
                        // You can initialize location requests here.
                   /* int permissionLocation = ContextCompat
                            .checkSelfPermission(AllImagesGridActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                      Location mCurrentLocation = LocationServices.FusedLocationApi
                                .getLastLocation(googleApiClient);
                    }*/
                        startCaptureActivity();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied.
                        // But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            // Ask to turn on GPS automatically
                            status.startResolutionForResult(AllImagesGridActivity.this,
                                    1201);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied.
                        // However, we have no way
                        // to fix the
                        // settings so we won't show the dialog.
                        // finish();
                        break;
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if(googleApiClient!=null) {
            googleApiClient.stopAutoManage(AllImagesGridActivity.this);
            googleApiClient.disconnect();
        }
    }

    private void setAdapters() {
        try {
            if (imagesList != null && imagesList.size() > 0) {
                tvNoImagesPager.setVisibility(View.GONE);
                gridView.setVisibility(View.VISIBLE);

                gridView.setAdapter(new ImagesAdapter(AllImagesGridActivity.this, imagesList));

            } else {
                gridView.setVisibility(View.GONE);
                tvNoImagesPager.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            gridView.setVisibility(View.GONE);
            tvNoImagesPager.setVisibility(View.VISIBLE);
        }

    }
}

