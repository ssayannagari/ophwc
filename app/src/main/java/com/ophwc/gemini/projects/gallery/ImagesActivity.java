package com.ophwc.gemini.projects.gallery;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.projectBudget.budget.BudgetDetailModel;
import com.ophwc.gemini.model.floor.FloorDetailsItem;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;

import com.ophwc.gemini.model.project.byProjId.GetProjectByIDModel;
import com.ophwc.gemini.model.project.imageUpload.DeleteImageItem;
import com.ophwc.gemini.model.project.imageUpload.ImageUploadModel;
import com.ophwc.gemini.model.project.projectBudget.ProjectBudgetItem;
import com.ophwc.gemini.model.user.update.UserUpdateModel;
import com.ophwc.gemini.projects.BudgetDetailsActivity;
import com.ophwc.gemini.projects.CaptureProjectStatus;
import com.ophwc.gemini.projects.FloorWiseUpdateActivity;
import com.ophwc.gemini.projects.map.ProjectsMapsActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImagesActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    SharedPrefsHelper sharedPrefsHelper;
    GridView gridView;
    FloatingActionButton addImagefab;
    List<String> imagesList=new ArrayList<>();
    private static ViewPager mPager;
    private static int currentPage = 0;
    OPHWCSharedPrefs ophwcSharedPrefs;
   PrjectList prjectLists = new PrjectList();
   TextView tvNoImagesPager;
    AlertDialog alertDialog;
    GetProjectByIDModel getProjectByIDModel;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_proj_nfo, menu);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(ImagesActivity.this);
        if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=0&&sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=4){
            menu.findItem(R.id.menu_delete_image).setVisible(false);
            menu.findItem(R.id.menu_share_image).setVisible(true);
        }else{
            menu.findItem(R.id.menu_delete_image).setVisible(true);
            menu.findItem(R.id.menu_share_image).setVisible(true);
            menu.findItem(R.id.menu_proj_loc).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }
    MenuItem miActionProgressItem;
    MenuItem shareImage;

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Store instance of the menu item containing progress
        miActionProgressItem = menu.findItem(R.id.miActionProgress);
        shareImage = menu.findItem(R.id.menu_share_image);
        // Extract the action-view from the menu item
        ProgressBar v =  (ProgressBar) MenuItemCompat.getActionView(miActionProgressItem);
        // Return to finish
        return super.onPrepareOptionsMenu(menu);
    }
    public void showProgressBar() {
        // Show progress item
       // miActionProgressItem.setVisible(true);
        //shareImage.setVisible(false);
        showShareAlertDialog();
       // shareImage(imagesList.get(mPager.getCurrentItem()), this);
    }

    public void hideProgressBar() {
        // Hide progress item
        miActionProgressItem.setVisible(false);
        shareImage.setVisible(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
                case R.id.menu_project_floor:
                startActivity(new Intent(ImagesActivity.this,FloorWiseUpdateActivity.class).putExtra("projId",getIntent().getIntExtra("projId", 0)));
                break;
            case R.id.menu_projInfo:
                showAlertDialog();
                break;
                case R.id.menu_project_budget:
                    startActivity(new Intent(ImagesActivity.this,BudgetDetailsActivity.class).putExtra("projId",getIntent().getIntExtra("projId", 0)));


                    break;
                case R.id.menu_delete_image:
                    showDeleteAlertDialog();
                break;
                case R.id.menu_share_image:

                    //showProgressBar();
                   // showShareAlertDialog();

                    DownloadImageWithURLTask downloadTask = new DownloadImageWithURLTask();
                    downloadTask.execute(imagesList.get(mPager.getCurrentItem()));
                    //return true;

                break;
            case R.id.menu_proj_loc:
                startActivity(new Intent(ImagesActivity.this, ProjectsMapsActivity.class)
                        .putExtra("isProject", true)
                        .putExtra("lat", getProjectByIDModel.getLatitude())
                        .putExtra("lon", getProjectByIDModel.getLangitude()));
                //showDeleteAlertDialog();
                break;

        }
        return super.onOptionsItemSelected(item);
    }



    private class DownloadImageWithURLTask extends AsyncTask<String, Void, Uri> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected Uri doInBackground(String... urls) {
            String pathToFile = urls[0];
            Bitmap bitmap = null;
            try {
                InputStream in = new java.net.URL(pathToFile).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            Uri bmpUri = getLocalBitmapUri(bitmap,ImagesActivity.this,"OPHWC_"+getProjectByIDModel.getProjectName());
            return bmpUri;
        }
        protected void onPostExecute(Uri result) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_STREAM, result);
            startActivity(Intent.createChooser(i, "Share using..."));
            progressBar.setVisibility(View.GONE);
        }
    }
    public void shareImage(String url, final Context context,Bitmap bmp) {

        Picasso.get().load(url).into(new Target() {
            @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context,""));
                context.startActivity(Intent.createChooser(i, "OPHWC_"+getProjectByIDModel.getProjectName()));
                //hideProgressBar();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
              Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
            }

            @Override public void onPrepareLoad(Drawable placeHolderDrawable) {


            }


        });
    }
    static public Uri getLocalBitmapUri(Bitmap bmp, Context context,String imgName) {
        Uri bmpUri = null;
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyHHmmss");
        String dateTime = sdf.format(Calendar.getInstance().getTime());
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),  imgName+ dateTime + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }
    private void showShareAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);




        alertDialogBuilder.setMessage("Do you want to share this image?");
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        }).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        progressBar.setVisibility(View.VISIBLE);
                        alertDialog.dismiss();


                        //shareImage(imagesList.get(mPager.getCurrentItem()), ImagesActivity.this);
                    }

                });



        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


                String projName = getIntent().getStringExtra("projName");
                String lat = getIntent().getStringExtra("lat");
                String lng = getIntent().getStringExtra("lng");
                StringBuffer sb = new StringBuffer();
                sb.append("Project : "+getProjectByIDModel.getProjectName());
                sb.append("\n");
                sb.append("Defination : "+getProjectByIDModel.getProjectDefination());
                sb.append("\n");
                sb.append("\n");
                sb.append("\n");
                sb.append("Latitude : "+lat);
                sb.append("\n");
                sb.append("\n");
                sb.append("Longitude "+lng);



        alertDialogBuilder.setMessage(sb.toString());
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                               if(alertDialog.isShowing())
                                   alertDialog.dismiss();
                                  }
                        });



         alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    private void showDeleteAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);


        alertDialogBuilder.setMessage("Do you want to delete the Image?");
        alertDialogBuilder.setTitle("Delete Image?");
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Log.e("Current",mPager.getCurrentItem()+"");
                                callDeleteImageService(mPager.getCurrentItem());
                               if(alertDialog.isShowing())
                                   alertDialog.dismiss();
                                  }
                        });



         alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void callDeleteImageService(int currentItem) {
        ProgressDialog progressDialog = new ProgressDialog(ImagesActivity.this);
        progressDialog.setMessage("Deleting image...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        DeleteImageItem projectsItem = new DeleteImageItem();


       String imgPath =  imagesList.get(currentItem);
       Log.e("PATH",""+imgPath);
       /* try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        //projectsItem.setId(ophwcSharedPrefs.getDivisionId());
        projectsItem.setImagePath(imgPath);

        Call<ImageUploadModel> call = service.DeleteImage(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ImageUploadModel>() {

            @Override
            public void onResponse(Call<ImageUploadModel> call, Response<ImageUploadModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:


                        if(response.body().getStatusCode()==1){
                            imagesList.remove(currentItem);
                            (new MyPagerAdapter(ImagesActivity.this,imagesList)).notifyDataSetChanged();
                            setAdapters();

                               try {

                                   if(imagesList.size()>0) {
                                       indicator.setVisibility(View.VISIBLE);
                                       if (currentItem <= (imagesList.size() - 1))
                                           mPager.setCurrentItem(currentItem, true);
                                       else
                                           mPager.setCurrentItem(currentItem - 1, true);
                                   }else{
                                       indicator.setVisibility(View.GONE);
                                   }
                               }catch (Exception e){
                                   e.printStackTrace();

                               }



                           Toast.makeText(ImagesActivity.this,"Image has been deleted successfully",Toast.LENGTH_SHORT).show();
                            }

                            if(response.body().getStatusCode()==0){
                           Toast.makeText(ImagesActivity.this,"Unable to delete image, try later.",Toast.LENGTH_SHORT).show();
                            }


                        //imagesList =

                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ImageUploadModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ImagesActivity.this,"Unable to delete image",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    CircleIndicator indicator;
    ProgressBar progressBar;
    private static final String STATE_ITEMS = "items";
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(STATE_ITEMS, (Serializable) imagesList);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        initialiseToolbar();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        gridView = (GridView) findViewById(R.id.gridViewImages);
        tvNoImagesPager = (TextView) findViewById(R.id.tvNoImagesPager);
        addImagefab = (FloatingActionButton) findViewById(R.id.addImagefab);
         indicator = (CircleIndicator) findViewById(R.id.indicator);
        mPager = (ViewPager) findViewById(R.id.imagePager);
        progressBar = (ProgressBar) findViewById(R.id.shareProgressbar);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(ImagesActivity.this);
        if(sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=0&&sharedPrefsHelper.get(Consts.PREF_LOGIN_AS,-1)!=4){
            addImagefab.setVisibility(View.GONE);
        }else{
            addImagefab.setVisibility(View.VISIBLE);
        }
        checkForPermissions();
        addImagefab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isGpsEnabled()) {
                    startCaptureActivity();
                }else{
                    setEnableLocation();
                }
            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //divisionLists.
               // startActivity(new Intent(ImagesActivity.this,ProjectsListActivity.class).putExtra("divId",String.valueOf(divisionLists.get(position).getId())));
            }
        });



    }
    public static final int REQUEST_PERMISSIONS = 114;
    private void checkForPermissions() {

        if (ContextCompat.checkSelfPermission(ImagesActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ImagesActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(ImagesActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(ImagesActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want

            //}


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseToolbar();
        //getAllImagesService();
        getAllImagesByProjectService();
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.imagestoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);





    }
    @Override
    public void onBackPressed() {

    }

    private void getAllImagesService() {
        ProgressDialog progressDialog = new ProgressDialog(ImagesActivity.this);
        progressDialog.setMessage("Getting images...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();


        String id = "1";
       /* try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        projectsItem.setId(ophwcSharedPrefs.getDivisionId());

        Call<ProjectsModel> call = service.getAllProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        int projId = getIntent().getIntExtra("projId",-1);
                        imagesList = new ArrayList<>();
                        if(projId!=-1){
                            imagesList.clear();
                            List<com.ophwc.gemini.model.project.PrjectList> imagePaths = response.body().getPrjectList();
                            for(com.ophwc.gemini.model.project.PrjectList myprjectList : imagePaths){
                                if(myprjectList.getId()==projId){
                                    prjectLists = myprjectList;
                                   imagesList =  myprjectList.getImagesPaths();
                                }
                            }
                            //imagesList =

                        }


                        setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ImagesActivity.this,"Unable to get Images",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllImagesByProjectService() {
        ProgressDialog progressDialog = new ProgressDialog(ImagesActivity.this);
        progressDialog.setMessage("Getting images...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();


        String id = "1";
       /* try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        //projectsItem.setId(ophwcSharedPrefs.getDivisionId());
        projectsItem.setId(getIntent().getIntExtra("projId",0));

        Call<GetProjectByIDModel> call = service.getProjectById(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetProjectByIDModel>() {

            @Override
            public void onResponse(Call<GetProjectByIDModel> call, Response<GetProjectByIDModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        imagesList = new ArrayList<>();
                          imagesList.clear();
                          try {
                              if (response.body().getImagesPaths() != null) {
                                  getProjectByIDModel = response.body();
                                  if (response.body().getImagesPaths().size() > 0) {

                                      //prjectLists = myprjectList;
                                      imagesList = response.body().getImagesPaths();
                                      getSupportActionBar().setTitle(getProjectByIDModel.getProjectName() + " Images");
                                      setAdapters();
                                  }


                              }
                          }catch (Exception e){
                              e.printStackTrace();
                          }
                            //imagesList =


                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<GetProjectByIDModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ImagesActivity.this,"Unable to get Images",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }




    private void setAdapters() {
        try {
            if(imagesList!=null&&imagesList.size()>0) {
                indicator.setVisibility(View.VISIBLE);
                tvNoImagesPager.setVisibility(View.GONE);
                mPager.setVisibility(View.VISIBLE);
                //gridView.setAdapter(new ImagesAdapter(ImagesActivity.this, imagesList));
                mPager.setAdapter(new MyPagerAdapter(ImagesActivity.this,imagesList));

                indicator.setViewPager(mPager);
                mPager.setCurrentItem(getIntent().getIntExtra("imagePos",0));
            }else{
                indicator.setVisibility(View.GONE);
                tvNoImagesPager.setVisibility(View.VISIBLE);
                mPager.setVisibility(View.GONE);
            }
        }catch (Exception e){
            indicator.setVisibility(View.GONE);
            tvNoImagesPager.setVisibility(View.VISIBLE);
            mPager.setVisibility(View.GONE);
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
    public boolean isGpsEnabled(){
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            return gps_enabled;
        } catch(Exception ex) {
            return false;
        }


    }
    GoogleApiClient googleApiClient;
    public void setEnableLocation() {
        LocationRequest mLocationRequest;
        LocationSettingsRequest mLocationSettingsRequest;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        builder.setAlwaysShow(true);
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 2, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                checkForPermissions();
                return;
            }
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(googleApiClient, mLocationRequest, this);
        } catch (Exception e) {
        }

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi
                        .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied.
                        // You can initialize location requests here.
                   /* int permissionLocation = ContextCompat
                            .checkSelfPermission(AllImagesGridActivity.this,
                                    Manifest.permission.ACCESS_FINE_LOCATION);
                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                      Location mCurrentLocation = LocationServices.FusedLocationApi
                                .getLastLocation(googleApiClient);
                    }*/
                        startCaptureActivity();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied.
                        // But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            // Ask to turn on GPS automatically
                            status.startResolutionForResult(ImagesActivity.this,
                                    1201);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied.
                        // However, we have no way
                        // to fix the
                        // settings so we won't show the dialog.
                        // finish();
                        break;
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if(googleApiClient!=null) {
            googleApiClient.stopAutoManage(ImagesActivity.this);
            googleApiClient.disconnect();
        }
    }
    private void startCaptureActivity() {
        StringBuffer sb = new StringBuffer();
        sb.append(getProjectByIDModel.getProjectName());
        sb.append("\n");
        try {
            sb.append("Definition : " + getProjectByIDModel.getProjectDefination());
        }catch (Exception e){};

        startActivity(new Intent(ImagesActivity.this, CaptureProjectStatus.class)
                .putExtra("projId", getIntent().getIntExtra("projId",-1))
                .putExtra("divId", ""+ophwcSharedPrefs.getDivisionId())
                .putExtra("projName", getProjectByIDModel.getProjectName())
                .putExtra("projDef", getProjectByIDModel.getProjectDefination())
                .putExtra("lat", getIntent().getStringExtra("lat"))
                .putExtra("lng", getIntent().getStringExtra("lng")));
    }
}

