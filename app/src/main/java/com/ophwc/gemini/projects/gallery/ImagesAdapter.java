package com.ophwc.gemini.projects.gallery;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ophwc.gemini.R;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImagesAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mImageLists;
    ImageLoader imageLoader;
    public ImagesAdapter(Context context, List<String> imagesList) {
        this.mContext = context;
        this.mImageLists = imagesList;
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
       // imageLoader.handleSlowNetwork(true);


    }

    @Override
    //public int getCount() {
        //return mImageLists.size();
   // }
    public int getCount() {
        return mImageLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflator =(LayoutInflater.from(mContext));
        if (convertView==null)
            convertView =inflator.inflate(R.layout.images_grid_row,null);
        ImageView imageGridRow = (ImageView)convertView.findViewById(R.id.imageGridRow);
        try {
          //  String imagePath ="http://164.100.141.155:8080/"+mImageLists.get(position);
            //String imagePath =Consts.BASE_URL+"/"+mImageLists.get(position);
            String imagePath =mImageLists.get(position);
            Log.e("Image",imagePath);

            Picasso.get().load(imagePath).into(imageGridRow);
            //imageLoader.displayImage(imagePath,imageGridRow);

        }catch (Exception e){
            e.printStackTrace();
        }

       // tvRowDivName.setText("Division \n"+mImageLists.get(position));
        //tvRowProjStatus.setText("Projects "+mImageLists.get(position).getCompletedProjects()+"/"+mDivisionLists.get(position).getNoOfProjects());



        return convertView;
        //return null;
    }
}
