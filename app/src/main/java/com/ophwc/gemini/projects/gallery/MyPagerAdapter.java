package com.ophwc.gemini.projects.gallery;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.digitalbithub.magnifize.MagnifizeView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ophwc.gemini.R;
import com.ophwc.gemini.TouchImageView;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.squareup.picasso.Picasso;

import java.util.List;

class MyPagerAdapter extends PagerAdapter {

    private List<String> images;
    private LayoutInflater inflater;
    private Context context;
    ImageLoader imageLoader;

    public MyPagerAdapter(Context context){
        //this.context = context;

    };

    public MyPagerAdapter(Context context, List<String> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.handleSlowNetwork(true);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.pager_images_row, view, false);

        TouchImageView magnImage = (TouchImageView) myImageLayout
                .findViewById(R.id.imgDisplay);
        //myImage.setImageResource(images.get(position));

       // imageLoader.displayImage(Consts.BASE_URL+images.get(position),magnImage);
       // imageLoader.displayImage(images.get(position),magnImage);
        Picasso.get().load(images.get(position)).into(magnImage);
        Log.e("Images",images.get(position));
        view.addView(myImageLayout, 0);
        //myImage.setScaleType(ImageView.ScaleType.FIT_XY);
        return myImageLayout;
    }
    public void deleteImage(int pos){
        this.images.remove(pos);

        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
