package com.ophwc.gemini.projects.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.projects.LocationTaggingActivity;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProjectsMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private static final int REQUEST_PERMISSIONS = 102;
    OPHWCSharedPrefs ophwcSharedPrefs;
    boolean isProj = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects_maps);
        Toolbar projToolbar = (Toolbar) findViewById(R.id.projectsLoctoolbar);
        setSupportActionBar(projToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Projects");
        isProj = getIntent().getBooleanExtra("isProject",false);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(ProjectsMapsActivity.this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        checkForPermissions();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkForPermissions() {

        if (ContextCompat.checkSelfPermission(ProjectsMapsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ProjectsMapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(ProjectsMapsActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);

            } else {
                ActivityCompat.requestPermissions(ProjectsMapsActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS);
            }
        } else {
            //Call whatever you want
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                // checkForPermissions();
                //return;
            }
            mMap.setMyLocationEnabled(true);
        } catch (Exception e) {
        }
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        if (isProj) {
            double lon1 = Double.parseDouble(getIntent().getStringExtra("lon"));
            double lat1 = Double.parseDouble(getIntent().getStringExtra("lat"));
            Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat1, lon1)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat1,lon1), 8.0f));

        } else {
            if (SharedPrefsHelper.getInstance().get(Consts.PREF_LOGIN_AS, -1) == 0) {
                callUserProjectsService();
            } else {
                getAllImagesService();
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS) {
     if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
         mMap.setMyLocationEnabled(true); // <-- Start Beemray here
} else {
// Permission was denied or request was cancelled
}
}
    }

    private void getAllImagesService() {
        ProgressDialog progressDialog = new ProgressDialog(ProjectsMapsActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();


        String id = "1";
       /* try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        projectsItem.setId(ophwcSharedPrefs.getDivisionId());

        Call<ProjectsModel> call = service.getAllProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        //int projId = getIntent().getIntExtra("projId",-1);
                        try {
                            if (response.body().getPrjectList() != null) {
                                if (response.body().getPrjectList().size() > 0) {
                                    int i =0;
                                    int j=0;

                                    //response.body().getPrjectList();
                                    for (com.ophwc.gemini.model.project.PrjectList myprjectList : response.body().getPrjectList()) {
                                        boolean isLocationTagged =  myprjectList.getLangitude()==null||myprjectList.getLatitude()==null||
                                                myprjectList.getLangitude().equals("")||myprjectList.getLatitude().equals("")||myprjectList.getLangitude().equals("null")||myprjectList.getLatitude().equals("null");

                                        if(!isLocationTagged) {
                                            j= i;
                                            double lon1 = Double.parseDouble(myprjectList.getLangitude());
                                            double lat1 = Double.parseDouble(myprjectList.getLatitude());
                                          Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat1, lon1)).
                                                    title("Name: "+myprjectList.getProjectName())
                                                    .snippet(("Defination: "+myprjectList.getProjectDefination())));
                                      //marker.showInfoWindow();
                                       if(i==0){
                                           mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat1,lon1), 8.0f));

                                       }
                                        }
                                        i++;
                                    }

                                }else{
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude()), 8.0f));

                                    Toast.makeText(ProjectsMapsActivity.this,"No Projects available",Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude()), 8.0f));

                                Toast.makeText(ProjectsMapsActivity.this,"No Projects available",Toast.LENGTH_SHORT).show();

                            }
                        }catch (Exception e){
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));

                            Toast.makeText(ProjectsMapsActivity.this, "No Projects location available", Toast.LENGTH_SHORT).show();

                        }
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));

                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));

                Toast.makeText(ProjectsMapsActivity.this,"Unable to get Images",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
AlertDialog alertDialog;
    private void callUserProjectsService() {
        ProgressDialog progressDialog = new ProgressDialog(ProjectsMapsActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();
        String id = "1";
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(ProjectsMapsActivity.this);

        int  projUserid = 0;
        projUserid = ophwcSharedPrefs.getUserId();
        projectsItem.setId(ophwcSharedPrefs.getUserId());

        Call<ProjectsModel> call = service.getAllUserProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try{
                        if(response.body().getPrjectList()!=null) {
                            if(response.body().getPrjectList().size()>0) {

                                int i =0;
                                int j=0;

                                //response.body().getPrjectList();
                                for (com.ophwc.gemini.model.project.PrjectList myprjectList : response.body().getPrjectList()) {
                                    boolean isLocationTagged =  myprjectList.getLangitude()==null||myprjectList.getLatitude()==null||
                                            myprjectList.getLangitude().equals("")||myprjectList.getLatitude().equals("")||myprjectList.getLangitude().equals("null")||myprjectList.getLatitude().equals("null");

                                    if(!isLocationTagged) {
                                        j= i;
                                        double lon1 = Double.parseDouble(myprjectList.getLangitude());
                                        double lat1 = Double.parseDouble(myprjectList.getLatitude());
                                        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(lat1, lon1)).
                                                title("Name: "+myprjectList.getProjectName())
                                                .snippet(("Defination: "+myprjectList.getProjectDefination())));
                                        //marker.showInfoWindow();
                                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                            @Override
                                            public boolean onMarkerClick(Marker marker) {
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProjectsMapsActivity.this);


                                                alertDialogBuilder.setTitle("Project details");
                                                StringBuilder sb = new StringBuilder();
                                                sb.append(marker.getTitle());
                                                sb.append("\n");
                                                sb.append(marker.getSnippet());
                                                sb.append("\n");
                                                sb.append("\n");
                                                sb.append("Latitude: "+marker.getPosition().latitude);
                                                sb.append("\n");
                                                sb.append("Longitude: "+marker.getPosition().longitude);

                                                alertDialogBuilder.setMessage(sb.toString());

                                                alertDialogBuilder.setPositiveButton("OK",
                                                        new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface arg0, int arg1) {
                                                                alertDialog.dismiss();


                                                            }
                                                        });


                                               // alertDialog = alertDialogBuilder.create();

                                                //alertDialog.show();
                                                return false;
                                            }
                                        });
                                        if(i==0){
                                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat1,lon1), 8.0f));

                                        }
                                    }
                                    i++;
                                }
                                  }else{
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude()), 8.0f));

                                Toast.makeText(ProjectsMapsActivity.this,"No Projects available",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMap.getMyLocation().getLatitude(),mMap.getMyLocation().getLongitude()), 8.0f));

                            Toast.makeText(ProjectsMapsActivity.this,"No Projects available",Toast.LENGTH_SHORT).show();

                        }
                        }catch (Exception e){
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));

                            Toast.makeText(ProjectsMapsActivity.this, "No Projects location available", Toast.LENGTH_SHORT).show();

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23,83), 4.0f));
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ProjectsMapsActivity.this,"No projects available",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
}
