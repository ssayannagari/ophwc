package com.ophwc.gemini.superuser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ophwc.gemini.R;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;

import java.util.List;

public class SuperDivisionAdapter extends BaseAdapter {
    private Context mContext;
    private List<DivisionList> mDivisionLists;


    // Constructor
    public SuperDivisionAdapter(Context c, List<DivisionList> divisionLists) {
        mContext = c;
        mDivisionLists = divisionLists;
    }



    public int getCount() {
        return mDivisionLists.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflator =(LayoutInflater.from(mContext));
        if (convertView==null)
            convertView =inflator.inflate(R.layout.divisions_grid_row,null);
        TextView tvRowDivName = (TextView) convertView.findViewById(R.id.tvRowDivName);
        TextView tvRowProjStatus = (TextView) convertView.findViewById(R.id.tvRowProjStatus);
        tvRowProjStatus.setVisibility(View.GONE);

        tvRowDivName.setText(" "+mDivisionLists.get(position).getDivisionName());



        return convertView;
    }

    // Keep all Images in array

}