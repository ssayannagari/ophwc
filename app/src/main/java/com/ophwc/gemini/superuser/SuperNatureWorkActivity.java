package com.ophwc.gemini.superuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.divisions.DivisionAdapter;
import com.ophwc.gemini.model.project.byNaturUser.GetProjByNatureAndUserItem;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkListwithProj;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorksWithProjCountModel;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.projects.map.ProjectsMapsActivity;
import com.ophwc.gemini.superuser.project.ProjectsListActivity;
import com.quickblox.sample.groupchatwebrtc.activities.PermissionsActivity;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.PermissionsChecker;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SuperNatureWorkActivity extends AppCompatActivity{
        SharedPrefsHelper sharedPrefsHelper;


        GridView gridView;
        TextView tvDivSuperName;
        OPHWCSharedPrefs ophwcSharedPrefs;
        List<NatureWorkListwithProj> natureWorkLists =new ArrayList<>();
        Toolbar naturetoolbar;
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                getMenuInflater().inflate(R.menu.menu_project_location, menu);
                //ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(SuperNatureWorkActivity.this);

                return super.onCreateOptionsMenu(menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

                switch (item.getItemId()){
                        case android.R.id.home:
                                finish();
                                break;
                        case R.id.menu_projLocation:
                               // showAlertDialog();
                                startActivity(new Intent(SuperNatureWorkActivity.this,ProjectsMapsActivity.class));
                                break;


                }
                return super.onOptionsItemSelected(item);
        }
@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.natures_activity);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(SuperNatureWorkActivity.this);
        initialiseToolbar();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        //sharedPrefsHelper.save(Consts.PREF_LOGIN_AS,3);
        gridView = (GridView) findViewById(R.id.gridNatureView);
        tvDivSuperName = (TextView) findViewById(R.id.tvNatureSuperName);
        tvDivSuperName.setText(getIntent().getStringExtra("divName"));
      /*  startService(new Intent(this, CheckCallService.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, CheckCallService.class);
        PendingIntent pintent = PendingIntent
                .getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                5*1000, pintent);*/


       getAllNatureOfWorks();


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //divisionLists.
        //ophwcSharedPrefs.putDivisionId(divisionLists.get(0).getId());
        try {
                if (natureWorkLists.get(position).getProjCount()>0) {
                        startActivity(new Intent(SuperNatureWorkActivity.this, ProjectsListActivity.class)
                                .putExtra("divId", "" + natureWorkLists.get(position).getId()));

                } else {
                        Toast.makeText(SuperNatureWorkActivity.this,
                                "No projects available for this Nature of work", Toast.LENGTH_SHORT).show();
                }
        }catch (Exception e){
                Toast.makeText(SuperNatureWorkActivity.this,
                        "No projects available for this Nature of work", Toast.LENGTH_SHORT).show();

        }
}
        });




        }

@Override
protected void onResume() {
        super.onResume();
        // initialiseToolbar();


        }



private void initialiseToolbar() {
        Toolbar projectstoolbar = (Toolbar) findViewById(R.id.naturetoolbar);
        setSupportActionBar(projectstoolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Nature of Works");

        }


private void getAllNatureOfWorks() {
        ProgressDialog progressDialog = new ProgressDialog(SuperNatureWorkActivity.this);
        progressDialog.setMessage("Getting Nature of projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        GetProjByNatureAndUserItem getProjByNatureAndUserItem = new GetProjByNatureAndUserItem();
        getProjByNatureAndUserItem.setUserId(0);
        getProjByNatureAndUserItem.setId(ophwcSharedPrefs.getDivisionId());

        Call<NatureWorksWithProjCountModel> call = service.getAllNatureWorksWithProjCount(getProjByNatureAndUserItem);

        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<NatureWorksWithProjCountModel>() {

@Override
public void onResponse(Call<NatureWorksWithProjCountModel> call, Response<NatureWorksWithProjCountModel> response) {
        int code = response.code();
        // String rMsg = response.body().getStatus();
        Gson gson = new Gson();
        // String json = gson.toJson(response.body());
        Log.d("usercheck", code+"");
        Log.d("usercheck", "");
        switch (code) {
        // 200 is success and user is registered
        case 200:

        long statusCode = response.body().getStatusCode();
        if(statusCode==1) {
        if (response.body().getNatureWorkList() != null) {
        natureWorkLists = response.body().getNatureWorkList();
        if (natureWorkLists.size() > 0) {
        // getNaturkNames();
        setAdapters();
        }

        } else {
        Toast.makeText(SuperNatureWorkActivity.this, response.body().getStatusMessage() + "", Toast.LENGTH_SHORT).show();


        }
        }


        if(progressDialog!=null)
        progressDialog.dismiss();
        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


        break;
        case 2:
        if(progressDialog!=null){
        progressDialog.dismiss();
        }
        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

        break;
// code other than 200
default:
        if(progressDialog!=null){
        progressDialog.dismiss();
        }
        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
        break;

        }


        }



@Override
public void onFailure(Call<NatureWorksWithProjCountModel> call, Throwable t) {
        String tMsg = t.toString();
        Log.d("check", tMsg);
        if(progressDialog!=null){
        progressDialog.dismiss();
        }

        Toast.makeText(SuperNatureWorkActivity.this,"Unable to get Nature of work",Toast.LENGTH_SHORT).show();

        // mLoginFormView.setVisibility(View.VISIBLE);
        //showProgress(false);

        }

        });
        }



private void setAdapters() {
        gridView.setAdapter(new DivisionAdapter(this,natureWorkLists));
        }

        }

