package com.ophwc.gemini.superuser;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.GetAllUsersModel;
import com.ophwc.gemini.model.user.UserList;
import com.ophwc.gemini.projects.map.ProjectsMapsActivity;
import com.ophwc.gemini.superuser.project.ProjectsListActivity;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.sample.groupchatwebrtc.activities.BaseActivity;
import com.quickblox.sample.groupchatwebrtc.activities.LoginActivity;
import com.quickblox.sample.groupchatwebrtc.activities.OpponentsActivity;
import com.quickblox.sample.groupchatwebrtc.activities.PermissionsActivity;
import com.quickblox.sample.groupchatwebrtc.activities.SplashActivity;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.services.CallService;
import com.quickblox.sample.groupchatwebrtc.services.CheckCallService;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.PermissionsChecker;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SuperuserDivisions extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    SharedPrefsHelper sharedPrefsHelper;
    DrawerLayout drawer;
    NavigationView navigationView;
    List<DivisionList> divisionLists = new ArrayList<>();
    GridView gridView;
    OPHWCSharedPrefs ophwcSharedPrefs;
    private PermissionsChecker checker;
    private QbUsersDbManager dbManager;
    List<NatureWorkList> natureWorkLists =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(SuperuserDivisions.this);
        initialiseToolbar();
        sharedPrefsHelper = SharedPrefsHelper.getInstance();
        //sharedPrefsHelper.save(Consts.PREF_LOGIN_AS,3);
        gridView = (GridView) findViewById(R.id.gridView);
        TextView tvDivSuperName = (TextView) findViewById(R.id.tvDivSuperName);
        tvDivSuperName.setVisibility(View.GONE);
        checker = new PermissionsChecker(getApplicationContext());
        dbManager = QbUsersDbManager.getInstance(getApplicationContext());
        Log.e("QBUSER",sharedPrefsHelper.getQbUser().getFullName());
        Log.e("QBUSER",sharedPrefsHelper.getQbUser().getFileId()+"");
       /* startService(new Intent(this, CheckCallService.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, CheckCallService.class);
        PendingIntent pintent = PendingIntent
                .getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                5*1000, pintent);*/
        CallService.start(SuperuserDivisions.this,sharedPrefsHelper.getQbUser());

        if(UsersUtils.isNetworkAvailable(SuperuserDivisions.this)) {
            getAllDivisionService();
        }else{
            Toast.makeText(SuperuserDivisions.this,"No internet connection available",Toast.LENGTH_LONG).show();

        }



        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //divisionLists.
                if(UsersUtils.isNetworkAvailable(SuperuserDivisions.this)) {
                    ophwcSharedPrefs.putDivisionId(divisionLists.get(position).getId());
                    startActivity(new Intent(SuperuserDivisions.this, SuperNatureWorkActivity.class)
                            .putExtra("divId", divisionLists.get(position).getId()).putExtra("divName", divisionLists.get(position).getDivisionName()));
                }else{
                    Toast.makeText(SuperuserDivisions.this,"No internet connection available",Toast.LENGTH_LONG).show();

                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
       // initialiseToolbar();


    }
    private void checkPermission() {
        if (checker.lacksPermissions(Consts.PERMISSIONS)) {
            startPermissionsActivity(false);
        }
    }
    private void startPermissionsActivity(boolean checkOnlyAudio) {
        PermissionsActivity.startActivity(this, checkOnlyAudio, Consts.PERMISSIONS);
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Divisions");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_camera).setVisible(true);
        nav_Menu.findItem(R.id.nav_manage).setVisible(false);
        //nav_Menu.findItem(R.id.nav_manage_setting).setVisible(false);

        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        TextView tvDrawerHeaderEmail = (TextView) headerView.findViewById(R.id.tvDrawerHeaderEmail);
        TextView tvDrawerHeaderName = (TextView) headerView.findViewById(R.id.tvDrawerHeaderName);

        // tvDrawerHeaderEmail.setText((OPHWCSharedPrefs.getInstance(SuperuserDivisions.this).getUserEmail()));

        tvDrawerHeaderName.setText(OPHWCSharedPrefs.getInstance(SuperuserDivisions.this).getUserName());
        tvDrawerHeaderEmail.setText(OPHWCSharedPrefs.getInstance(SuperuserDivisions.this).getUserEmail()+"\n"+"Version : "+UsersUtils.VERSION_NUMBER);

    }
    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            sharedPrefsHelper.save(Consts.PREF_LOGIN_AS,-1);
            //System.exit(0);
            finish();
            startActivity(new Intent(this, LoginActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            OpponentsActivity.start(SuperuserDivisions.this,false);
        } else if (id == R.id.nav_gallery) {


        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {
            startActivity(new Intent(SuperuserDivisions.this,ProjectsMapsActivity.class));
            //Toast.makeText(SuperuserDivisions.this,"Under process",Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getAllDivisionService() {
        ProgressDialog progressDialog = new ProgressDialog(SuperuserDivisions.this);
        progressDialog.setMessage("Getting divisions...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(SuperuserDivisions.this);
        int userId = ophwcSharedPrefs.getUserId();
        ProjectsItem projectsItem = new ProjectsItem();
        projectsItem.setId(userId);

        Call<divisionsModel> call = service.getDivisions();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<divisionsModel>() {

            @Override
            public void onResponse(Call<divisionsModel> call, Response<divisionsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if( response.body().getDivisionList()!=null) {
                            divisionLists.clear();
                            divisionLists = response.body().getDivisionList();
                           setAdapters();
                        }else{
                            Toast.makeText(SuperuserDivisions.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                        }
                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

                loadUser();


            }



            @Override
            public void onFailure(Call<divisionsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }
                loadUser();


                Toast.makeText(SuperuserDivisions.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void loadUser() {
        startLoadUsers();
        /*if(dbManager.getAllUsers().size()==0) {
            startLoadUsers();
        }else{
            checkPermission();
        }*/

    }

    private void setAdapters() {
        gridView.setAdapter(new SuperDivisionAdapter(this,divisionLists));
    }
    private void startLoadUsers() {
        showProgressDialog(R.string.dlg_loading_opponents);
        String currentRoomName = SharedPrefsHelper.getInstance().get(Consts.PREF_CURREN_ROOM_NAME);
        requestExecutor.loadUsersByTag(currentRoomName, new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> result, Bundle params) {
                hideProgressDialog();
                dbManager.clearDivUserDB();
                dbManager.clearDB();
                Log.d("CALLOFF",result.size()+"");
                dbManager.saveAllUsers(result, true);
                checkPermission();
                //initUsersList();
            }

            @Override
            public void onError(QBResponseException responseException) {
                hideProgressDialog();
                checkPermission();
                showErrorSnackbar(R.string.loading_users_error, responseException, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startLoadUsers();
                    }
                });
            }
        });
    }
}

