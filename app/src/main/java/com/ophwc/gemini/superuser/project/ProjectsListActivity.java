package com.ophwc.gemini.superuser.project;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.byNaturUser.GetProjByNatureAndUserItem;
import com.ophwc.gemini.model.project.byNature.GetProjectByNatureItem;
import com.ophwc.gemini.model.project.byNature.GetProjectsByNatureModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ProjectsListActivity extends AppCompatActivity{
    List<PrjectList> projectList = new ArrayList<>();
   private static List<PrjectList> UnmuteprojectList = new ArrayList<>();
    ListView projectListView;
    SearchView edtProjSearch ;
    ProjectsListAdapter projectsListAdapter;
    LinearLayout lnrNoProjects;
    TextView tvNoProjectsAvailable;

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("State","onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("State","onstop()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllProjectService();
        hideKeyBoard();
        Log.e("State","onResume()");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("State","onCreate()");
        setContentView(R.layout.activity_projects_list);

        projectListView = (ListView) findViewById(R.id.projects_list_view);
        edtProjSearch = (SearchView) findViewById(R.id.edtProjSearch);
        lnrNoProjects = (LinearLayout) findViewById(R.id.lnrNoProjects);
        tvNoProjectsAvailable = (TextView) findViewById(R.id.tvNoProjectsAvailable);

        edtProjSearch.setQueryHint("Search projects");
        edtProjSearch.setIconifiedByDefault(false);

        edtProjSearch.clearFocus();
        hideKeyBoard();
        edtProjSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                filter(text);
                return false;
            }
        });
        //getAllProjectService();
        Toolbar projectstoolbar = (Toolbar) findViewById(R.id.projectstoolbar);
        setSupportActionBar(projectstoolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Projects");



    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        //if(imm.isActive())
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void getAllProjectService() {
        ProgressDialog progressDialog = new ProgressDialog(ProjectsListActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        GetProjectByNatureItem projectsItem = new GetProjectByNatureItem();
        int id = 0;

        try{
            id = Integer.parseInt(getIntent().getStringExtra("divId"));
        }catch (Exception e){

        }
        projectsItem.setId(OPHWCSharedPrefs.getInstance(ProjectsListActivity.this).getDivisionId());

        List<NatureOfProject> natureOfProjects = new ArrayList<>();

            natureOfProjects.add(new NatureOfProject(id));

        projectsItem.setNatureOfProject(natureOfProjects);


        //Call<ProjectsModel> call = service.getAllUserProjects(projectsItem);
        ProjectsItem projectsItem1 = new ProjectsItem();
        projectsItem1.setId(id);
        Call<GetProjectsByNatureModel> call = service.getAllProjectsbyNature(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetProjectsByNatureModel>() {

            @Override
            public void onResponse(Call<GetProjectsByNatureModel> call, Response<GetProjectsByNatureModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        projectList.clear();
                        UnmuteprojectList.clear();
                        if(response.body().getPrjectList()!=null) {
                            projectList = response.body().getPrjectList();
                            UnmuteprojectList = response.body().getPrjectList();


                            setAdapters("");
                        }else{
                            setAdapters("");
                            Toast.makeText(ProjectsListActivity.this,response.body().getStatusMessage(),Toast.LENGTH_SHORT).show();

                        }
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(ProjectsListActivity.this,"Unable to get projects",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<GetProjectsByNatureModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(ProjectsListActivity.this,"Unable to get projects",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void setAdapters(String text) {
        if(projectList!=null) {
            if(projectList.size()>0) {
                projectListView.setVisibility(View.VISIBLE);
                lnrNoProjects.setVisibility(View.GONE);
                projectsListAdapter = new ProjectsListAdapter(getBaseContext(), projectList, text,ProjectsListActivity.this);
                projectListView.setAdapter(projectsListAdapter);
            }else{
                projectListView.setVisibility(View.GONE);
                lnrNoProjects.setVisibility(View.VISIBLE);
                tvNoProjectsAvailable.setText("No projects available");
            }
        }else{
            projectListView.setVisibility(View.GONE);
            lnrNoProjects.setVisibility(View.VISIBLE);
            tvNoProjectsAvailable.setText("No projects available");
        }
    }



    // Filter Class
    public void filter(String charText) {
        UnmuteprojectList.size();
      //  projectList.clear();
        charText = charText.toLowerCase(Locale.getDefault());
        setAdapters(charText);



        //projectsListAdapter.notifyDataSetChanged();
    }
}
