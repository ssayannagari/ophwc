package com.ophwc.gemini.superuser.project;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.divsuperuser.SuperUserImagesGrid;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.offline.OfflineImagesGrid;
import com.ophwc.gemini.projects.LocationTaggingActivity;
import com.ophwc.gemini.projects.gallery.ImagesActivity;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;

import java.util.ArrayList;
import java.util.List;

public class ProjectsListAdapter extends BaseAdapter {
    Context mcontext;
    String filterText;
    AlertDialog alertDialog;
    List<PrjectList> mProjectList = new ArrayList<>();
    Activity parentAct;
    public ProjectsListAdapter(Context baseContext) {
        mcontext = baseContext;
    }

    public ProjectsListAdapter(Context baseContext, List<PrjectList> projectList) {
        mcontext = baseContext;
        mProjectList = projectList;
    }

    public ProjectsListAdapter(Context baseContext, List<PrjectList> projectList, String text, Activity projectsListActivity) {

        mcontext = baseContext;
        //mProjectList = projectList;
        filterText = text;
        parentAct = projectsListActivity;

        if(text.equalsIgnoreCase("")){
            mProjectList = projectList;

        }else {
            for (PrjectList pl : projectList) {
                if (pl.getProjectName().toLowerCase().contains(text.toLowerCase())) {
                    mProjectList.add(pl);
                }
            }
        }
    }


    public void showAlertDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(parentAct);


        alertDialogBuilder.setTitle("OPHWC");
        alertDialogBuilder.setMessage("No Images are uploaded for this project yet.");

        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        alertDialog.dismiss();

                    }
                });


        alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public ProjectsListAdapter(Context baseContext, List<PrjectList> projectList, String text) {
        mcontext = baseContext;
        //mProjectList = projectList;
        filterText = text;

        if(text.equalsIgnoreCase("")){
            mProjectList = projectList;

        }else {
            for (PrjectList pl : projectList) {
                if (pl.getProjectName().toLowerCase().contains(text.toLowerCase())) {
                    mProjectList.add(pl);
                }
            }
        }
    }

    @Override
    public int getCount() {
        return mProjectList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;

        LayoutInflater inflator =(LayoutInflater.from(mcontext));
        if(convertView==null)
            vi = inflator.inflate(R.layout.projects_list_row, null);

        TextView title = (TextView)vi.findViewById(R.id.listview_item_title); // title
        TextView tvProjectLocation = (TextView)vi.findViewById(R.id.tvProjectLocation); //
        TextView tvAssignedTo = (TextView)vi.findViewById(R.id.tvAssignedTo);
        TextView tvProjectStatus = (TextView)vi.findViewById(R.id.tvProjectStatus);
        ImageView listview_image = (ImageView) vi.findViewById(R.id.listview_image);
        tvAssignedTo.setText("Project Name: "+mProjectList.get(position).getProjectName());
        //position++;

        title.setText(mProjectList.get(position).getProjectDefination());
        tvProjectLocation.setText(mProjectList.get(position).getProjectLocation());
        int imgCount = 0;
        if(mProjectList.get(position).getImagesPaths()==null){
            imgCount =0;
        }else{
            imgCount = mProjectList.get(position).getImagesPaths().size();
        }

        if(imgCount==0){
            tvProjectStatus.setText("No Images available");
            listview_image.setImageResource(R.drawable.ic_ophwcdefault);
            title.setTextColor(mcontext.getResources().getColor(R.color.text_color_medium_grey));
            tvProjectLocation.setTextColor(mcontext.getResources().getColor(R.color.text_color_medium_grey));
            tvAssignedTo.setTextColor(mcontext.getResources().getColor(R.color.text_color_medium_grey));
            tvProjectStatus.setTextColor(mcontext.getResources().getColor(R.color.light_red));
        }else{
            listview_image.setImageResource(R.mipmap.ophwc_logo);
            title.setTextColor(mcontext.getResources().getColor(R.color.color_primary_dark));
            tvProjectLocation.setTextColor(mcontext.getResources().getColor(R.color.color_primary));
            tvAssignedTo.setTextColor(mcontext.getResources().getColor(R.color.color_primary));
            tvProjectStatus.setTextColor(mcontext.getResources().getColor(R.color.color_primary));
            if(imgCount>1) {
                tvProjectStatus.setText(imgCount + " images availbale");
            }else{
                tvProjectStatus.setText(imgCount + " image availbale");
            }
        }
        vi.setTag(position);
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos =  ( Integer)v.getTag();
                int imgCount = 0;
                if(mProjectList.get(pos).getImagesPaths()==null){
                    imgCount = 0;
                }else {
                    imgCount =mProjectList.get(pos).getImagesPaths().size();
                }

                if(UsersUtils.isNetworkAvailable(mcontext)) {

                    boolean isLocationTagged = mProjectList.get(pos).getLangitude() == null || mProjectList.get(pos).getLatitude() == null ||
                            mProjectList.get(pos).getLangitude().equals("") || mProjectList.get(pos).getLatitude().equals("") || mProjectList.get(pos).getLangitude().equals("null") || mProjectList.get(pos).getLatitude().equals("null");
                    if (isLocationTagged) {
                        mcontext.startActivity(new Intent(mcontext, LocationTaggingActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("projId", mProjectList.get(pos).getId())
                                .putExtra("divId", "" + OPHWCSharedPrefs.getInstance(mcontext).getDivisionId())
                                .putExtra("projName", mProjectList.get(pos).getProjectName())
                                .putExtra("projDef", mProjectList.get(pos).getProjectDefination())
                                .putExtra("proLoc", mProjectList.get(pos).getProjectLocation()));
                    } else {
                        mcontext.startActivity(new Intent(mcontext, SuperUserImagesGrid.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("projId", mProjectList.get(pos).getId())
                                .putExtra("divId", "" + OPHWCSharedPrefs.getInstance(mcontext).getDivisionId())
                                .putExtra("projName", mProjectList.get(pos).getProjectName())
                                .putExtra("projDef", mProjectList.get(pos).getProjectDefination())
                                .putExtra("lat", mProjectList.get(pos).getLatitude())
                                .putExtra("lng", mProjectList.get(pos).getLangitude()));

                    }
                }else{
                    mcontext.startActivity(new Intent(mcontext, OfflineImagesGrid.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra("projId", mProjectList.get(pos).getId())
                            .putExtra("divId", "" + OPHWCSharedPrefs.getInstance(mcontext).getDivisionId())
                            .putExtra("projName", mProjectList.get(pos).getProjectName())
                            .putExtra("projDef", mProjectList.get(pos).getProjectDefination())
                            .putExtra("lat", "")
                            .putExtra("lng", ""));

                }
            }
        });

        return vi;
    }
}
