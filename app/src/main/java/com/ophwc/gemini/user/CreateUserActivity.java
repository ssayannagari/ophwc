package com.ophwc.gemini.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.project.GetProjectByDivNatureItem;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.byNature.GetProjectByNatureItem;
import com.ophwc.gemini.model.project.byNature.GetProjectsByNatureModel;
import com.ophwc.gemini.model.project.createProject.NatureList;
import com.ophwc.gemini.model.project.createProject.ProjList;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.user.CreateUserItem;
import com.ophwc.gemini.model.user.CreateUserModel;
import com.ophwc.gemini.model.user.Divisions;
import com.ophwc.gemini.model.user.ProjectIDList;
import com.ophwc.gemini.model.user.UserTypes;
import com.ophwc.gemini.model.user.UserTypesList;
import com.ophwc.gemini.model.user.UsertypeModel;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateUserActivity extends AppCompatActivity {
    SharedPrefsHelper sharedPrefsHelper;
    List<DivisionList> divisionLists =new ArrayList<>();
    List<Long> natureIds = new ArrayList<>();
    List<NatureWorkList> natureWorkLists =new ArrayList<>();
    List<UserTypesList> userTypesLists =new ArrayList<>();
    List<PrjectList> projectList = new ArrayList<>();
    ArrayList<Integer> selProjIds = new ArrayList<>();
    List<ProjectIDList> projectIDLists = new ArrayList<>();
    int iii =0;
    int jjj =0;
    MultiSelectSpinner multiSelectSpinner,natureSpinner;
    Spinner divSpinner;
    TextView tvProjectSelect;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,15})";
    private static final String USERNAME_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,32})";
    Matcher matcher;
    Pattern pattern;
    public boolean validate(final String matchStr,String patternStr){
        pattern = Pattern.compile(patternStr);

        matcher = pattern.matcher(matchStr);
        return matcher.matches();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);




    }

    @Override
    protected void onResume() {
        super.onResume();
        getAllUserTypes();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    Toast.makeText(CreateUserActivity.this,"Space is not allowed here",Toast.LENGTH_SHORT).show();
                    return "";
                }
            }
            return null;
        }

    };
    private void showAddUserDialog(int i, List<String> divisionNames) {

        final Dialog dialog = new Dialog(CreateUserActivity.this);
        dialog.setContentView(R.layout.add_user_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);


        Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddUser);
        Button cancelDilog = (Button) dialog.findViewById(R.id.btnCanceldlg);
        final TextInputEditText edtUserName = (TextInputEditText) dialog.findViewById(R.id.edtAddUserName);
        final TextInputEditText edtFullName = (TextInputEditText) dialog.findViewById(R.id.edtAddFullName);
        final TextInputEditText edtUserMobile = (TextInputEditText) dialog.findViewById(R.id.edtAddUserMobile);
        final TextInputEditText edtVendorName = (TextInputEditText) dialog.findViewById(R.id.edtAddVendorName);
        final TextInputEditText edtUserPassword = (TextInputEditText) dialog.findViewById(R.id.edtUserPassword);
        final TextView tvDivDialog = (TextView) dialog.findViewById(R.id.tvDivDialog);
       // edtUserName.setFilters(new InputFilter[] { filter });
        ///edtUserPassword.setFilters(new InputFilter[] { filter });
        tvProjectSelect = (TextView) dialog.findViewById(R.id.tvProjDialog);
        final TextView tvNatureDialog = (TextView) dialog.findViewById(R.id.tvNatureDialog);
        final Spinner userTypeSpinner = (Spinner) dialog.findViewById(R.id.userTypeSpinner);
        divSpinner = (Spinner) dialog.findViewById(R.id.divSpinner);
        natureSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.natureSpinner);

        btnAddUser.setText("Add User");


        //String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";

        List<String> userTypeList = new ArrayList<>();
        userTypeList.add("Admin");
        userTypeList.add("Division user");
        //userTypeList.add("Supervisor");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, getUserTypeNames());
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        userTypeSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, divisionNames);
        // Drop down layout style - list view with radio button
        divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> natureNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, getNaturkNames());
        // Drop down layout style - list view with radio button

        multiSelectSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.multiselectSpinner);

        // attaching data adapter to spinner
        divSpinner.setAdapter(divNameAdapter);
        divSpinner.setVisibility(View.GONE);
        tvDivDialog.setVisibility(View.GONE);
        tvProjectSelect.setVisibility(View.GONE);


        natureSpinner.setVisibility(View.GONE);
        tvNatureDialog.setVisibility(View.GONE);
        multiSelectSpinner.setVisibility(View.GONE);
        divSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int divCode = divisionLists.get(position).getId();
                if(userTypeSpinner.getSelectedItemPosition()!=3) {
                    natureSpinner.setVisibility(View.VISIBLE);
                }
                if(selProjIds!=null){
                    selProjIds.clear();
                }
                if(projectList!=null){
                    projectList.clear();
                }
                StringBuffer sb = new StringBuffer();
                sb.append("Selected Projects\n");
                sb.append("\n");
                sb.append("No project selected");
                tvProjectSelect.setText(sb.toString());



                if(iii == 0){
                 initNatureMultiSpinner(divCode);
                }
                iii++;

                //getAllProjectService(1,divCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(userTypesLists.get(position).getId()==2){

                    tvDivDialog.setVisibility(View.VISIBLE);
                    divSpinner.setVisibility(View.VISIBLE);
                    tvProjectSelect.setVisibility(View.VISIBLE);


                    // multiSelectSpinner.setVisibility(View.VISIBLE);

                    natureSpinner.setVisibility(View.VISIBLE);
                    tvNatureDialog.setVisibility(View.VISIBLE);
                }else if(userTypesLists.get(position).getId()==4){

                    tvDivDialog.setVisibility(View.VISIBLE);
                    divSpinner.setVisibility(View.VISIBLE);
                    tvProjectSelect.setVisibility(View.GONE);
                    tvNatureDialog.setVisibility(View.GONE);
                    natureSpinner.setVisibility(View.GONE);
                    multiSelectSpinner.setVisibility(View.GONE);
                }else{
                    tvDivDialog.setVisibility(View.GONE);
                    divSpinner.setVisibility(View.GONE);
                    tvProjectSelect.setVisibility(View.GONE);
                    tvNatureDialog.setVisibility(View.GONE);
                    natureSpinner.setVisibility(View.GONE);
                    multiSelectSpinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });

        cancelDilog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null)
                    if(dialog.isShowing())
                        dialog.dismiss();
                finish();
            }

        });

        // if button is clicked, close the custom dialog
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int divCode = -1;
                int userTypeCode = userTypesLists.get(userTypeSpinner.getSelectedItemPosition()).getId();
                String strUserType = !String.valueOf(userTypeSpinner.getSelectedItem()).isEmpty()?String.valueOf(userTypeSpinner.getSelectedItem()):"";
                String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";
                String strUserMobile = !edtUserMobile.getText().toString().trim().isEmpty()?edtUserMobile.getText().toString():"";
                String stremail = !edtVendorName.getText().toString().trim().isEmpty()?edtVendorName.getText().toString():"";
                String stredtFullName = !edtFullName.getText().toString().trim().isEmpty()?edtFullName.getText().toString():"";

                if(strUserType.equals("")){
                    Toast.makeText(CreateUserActivity.this,"Please select User Type",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(userTypeCode==2){
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = divisionLists.get(pos).getId();
                }else if(userTypeCode==4){
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = divisionLists.get(pos).getId();
                }
                else{
                    divCode = 1;
                }
                if((userTypeCode==2||userTypeCode==4)&&divCode==-1){
                    Toast.makeText(CreateUserActivity.this,"Please select Division",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(userTypeCode==2&&(natureIds==null||natureIds.size()==0)){

                }
                if(userTypeCode==2&&(selProjIds==null||selProjIds.size()==0)){

                }

                if(strUserName.equals("")){
                    Toast.makeText(CreateUserActivity.this,"Please enter User name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strUserName.length()<8){
                    Toast.makeText(CreateUserActivity.this,"User name should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(strUserName.length()>32){
                    Toast.makeText(CreateUserActivity.this,"User name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(strUserName,USERNAME_PATTERN)){
                    Toast.makeText(CreateUserActivity.this,
                            "User name should contain at least One Upper case, one lower case and one Number",Toast.LENGTH_LONG).show();

                    return;
                }
                if(edtUserPassword.getText().toString().equals("")){
                    Toast.makeText(CreateUserActivity.this,"Please enter password",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(edtUserPassword.getText().toString().length()<8){
                    Toast.makeText(CreateUserActivity.this,"Password should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                } if(edtUserPassword.getText().toString().length()>15){
                    Toast.makeText(CreateUserActivity.this,"Password should contain max of 15 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(edtUserPassword.getText().toString(),PASSWORD_PATTERN)){
                    Toast.makeText(CreateUserActivity.this,
                            "Password should contain at least One Upper case, one lower case,one Number and a special char",Toast.LENGTH_LONG).show();

                    return;
                }
                //====
                if(stredtFullName.equals("")){
                    Toast.makeText(CreateUserActivity.this,"Please enter User full name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(stredtFullName.length()<4){
                    Toast.makeText(CreateUserActivity.this,"User full name should contain minimum of 4 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(stredtFullName.length()>32){
                    Toast.makeText(CreateUserActivity.this,"User full name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                ///====
                if(strUserMobile.length()!=10){
                    Toast.makeText(CreateUserActivity.this,"Please enter 10 digit Mobile no",Toast.LENGTH_SHORT).show();

                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(stremail).matches()){
                    Toast.makeText(CreateUserActivity.this,"Please enter valid email ID",Toast.LENGTH_SHORT).show();

                    return;
                }
                createUserService(divCode, strUserName, strUserType, userTypeCode, stremail, strUserMobile, "Profile pic", edtUserPassword.getText().toString(),getCreateUserItem(),stredtFullName);

                Log.d("UserInputs","Pos "+ userTypeCode+" "+strUserMobile+" "+strUserName+" "+strUserType);
                dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    private void createUserService(int divCode, String strUserName, String strUserType, int userTypeCode, String stremail,
                                   String strUserMobile, String profile_pic, String s,
                                   List<NatureList> UserItem,String fullName) {

        ProgressDialog progressDialog = new ProgressDialog(CreateUserActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        CreateUserItem createUserItem = new CreateUserItem();
        createUserItem.setUsername(strUserName);
        Divisions divisions = new Divisions();
        divisions.setId(divCode);
        UserTypes userTypes = new UserTypes();
        userTypes.setId(userTypeCode);
        createUserItem.setDivisions(divisions);
        createUserItem.setUserTypes(userTypes);
        createUserItem.setEmailId(stremail);
        createUserItem.setFirstName(fullName);
        createUserItem.setLastName(fullName);
        createUserItem.setPhoneNum(strUserMobile);
        createUserItem.setPassword(s);
        createUserItem.setNatureList(UserItem);
        createUserItem.setStatus("Y");
        //createUserItem.set("");

        Call<CreateUserModel> call = service.createUser(createUserItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<CreateUserModel>() {

            @Override
            public void onResponse(Call<CreateUserModel> call, Response<CreateUserModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:


                        int status = response.body().getStatusCode();
                        Log.e("status " , ""+status);
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        if(status==1) {
                            Toast.makeText(CreateUserActivity.this, "User created successfully", Toast.LENGTH_SHORT).show();

                           finish();
                            startActivity(new Intent(CreateUserActivity.this, UsersActivity.class));
                        }else{
                            Toast.makeText(CreateUserActivity.this,response.body().getStatusMessage() , Toast.LENGTH_SHORT).show();

                        }
                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(CreateUserActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(CreateUserActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<CreateUserModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateUserActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }

    private List<NatureList> getCreateUserItem() {
        List<NatureList>  natureWorkListsItem = new ArrayList<>();


      try {
          if (projectList != null) {
              if (projectList.size() > 0) {
                  for (int i = 0; i < projectList.size(); i++) {
                      if (selProjIds.contains(projectList.get(i).getId())) {
                          List<ProjList> projLists = new ArrayList<>();
                          projLists.add(new ProjList(projectList.get(i).getId()));
                          natureWorkListsItem.add(new NatureList(projectList.get(i).getNatureOfProject().getId(), projLists));

                      }
                  }
                  return natureWorkListsItem;
              } else {
                  return null;
              }
          } else {
              return null;
          }
      }catch (Exception e){
          return null;
      }
    }

    private List<String> getDivisionNames() {
        List<String> divName = new ArrayList<>();
        divName.clear();
        if(divisionLists!=null) {
            for (int i=0; i<divisionLists.size();i++) {
                if(divisionLists.get(i).getDivisionName()!=null) {
                    divName.add(divisionLists.get(i).getDivisionName());
                }
            }
        }

        return divName;
    }
    private List<String> getNaturkNames() {
        List<String> natureNames = new ArrayList<>();
        natureNames.clear();
        if(natureWorkLists!=null) {
            for (int i=0; i<natureWorkLists.size();i++) {
                if(natureWorkLists.get(i).getName()!=null) {
                    natureNames.add(natureWorkLists.get(i).getName());
                }
            }
        }

        return natureNames;
    }
    private List<String> getUserTypeNames() {
        List<String> natureNames = new ArrayList<>();
        natureNames.clear();
        if(userTypesLists!=null) {
            for (int i=0; i<userTypesLists.size();i++) {
                if(userTypesLists.get(i).getTypeName()!=null) {
                    natureNames.add(userTypesLists.get(i).getTypeName());
                }
            }
        }

        return natureNames;
    }
    private void getAllProjectByNatureService(int isAdd ,int divId, long NatureId) {
        ProgressDialog progressDialog = new ProgressDialog(CreateUserActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        GetProjectByDivNatureItem projectsItem = new GetProjectByDivNatureItem();

        /*try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        int divisionCode =  divisionLists.get(divSpinner.getSelectedItemPosition()).getId();

        projectsItem.setId(divisionCode);
        projectsItem.setUserId(0);

        List<NatureOfProject> natureOfProjects = new ArrayList<>();
        for(int i=0;i<natureIds.size();i++){
            natureOfProjects.add(new NatureOfProject(natureIds.get(i)));
        }
        projectsItem.setNatureOfProject(natureOfProjects);


        Call<GetProjectsByNatureModel> call = service.assignGetProjectsByDivisionAndNature(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetProjectsByNatureModel>() {

            @Override
            public void onResponse(Call<GetProjectsByNatureModel> call, Response<GetProjectsByNatureModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try {
                        if(response.body().getPrjectList()!=null) {
                            projectList = new ArrayList<>();
                            projectList.clear();
                            projectList = response.body().getPrjectList();

                                if (projectList != null && projectList.size() > 0) {
                                    if (isAdd == 1) {
                                        //initMultiSpinner();

                                        if (progressDialog != null)
                                            progressDialog.dismiss();
                                        showProjDialog();
                                    } else {
                                        //callUserProjectsService();
                                    }
                                } else {
                                    Toast.makeText(CreateUserActivity.this, "No Projects available for the selection", Toast.LENGTH_LONG).show();
                                }
                            }else{
                            Toast.makeText(CreateUserActivity.this, "No Projects available for the selection", Toast.LENGTH_LONG).show();

                        }
                        } catch (Exception e) {
                            Toast.makeText(CreateUserActivity.this, "No Projects available for the selection", Toast.LENGTH_LONG).show();

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<GetProjectsByNatureModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateUserActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void showProjDialog() {
        // ArrayList<Integer> selIds = new ArrayList<>();
        if(projectList!=null) {
        if(projectList.size()>0) {
            ArrayList<MultiSelectModel> selNames = new ArrayList<>();
            List<String> projName = new ArrayList<>();
           // selNames.add(new MultiSelectModel(0,"Select all"));
            try {
                for (PrjectList prjectList : projectList)
                    selNames.add(new MultiSelectModel(prjectList.getId(), prjectList.getProjectName() + "\n" + prjectList.getNatureOfProject().getName()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                    .title("Select projects") //setting title for dialog

                    .positiveText("Done")
                    .negativeText("Cancel")
                    .setMinSelectionLimit(0) //you can set minimum checkbox selection limit (Optional)
                    .setMaxSelectionLimit(selNames.size()) //you can set maximum checkbox selection limit (Optional)
                    .preSelectIDsList(selProjIds) //List of ids that you need to be selected
                    .multiSelectList(selNames) // the multi select model list with ids and name
                    .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {

                        @Override
                        public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                            //will return list of selected IDS
                            StringBuffer sb = new StringBuffer();
                            sb.append("Selected Projects\n");
                            sb.append("\n");

                            int size = selectedIds.size();
                            selProjIds = selectedIds;

                            for (int i = 0; i < size; i++) {

                                try {
                                    sb.append(selectedNames.get(i).split("\n")[0] + ", ");
                                    //  Toast.makeText(UsersActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" + "Selected Names : " + selectedNames.get(i) + "\n" + "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                    int size1 = selectedIds.size();
                                } catch (Exception e) {
                                    onCancel();
                                }

                            }
                            tvProjectSelect.setVisibility(View.VISIBLE);
                            sb.append("\n");
                            tvProjectSelect.setText(sb.toString());
                            tvProjectSelect.setMaxLines(3);

                            tvProjectSelect.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    try {
                                        if (natureIds.size() > 0) {
                                            showProjDialog();
                                        }
                                    }catch (Exception e){
                                        Toast.makeText(CreateUserActivity.this,"Please select Nature of project",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            // multiSelectSpinner.setVisibility(View.VISIBLE);

                        }


                        @Override
                        public void onCancel() {
                            Log.d("Multi", "Dialog cancelled");
                        }


                    });


            multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
        }else{getAllProjectByNatureService(1,0,0);}
        }else{

        }
    }
    private void getAllUserTypes() {
        ProgressDialog progressDialog = new ProgressDialog(CreateUserActivity.this);
        progressDialog.setMessage("Getting User types...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<UsertypeModel> call = service.getUsertypes();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UsertypeModel>() {

            @Override
            public void onResponse(Call<UsertypeModel> call, Response<UsertypeModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1){
                            if(response.body().getUserTypesList()!=null) {
                                userTypesLists = response.body().getUserTypesList();
                                if (userTypesLists.size() > 0) {
                                    getAllDivisionService();
                                    /// getNaturkNames();

                                   // showAddUserDialog(1, getDivisionNames());
                                }else{
                                    Toast.makeText(CreateUserActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                                }
                            }else{
                                Toast.makeText(CreateUserActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                            }

                        }else{
                            Toast.makeText(CreateUserActivity.this,response.body().getStatusMessage()+"",Toast.LENGTH_SHORT).show();


                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<UsertypeModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateUserActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllNatureOfWorks() {
        ProgressDialog progressDialog = new ProgressDialog(CreateUserActivity.this);
        progressDialog.setMessage("Getting Nature of projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<NatureOfWorkModel> call = service.getNatureOfWork();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<NatureOfWorkModel>() {

            @Override
            public void onResponse(Call<NatureOfWorkModel> call, Response<NatureOfWorkModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1){
                            if(response.body().getNatureWorkList()!=null) {
                                natureWorkLists = response.body().getNatureWorkList();
                                if (natureWorkLists.size() > 0) {
                                    /// getNaturkNames();

                                    showAddUserDialog(1, getDivisionNames());
                                }
                            }

                        }else{
                            Toast.makeText(CreateUserActivity.this,response.body().getStatusMessage()+"",Toast.LENGTH_SHORT).show();


                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<NatureOfWorkModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateUserActivity.this,"Unable to get Nature of work",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllDivisionService() {
        ProgressDialog progressDialog = new ProgressDialog(CreateUserActivity.this);
        progressDialog.setMessage("Getting divisions...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<divisionsModel> call = service.getDivisions();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<divisionsModel>() {

            @Override
            public void onResponse(Call<divisionsModel> call, Response<divisionsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        divisionLists.clear();
                        if( response.body().getDivisionList()!=null) {
                            divisionLists = response.body().getDivisionList();

                            if (divisionLists.size() > 0) {
                                if (true) {
                                    getAllNatureOfWorks();


                                } else {
                                    //showEditUserDialog(i,getDivisionNames(),usersObj);
                                }

                            }
                        }else{

                        }

                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<divisionsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(CreateUserActivity.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void initNatureMultiSpinner(int divCode) {
        List<String> natureName = new ArrayList<>();
        natureName = getNaturkNames();


        ArrayAdapter<String> adapter = new ArrayAdapter <String>(CreateUserActivity.this, android.R.layout.simple_list_item_multiple_choice, natureName);

        natureSpinner.setListAdapter(adapter).setSelectAll(false).setAllCheckedText("Selected All").setTitle("Select Nature(s)")
                .setMinSelectedItems(1).setListener( new BaseMultiSelectSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                natureIds.clear();
                for(int i = 0; i<selected.length;i++){
                    if(selected[i]){

                        natureIds.add(natureWorkLists.get(i).getId());

                    }



                }
                getAllProjectByNatureService(1,divCode,0);

            }
        });
/*
        List<PrjectList> prjectLists2 = projectList1;
        //prjectLists2 = projectList1;

            for(int i=0; i<projectList.size();i++){
                for(int j=0;j<prjectLists2.size();j++){
                    if(prjectLists2.get(j).getId()==projectList.get(i).getId()){
                        multiSelectSpinner.selectItem(i,true);
                    }
                }
            }*/




        multiSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
