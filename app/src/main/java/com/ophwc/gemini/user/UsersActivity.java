package com.ophwc.gemini.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.R;
import com.ophwc.gemini.admin.project.ManageProjects;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.divisions.DivisionList;
import com.ophwc.gemini.model.divisions.divisionsModel;
import com.ophwc.gemini.model.project.GetProjectByDivNatureItem;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.model.project.ProjectsItem;
import com.ophwc.gemini.model.project.ProjectsModel;
import com.ophwc.gemini.model.project.byNature.GetProjectByNatureItem;
import com.ophwc.gemini.model.project.byNature.GetProjectsByNatureModel;
import com.ophwc.gemini.model.project.createProject.NatureList;
import com.ophwc.gemini.model.project.createProject.ProjList;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfProject;
import com.ophwc.gemini.model.project.natureOfWork.NatureOfWorkModel;
import com.ophwc.gemini.model.project.natureOfWork.NatureWorkList;
import com.ophwc.gemini.model.user.CreateUserItem;
import com.ophwc.gemini.model.user.CreateUserModel;
import com.ophwc.gemini.model.user.Divisions;
//import com.ophwc.gemini.model.user.GetAllUsersModdel;
import com.ophwc.gemini.model.user.GetAllUsersModel;
import com.ophwc.gemini.model.user.ProjectIDList;
import com.ophwc.gemini.model.user.UserList;
import com.ophwc.gemini.model.user.UserTypes;
import com.ophwc.gemini.model.user.UserTypesList;
import com.ophwc.gemini.model.user.UsertypeModel;
import com.ophwc.gemini.model.user.delete.DeleteUserItem;
import com.ophwc.gemini.model.user.delete.DeleteUserModel;
import com.ophwc.gemini.model.user.update.UserUpdateItem;
import com.ophwc.gemini.model.user.update.UserUpdateModel;
import com.quickblox.sample.groupchatwebrtc.activities.SplashActivity;
import com.quickblox.sample.groupchatwebrtc.db.DbHelper;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.apptik.widget.multiselectspinner.BaseMultiSelectSpinner.MultiSpinnerListener;
import io.apptik.widget.multiselectspinner.MultiSelectSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class UsersActivity extends AppCompatActivity implements MultiSpinnerListener, NavigationView.OnNavigationItemSelectedListener{

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<UsersObj> usersArrayList = new ArrayList<>();
    static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;
    int iii =0;
    int jjj =0;
    List<PrjectList> projectList = new ArrayList<>();
    ArrayList<Integer> selectedProjIds = new ArrayList<>();
    List<ProjectIDList> projectIDLists = new ArrayList<>();
    List<Long> natureIds = new ArrayList<>();
    DbHelper dbHelper;
    FloatingActionButton addUserFab;
    LinearLayout lnrNoUsers;
    QbUsersDbManager qbUsersDbManager;
    SharedPrefsHelper sharedPrefsHelper;
    List<DivisionList> divisionLists =new ArrayList<>();
    List<NatureWorkList> natureWorkLists =new ArrayList<>();
    List<UserList> mallUserList = new ArrayList<>();
    List<String> divisionsNames =new ArrayList<>();
    List<String> natureOfWorkNames =new ArrayList<>();
    UserList selUserObj = new UserList();
    AlertDialog alertDialog;
    List<UserTypesList> userTypesLists =new ArrayList<>();

    ArrayList<Integer> selProjIds = new ArrayList<>();

    int selposition = 0;
    int selUserId =0;
    MultiSelectSpinner multiSelectSpinner,natureSpinner;
    Spinner divSpinner;
    TextView tvProjectSelect;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,15})";
    private static final String USERNAME_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,32})";
    Matcher matcher;
    Pattern pattern;
    DrawerLayout drawer;
    NavigationView navigationView;


    public boolean validate(final String matchStr,String patternStr){
        pattern = Pattern.compile(patternStr);

        matcher = pattern.matcher(matchStr);
        return matcher.matches();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        // System.exit(0);
    }

    private void initialiseToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.userslogtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Manage Users");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout_user);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view_user);
        View headerView = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        TextView tvDrawerHeaderEmail = (TextView) headerView.findViewById(R.id.tvDrawerHeaderEmail);
        TextView tvDrawerHeaderName = (TextView) headerView.findViewById(R.id.tvDrawerHeaderName);

        // tvDrawerHeaderEmail.setText((OPHWCSharedPrefs.getInstance(DivisionsActivity.this).getUserEmail()));

        tvDrawerHeaderName.setText(OPHWCSharedPrefs.getInstance(UsersActivity.this).getUserName());
        tvDrawerHeaderEmail.setText(OPHWCSharedPrefs.getInstance(UsersActivity.this).getUserEmail());

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_manage_users) {
            // startActivity(new Intent(UsersActivity.this,));
        } else if (id == R.id.nav_manage_project) {
            startActivity(new Intent(UsersActivity.this, ManageProjects.class));

        }else if(id==R.id.nav_manage_setting){
            Toast.makeText(UsersActivity.this,"Under process",Toast.LENGTH_SHORT).show();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_activity);
        // dbHelper  = DbHelper.getInstance(UsersActivity.this);
        qbUsersDbManager = QbUsersDbManager.getInstance(UsersActivity.this);
        sharedPrefsHelper =SharedPrefsHelper.getInstance();
        //sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 1);

        myOnClickListener = new MyOnClickListener(UsersActivity.this);

        recyclerView = (RecyclerView) findViewById(R.id.users_recyle_view);
        addUserFab = (FloatingActionButton) findViewById(R.id.addUserFab);
        lnrNoUsers = (LinearLayout) findViewById(R.id.lnrNoUsers);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        // recyclerView.setItemAnimator(new DefaultItemAnimator());
        initialiseToolbar();


        //getSupportActionBar().setHomeButtonEnabled(true);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        setAdapters();


        removedItems = new ArrayList<Integer>();
        addUserFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //addUser();
                //getAllDivisionService(1, null);
                finish();
                startActivity(new Intent(UsersActivity.this,CreateUserActivity.class));


            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && addUserFab.getVisibility() == View.VISIBLE) {
                    addUserFab.hide();
                } else if (dy < 0 && addUserFab.getVisibility() != View.VISIBLE) {
                    addUserFab.show();
                }
            }
        });


    }
    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    Toast.makeText(UsersActivity.this,"Space is not allowed here",Toast.LENGTH_SHORT).show();
                    return "";
                }
            }
            return null;
        }

    };
    private void showEditUserDialog(int isAdd, List<String> divisionNames, UsersObj usersObj) {


        final Dialog dialog = new Dialog(UsersActivity.this);
        dialog.setContentView(R.layout.add_user_dialog);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddUser);
        Button btnDeleteUser = (Button) dialog.findViewById(R.id.btnDeleteUser);
        Button btnCanceldlg = (Button) dialog.findViewById(R.id.btnCanceldlg);
        final TextInputEditText edtUserName = (TextInputEditText) dialog.findViewById(R.id.edtAddUserName);
        final TextInputEditText edtUserMobile = (TextInputEditText) dialog.findViewById(R.id.edtAddUserMobile);
        final TextInputEditText edtVendorName = (TextInputEditText) dialog.findViewById(R.id.edtAddVendorName);
        final TextInputEditText edtUserPassword = (TextInputEditText) dialog.findViewById(R.id.edtUserPassword);
        final TextInputEditText edtFullName = (TextInputEditText) dialog.findViewById(R.id.edtAddFullName);
        final TextView tvDivDialog = (TextView) dialog.findViewById(R.id.tvDivDialog);
        final TextView tvProjDialog = (TextView) dialog.findViewById(R.id.tvProjDialog);
        final Spinner userTypeSpinner = (Spinner) dialog.findViewById(R.id.userTypeSpinner);
        //edtUserName.setFilters(new InputFilter[] { filter });
        //edtUserPassword.setFilters(new InputFilter[] { filter });
        tvProjectSelect = (TextView) dialog.findViewById(R.id.tvProjDialog);
        final TextView tvNatureDialog = (TextView) dialog.findViewById(R.id.tvNatureDialog);
        divSpinner = (Spinner) dialog.findViewById(R.id.divSpinner);
        natureSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.natureSpinner);
        multiSelectSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.multiselectSpinner);
        multiSelectSpinner.setVisibility(View.GONE);
        natureSpinner.setVisibility(View.GONE);
        divSpinner.setVisibility(View.GONE);

        btnAddUser.setText("Update User");
        btnDeleteUser.setVisibility(View.VISIBLE);
        btnCanceldlg.setVisibility(View.VISIBLE);

        btnCanceldlg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UsersActivity.this);


                alertDialogBuilder.setTitle("Delete User?");
                alertDialogBuilder.setMessage("User will be deleted and all projects assigned for this users will be removed.\n\nDo you want to proceed");

                alertDialogBuilder.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                alertDialog.dismiss();
                                callDeleteUserService(usersObj.getUserId());
                                dialog.dismiss();
                            }
                        }).setNegativeButton("No",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        alertDialog.dismiss();
                        return;
                    }
                });


                alertDialog = alertDialogBuilder.create();

                alertDialog.show();




            }
        });


        //String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";

        List<String> userTypeList = new ArrayList<>();
        userTypeList.add("Admin");
        userTypeList.add("Division user");
        //userTypeList.add("Supervisor");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, getUserTypeNames());
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        userTypeSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, divisionNames);
        // Drop down layout style - list view with radio button
        divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        divSpinner.setAdapter(divNameAdapter);

        edtUserName.setText(usersObj.getUserName());
        edtUserMobile.setText(usersObj.getMobileNo());
        edtUserPassword.setText(usersObj.get_password());
        edtVendorName.setText(usersObj.getEmailId());
        edtFullName.setText(usersObj.getFullName());
        selUserId = usersObj.getUserId();

        try {
            for (int i = 0; i < userTypesLists.size(); i++) {
                if (usersObj.getUserTypeCode() == userTypesLists.get(i).getId()) {
                    userTypeSpinner.setSelection(i);
                }
            }
        }catch (Exception e){
            userTypeSpinner.setSelection(0);
        }
        /*if(usersObj.getUserTypeCode()==1){

            userTypeSpinner.setSelection(0);
        }else if(usersObj.getUserTypeCode()==2){

            userTypeSpinner.setSelection(1);
        }inelse{
            userTypeSpinner.setSelection(2);
        }*/
        int dvId =1;
        try {

            dvId = mallUserList.get(selposition).getDivisions().getId();


            for (int i = 0; i < divisionLists.size(); i++) {


                if (divisionLists.get(i).getId() == dvId) {

                    divSpinner.setSelection(i);
                }
            }

        }catch (Exception e){}
        divSpinner.setVisibility(View.GONE);
        tvDivDialog.setVisibility(View.GONE);
        multiSelectSpinner.setVisibility(View.GONE);
        tvProjDialog.setVisibility(View.GONE);
        initNatureMultiSpinner(0);

        divSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int divCode = divisionLists.get(position).getId();

                if(userTypeSpinner.getSelectedItemPosition()!=3) {
                    natureSpinner.setVisibility(View.VISIBLE);
                }

                iii++;


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==1){

                    tvProjDialog.setVisibility(View.VISIBLE);
                    tvDivDialog.setVisibility(View.VISIBLE);
                    divSpinner.setVisibility(View.VISIBLE);
                    natureSpinner.setVisibility(View.VISIBLE);
                    tvNatureDialog.setVisibility(View.VISIBLE);
                    // multiSelectSpinner.setVisibility(View.VISIBLE);
                }else if(position==3){

                    tvDivDialog.setVisibility(View.VISIBLE);
                    divSpinner.setVisibility(View.VISIBLE);
                    tvProjectSelect.setVisibility(View.GONE);
                    tvNatureDialog.setVisibility(View.GONE);
                    natureSpinner.setVisibility(View.GONE);
                    multiSelectSpinner.setVisibility(View.GONE);
                }else{
                    tvDivDialog.setVisibility(View.GONE);
                    divSpinner.setVisibility(View.GONE);
                    tvProjectSelect.setVisibility(View.GONE);
                    tvNatureDialog.setVisibility(View.GONE);
                    multiSelectSpinner.setVisibility(View.GONE);
                    tvProjDialog.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });

        // if button is clicked, close the custom dialog
        int finalDvId = dvId;
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int divCode = -1;
                int userTypeCode = userTypesLists.get(userTypeSpinner.getSelectedItemPosition()).getId();
                String strUserType = !String.valueOf(userTypeSpinner.getSelectedItem()).isEmpty()?String.valueOf(userTypeSpinner.getSelectedItem()):"";
                String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";
                String strUserMobile = !edtUserMobile.getText().toString().trim().isEmpty()?edtUserMobile.getText().toString():"";
                String stremail = !edtVendorName.getText().toString().trim().isEmpty()?edtVendorName.getText().toString():"";
                String strFullName = !edtFullName.getText().toString().trim().isEmpty()?edtFullName.getText().toString():"";

                if(strUserType.equals("")){
                    Toast.makeText(UsersActivity.this,"Please select User Type",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(userTypeCode==2){
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = divisionLists.get(pos).getId();
                }else if(userTypeCode==4){
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = divisionLists.get(pos).getId();
                }
                else{
                    divCode = 1;
                }
                if((userTypeCode==2||userTypeCode==4)&&divCode==-1){
                    Toast.makeText(UsersActivity.this,"Please select Division",Toast.LENGTH_SHORT).show();

                    return;
                }

                if(strUserName.equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter User name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strUserName.length()<8){
                    Toast.makeText(UsersActivity.this,"User name should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(strUserName.length()>32){
                    Toast.makeText(UsersActivity.this,"User name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(strUserName,USERNAME_PATTERN)){
                    Toast.makeText(UsersActivity.this,
                            "User name should contain at least One Upper case, one lower case and one Number",Toast.LENGTH_LONG).show();

                    return;
                }
                if(edtUserPassword.getText().toString().equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter password",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(edtUserPassword.getText().toString().length()<8){
                    Toast.makeText(UsersActivity.this,"Password should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                } if(edtUserPassword.getText().toString().length()>15){
                    Toast.makeText(UsersActivity.this,"Password should contain max of 15 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(edtUserPassword.getText().toString(),PASSWORD_PATTERN)){
                    Toast.makeText(UsersActivity.this,
                            "Password should contain at least One Upper case, one lower case,one Number and a special char",Toast.LENGTH_LONG).show();

                    return;
                }
                //====
                if(strFullName.equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter User full name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strFullName.length()<4){
                    Toast.makeText(UsersActivity.this,"User full name should contain minimum of 4 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(strFullName.length()>32){
                    Toast.makeText(UsersActivity.this,"User full name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                ///====
                if(strUserMobile.length()!=10){
                    Toast.makeText(UsersActivity.this,"Please enter 10 digit Mobile no",Toast.LENGTH_SHORT).show();

                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(stremail).matches()){
                    Toast.makeText(UsersActivity.this,"Please enter valid email ID",Toast.LENGTH_SHORT).show();

                    return;
                }

                if(userTypeCode==2&&(selectedProjIds==null||selectedProjIds.size()==0)){

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UsersActivity.this);


                    alertDialogBuilder.setTitle("Remove project assignments");
                    alertDialogBuilder.setMessage("No Project selected. All projects for this users will be removed.\n Do you want to proceed");
                    int finalDivCode = divCode;
                    alertDialogBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    alertDialog.dismiss();
                                    callUpdateUserService(usersObj.getUserId(), 1, strUserName, strUserType, userTypeCode, stremail, strUserMobile, "Profile pic", edtUserPassword.getText().toString(),strFullName);

                                }
                            }).setNegativeButton("No",new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            alertDialog.dismiss();
                            return;
                        }
                    });


                    alertDialog = alertDialogBuilder.create();

                    alertDialog.show();
                    // return;
                }else{
                    callUpdateUserService(usersObj.getUserId(),divCode, strUserName, strUserType, userTypeCode, stremail, strUserMobile, "Profile pic", edtUserPassword.getText().toString(),strFullName);

                }
                iii=0;



                Log.d("UserInputs","Pos "+ userTypeCode+" "+strUserMobile+" "+strUserName+" "+strUserType);
                dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();


    }
    private void showAddUserDialog(int i, List<String> divisionNames) {

        final Dialog dialog = new Dialog(UsersActivity.this);
        dialog.setContentView(R.layout.add_user_dialog);

        Button btnAddUser = (Button) dialog.findViewById(R.id.btnAddUser);
        Button cancelBtn = (Button) dialog.findViewById(R.id.btnCanceldlg);
        final TextInputEditText edtUserName = (TextInputEditText) dialog.findViewById(R.id.edtAddUserName);
        final TextInputEditText edtAddFullName = (TextInputEditText) dialog.findViewById(R.id.edtAddFullName);
        final TextInputEditText edtUserMobile = (TextInputEditText) dialog.findViewById(R.id.edtAddUserMobile);
        final TextInputEditText edtVendorName = (TextInputEditText) dialog.findViewById(R.id.edtAddVendorName);
        final TextInputEditText edtUserPassword = (TextInputEditText) dialog.findViewById(R.id.edtUserPassword);
        final TextView tvDivDialog = (TextView) dialog.findViewById(R.id.tvDivDialog);
        tvProjectSelect = (TextView) dialog.findViewById(R.id.tvProjDialog);
        final TextView tvNatureDialog = (TextView) dialog.findViewById(R.id.tvNatureDialog);
        final Spinner userTypeSpinner = (Spinner) dialog.findViewById(R.id.userTypeSpinner);
        final Spinner divSpinner = (Spinner) dialog.findViewById(R.id.divSpinner);
        natureSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.natureSpinner);

        btnAddUser.setText("Add User");


        //String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";

        List<String> userTypeList = new ArrayList<>();
        userTypeList.add("Admin");
        userTypeList.add("Division user");
        //userTypeList.add("Supervisor");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, userTypeList);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        userTypeSpinner.setAdapter(dataAdapter);

        ArrayAdapter<String> divNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, divisionNames);
        // Drop down layout style - list view with radio button
        divNameAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> natureNameAdapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, getNaturkNames());
        // Drop down layout style - list view with radio button

        multiSelectSpinner = (MultiSelectSpinner) dialog.findViewById(R.id.multiselectSpinner);

        // attaching data adapter to spinner
        divSpinner.setAdapter(divNameAdapter);
        divSpinner.setVisibility(View.GONE);
        tvDivDialog.setVisibility(View.GONE);
        tvProjectSelect.setVisibility(View.GONE);


        natureSpinner.setVisibility(View.GONE);
        tvNatureDialog.setVisibility(View.GONE);
        multiSelectSpinner.setVisibility(View.GONE);
        divSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int divCode = divisionLists.get(position).getId();
                natureSpinner.setVisibility(View.VISIBLE);

                initNatureMultiSpinner(divCode);
                if(iii != 0){

                }
                iii++;

                //getAllProjectService(1,divCode);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        userTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==1){

                    tvDivDialog.setVisibility(View.VISIBLE);
                    divSpinner.setVisibility(View.VISIBLE);


                    // multiSelectSpinner.setVisibility(View.VISIBLE);

                    natureSpinner.setVisibility(View.VISIBLE);
                    tvNatureDialog.setVisibility(View.VISIBLE);
                }else{
                    tvDivDialog.setVisibility(View.GONE);
                    divSpinner.setVisibility(View.GONE);
                    tvProjectSelect.setVisibility(View.GONE);
                    multiSelectSpinner.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null)
                    if(dialog.isShowing())
                        dialog.dismiss();
            }
        });

        // if button is clicked, close the custom dialog
        btnAddUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int divCode = -1;
                int userTypeCode = userTypeSpinner.getSelectedItemPosition()+1;
                String strUserType = !String.valueOf(userTypeSpinner.getSelectedItem()).isEmpty()?String.valueOf(userTypeSpinner.getSelectedItem()):"";
                String strUserName = !edtUserName.getText().toString().trim().isEmpty()?edtUserName.getText().toString():"";
                String strUserMobile = !edtUserMobile.getText().toString().trim().isEmpty()?edtUserMobile.getText().toString():"";
                String stremail = !edtVendorName.getText().toString().trim().isEmpty()?edtVendorName.getText().toString():"";
                String strFullName = !edtAddFullName.getText().toString().trim().isEmpty()?edtAddFullName.getText().toString():"";

                if(strUserType.equals("")){
                    Toast.makeText(UsersActivity.this,"Please select User Type",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(userTypeCode==2){
                    int pos =  divSpinner.getSelectedItemPosition();
                    divCode = divisionLists.get(pos).getId();
                }else{
                    divCode = 1;
                }
                if(userTypeCode==2&&divCode==-1){
                    Toast.makeText(UsersActivity.this,"Please select Division",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(userTypeCode==2&&projectIDLists!=null&&projectIDLists.size()==0){
                    Toast.makeText(UsersActivity.this,"Please select Project",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strUserName.equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter User name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strUserName.length()<8){
                    Toast.makeText(UsersActivity.this,"User name should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(strUserName.length()>32){
                    Toast.makeText(UsersActivity.this,"User name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(strUserName,USERNAME_PATTERN)){
                    Toast.makeText(UsersActivity.this,
                            "User name should contain at least One Upper case, one lower case and one Number",Toast.LENGTH_LONG).show();

                    return;
                }
                if(edtUserPassword.getText().toString().equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter password",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(edtUserPassword.getText().toString().length()<8){
                    Toast.makeText(UsersActivity.this,"Password should contain minimum of 8 characters",Toast.LENGTH_SHORT).show();

                    return;
                } if(edtUserPassword.getText().toString().length()>15){
                    Toast.makeText(UsersActivity.this,"Password should contain max of 15 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(!validate(edtUserPassword.getText().toString(),PASSWORD_PATTERN)){
                    Toast.makeText(UsersActivity.this,
                            "Password should contain at least One Upper case, one lower case,one Number and a special char",Toast.LENGTH_LONG).show();

                    return;
                }
                //====
                if(strFullName.equals("")){
                    Toast.makeText(UsersActivity.this,"Please enter User full name",Toast.LENGTH_SHORT).show();

                    return;
                }
                if(strFullName.length()<4){
                    Toast.makeText(UsersActivity.this,"User full name should contain minimum of 4 characters",Toast.LENGTH_SHORT).show();

                    return;
                }  if(strFullName.length()>32){
                    Toast.makeText(UsersActivity.this,"User full name should contain max of 32 characters",Toast.LENGTH_SHORT).show();

                    return;
                }
                ///====
                if(strUserMobile.length()!=10){
                    Toast.makeText(UsersActivity.this,"Please enter 10 digit Mobile no",Toast.LENGTH_SHORT).show();

                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(stremail).matches()){
                    Toast.makeText(UsersActivity.this,"Please enter valid email ID",Toast.LENGTH_SHORT).show();

                    return;
                }
                addUser(divCode, strUserName, strUserType, userTypeCode, stremail, strUserMobile, "Profile pic", edtUserPassword.getText().toString(),projectIDLists,strFullName);


                Log.d("UserInputs","Pos "+ userTypeCode+" "+strUserMobile+" "+strUserName+" "+strUserType);
                dialog.dismiss();
                // Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
    private void showProjDialog() {
        // ArrayList<Integer> selIds = new ArrayList<>();
        ArrayList<MultiSelectModel> selNames = new ArrayList<>();
        List<String> projName = new ArrayList<>();
        try {
            for (PrjectList prjectList : projectList)
                selNames.add(new MultiSelectModel(prjectList.getId(), prjectList.getProjectName()+"\n"+prjectList.getNatureOfProject().getName()));
        }catch (Exception e){
            e.printStackTrace();
        }

        MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("Select projects") //setting title for dialog

                .positiveText("Done")
                .negativeText("Cancel")
                .setMinSelectionLimit(0) //you can set minimum checkbox selection limit (Optional)
                .setMaxSelectionLimit(projName.size()) //you can set maximum checkbox selection limit (Optional)
                .preSelectIDsList(selectedProjIds) //List of ids that you need to be selected
                .multiSelectList(selNames) // the multi select model list with ids and name
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {

                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String dataString) {
                        //will return list of selected IDS
                        StringBuffer sb = new StringBuffer();
                        sb.append("Selected Projects\n");
                        sb.append("\n");

                        int size = selectedIds.size();
                        selectedProjIds = selectedIds;

                        for (int i = 0; i < size; i++) {

                            try {
                                sb.append(selectedNames.get(i).split("\n")[0]+", ");
                                //  Toast.makeText(UsersActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" + "Selected Names : " + selectedNames.get(i) + "\n" + "DataString : " + dataString, Toast.LENGTH_SHORT).show();
                                int size1 = selectedIds.size();
                            }catch(Exception e){
                                onCancel();
                            }

                        }
                        tvProjectSelect.setVisibility(View.VISIBLE);
                        sb.append("\n");
                        tvProjectSelect.setText(sb.toString());
                        tvProjectSelect.setMaxLines(3);

                        tvProjectSelect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showProjDialog();
                            }
                        });
                        // multiSelectSpinner.setVisibility(View.VISIBLE);

                    }


                    @Override
                    public void onCancel() {
                        Log.d("Multi","Dialog cancelled");
                    }



                });
        // multiSelectDialog.


        multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
    }

    private List<String> getDivisionNames() {
        List<String> divName = new ArrayList<>();
        divisionsNames.clear();
        if(divisionLists!=null) {
            for (int i=0; i<divisionLists.size();i++) {
                if(divisionLists.get(i).getDivisionName()!=null) {
                    divisionsNames.add(divisionLists.get(i).getDivisionName());
                }
            }
        }

        return divisionsNames;
    }
    private List<String> getNaturkNames() {
        List<String> divName = new ArrayList<>();
        natureOfWorkNames.clear();
        if(natureWorkLists!=null) {
            for (int i=0; i<natureWorkLists.size();i++) {
                if(natureWorkLists.get(i).getName()!=null) {
                    natureOfWorkNames.add(natureWorkLists.get(i).getName());
                }
            }
        }

        return natureOfWorkNames;
    }

    private void setAdapters() {
        usersArrayList.clear();

        usersArrayList = qbUsersDbManager.getAllOphwcUsers();
        try {
            if (usersArrayList.size() > 0) {
                lnrNoUsers.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter = new UsersCustomAdapter(usersArrayList);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(usersArrayList.size()-1);
            }else{
                lnrNoUsers.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);

            }
        }catch (NullPointerException ne) {
            ne.printStackTrace();
        }
        //adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemsSelected(boolean[] selected) {
        for(int i = 0; i<selected.length;i++){
            if(selected[i]){
                Log.e("Selected", projectList.get(i).getProjectName());
            }
        }

    }



    private class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            removeItem(v);
        }

        private void removeItem(View v) {
            selposition =recyclerView.getChildPosition(v);
            selUserObj = mallUserList.get(recyclerView.getChildPosition(v));

            getAllDivisionService(0,usersArrayList.get(recyclerView.getChildPosition(v)));

            //Toast.makeText(context,"Pos "+recyclerView.getChildPosition(v),Toast.LENGTH_SHORT).show();
            /*int selectedItemPosition = recyclerView.getChildPosition(v);
            DbHelper dbHelper = DBHelper.getInstance(context);
            dbHelper.removeUser(selectedItemPosition);
            usersArrayList.remove(selectedItemPosition);
            adapter.notifyItemRemoved(selectedItemPosition);
*/
            /*RecyclerView.ViewHolder viewHolder
                    = recyclerView.findViewHolderForPosition(selectedItemPosition);

            usersArrayList.remove(selectedItemPosition);
            adapter.notifyItemRemoved(selectedItemPosition);*/
        }
    }





    private void addUser(int userId, String userName, String strUserType,
                         int userTypeCode, String userMail, String strUserMobile,
                         String userProfilepic, String password, List<ProjectIDList> prjctIDList,String fullName) {

        usersArrayList.clear();
        usersArrayList.add(new UsersObj(userId,userName,strUserType,
                userTypeCode,userMail,strUserMobile,userProfilepic,password,fullName));

        // callCreateUserService(usersArrayList.get(0));



    }



    @Override
    protected void onResume() {
        super.onResume();
        if(UsersUtils.isNetworkAvailable(UsersActivity.this)) {
            if (SharedPrefsHelper.getInstance().get(Consts.PREF_LOGIN_AS, -1) != -1) {
                getAllUsersService();
            } else {
                //finish();
                //  System.exit(0);
            }
        }else{
          Toast.makeText(UsersActivity.this,"No internet connection available",Toast.LENGTH_LONG).show();
        }

    }

    private void getAllUsersService() {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting users...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<GetAllUsersModel> call = service.getUsers();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetAllUsersModel>() {

            @Override
            public void onResponse(Call<GetAllUsersModel> call, Response<GetAllUsersModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", json+"");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        usersArrayList = new ArrayList<>();
                        mallUserList = new ArrayList<>();
                        usersArrayList.clear();
                        mallUserList.clear();
                        if(response.body().getUserList()!=null) {
                            List<UserList> allusers = response.body().getUserList();

                            for (UserList userList : allusers) {

                                if (userList.getId() != sharedPrefsHelper.get(Consts.PREF_LOGIN_USERID, -1)) {
                                    mallUserList.add(userList);
                                    usersArrayList.add(new UsersObj(userList.getId() == 0 ? 0 : userList.getId(),
                                            userList.getUsername(), userList.getUserTypes().getId() == 1 ? "Admin" : "User",
                                            userList.getUserTypes().getId(), userList.getEmailId() == null ? "" : userList.getEmailId(),
                                            userList.getPhoneNum() == null ? "" : userList.getPhoneNum(),
                                            userList.getUserImage() == null ? "" : userList.getUserImage(), userList.getPassword(),userList.getFirstName()));
                                }

                            }
                            qbUsersDbManager.clearDB();
                            for (UsersObj usersObj : usersArrayList)
                                qbUsersDbManager.saveOPHWCUser(usersObj);

                            setAdapters();
                        }else{
                            setAdapters();
                            Toast.makeText(UsersActivity.this,"Unable to get users",Toast.LENGTH_SHORT).show();

                        }
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }
                getAllNatureOfWorks();

            }



            @Override
            public void onFailure(Call<GetAllUsersModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }
                getAllNatureOfWorks();

                Toast.makeText(UsersActivity.this,"Unable to get users",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllDivisionService(int i, UsersObj usersObj) {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting divisions...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<divisionsModel> call = service.getDivisions();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<divisionsModel>() {

            @Override
            public void onResponse(Call<divisionsModel> call, Response<divisionsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        divisionLists.clear();
                        divisionLists = response.body().getDivisionList();

                        if(divisionLists.size()>0) {
                            if(i==1) {
                                showAddUserDialog(i,getDivisionNames());
                            }else{
                                showEditUserDialog(i,getDivisionNames(),usersObj);
                            }

                        }

                        // setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<divisionsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to get divisions",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void callUserProjectsService() {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();
        String id = "1";
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(UsersActivity.this);

        int  projUserid = 0;
        projUserid = ophwcSharedPrefs.getUserId();
        projectsItem.setId(selUserId);

        Call<ProjectsModel> call = service.getAllUserProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if(response.body().getPrjectList()!=null) {
                            List<PrjectList> projectList = new ArrayList<>();
                            projectList.clear();
                            projectList = response.body().getPrjectList();
                            initMultiSpinner(projectList);
                        }else{

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    private void initMultiSpinner(List<PrjectList> projectList1) {
        ArrayList<String> projName = new ArrayList<>();
        for(PrjectList prjectList :projectList)
            projName.add(prjectList.getProjectName());

        ArrayAdapter<String> adapter = new ArrayAdapter <String>(UsersActivity.this, android.R.layout.simple_list_item_multiple_choice, projName);

        multiSelectSpinner.setListAdapter(adapter).setSelectAll(false).setAllCheckedText("Selected All").setTitle("Select Project(s)")
                .setMinSelectedItems(1).setListener( new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                projectIDLists.clear();
                for(int i = 0; i<selected.length;i++){
                    if(selected[i]){
                        projectIDLists.add(new ProjectIDList(projectList.get(i).getId()));
                        Log.e("Selected", projectList.get(i).getProjectName()+"size "+projectIDLists.size());
                    }
                }

            }
        });

        List<PrjectList> prjectLists2 = projectList1;
        //prjectLists2 = projectList1;

        for(int i=0; i<projectList.size();i++){
            for(int j=0;j<prjectLists2.size();j++){
                if(prjectLists2.get(j).getId()==projectList.get(i).getId()){
                    multiSelectSpinner.selectItem(i,true);
                }
            }
        }




        multiSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void initNatureMultiSpinner(int divCode) {
        List<String> natureName = new ArrayList<>();
        natureName = getNaturkNames();



        ArrayAdapter<String> adapter = new ArrayAdapter <String>(UsersActivity.this, android.R.layout.simple_list_item_multiple_choice, natureName);

        natureSpinner.setListAdapter(adapter).setSelectAll(false).setAllCheckedText("Selected All").setTitle("Select Nature(s)")
                .setMinSelectedItems(1).setListener( new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                natureIds.clear();
                for(int i = 0; i<selected.length;i++){
                    if(selected[i]){

                        natureIds.add(natureWorkLists.get(i).getId());

                    }

                }

                getAllProjectByNatureService(1,divCode,natureIds);
                return;

            }
        });

        List<PrjectList> prjectLists2 = selUserObj.getProjList();
        //prjectLists2 = projectList1;

        for(int i=0; i<prjectLists2.size();i++){
            for(int j=0;j<natureWorkLists.size();j++){
                if(natureWorkLists.get(j).getId()==prjectLists2.get(i).getNatureOfProject().getId()){
                    natureSpinner.selectItem(j,true);
                    natureIds.add(natureWorkLists.get(j).getId());
                }
            }
        }
        getAllProjectByNatureService(0,divCode,natureIds);




        multiSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void initMultiSpinner() {
        // callUserProjectsService();
        ArrayList<String> projName = new ArrayList<>();
        for(PrjectList prjectList :projectList)
            projName.add(prjectList.getProjectName());

        ArrayAdapter<String> adapter = new ArrayAdapter <String>(UsersActivity.this, android.R.layout.simple_list_item_multiple_choice, projName);

        multiSelectSpinner.setListAdapter(adapter).setSelectAll(false).setAllCheckedText("Selected All").setTitle("Select Project(s)")
                .setMinSelectedItems(1).setListener( new MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                projectIDLists.clear();
                for(int i = 0; i<selected.length;i++){
                    if(selected[i]){
                        projectIDLists.add(new ProjectIDList(projectList.get(i).getId()));
                        Log.e("Selected", projectList.get(i).getProjectName()+"size "+projectIDLists.size());
                    }
                }

            }
        });



        multiSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {



            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }



    private void callDeleteUserService(int userId) {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting users...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        DeleteUserItem deleteUserItem = new DeleteUserItem();

        deleteUserItem.setId(String.valueOf(userId));

        Call<DeleteUserModel> call = service.deleteObject(deleteUserItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<DeleteUserModel>() {

            @Override
            public void onResponse(Call<DeleteUserModel> call, Response<DeleteUserModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if(response.body().getStatusCode()==1){
                            getAllUsersService();
                            Toast.makeText(UsersActivity.this,"User delete successfully",Toast.LENGTH_SHORT).show();
                        }else{

                            Toast.makeText(UsersActivity.this,"Remove all assignments for the user",Toast.LENGTH_SHORT).show();

                            getAllUsersService();
                        }


                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<DeleteUserModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to delete user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private List<NatureList> getCreateUserItem() {
        List<NatureList>  natureWorkListsItem = new ArrayList<>();

        try {
            for (int i = 0; i < projectList.size(); i++) {
                if (selectedProjIds.contains(projectList.get(i).getId())) {
                    List<ProjList> projLists = new ArrayList<>();
                    projLists.add(new ProjList(projectList.get(i).getId()));
                    natureWorkListsItem.add(new NatureList(projectList.get(i).getNatureOfProject().getId(), projLists));

                }
            }
            return natureWorkListsItem;
        }catch (Exception e){
            return null;
        }
        //return null;
    }

    private void callUpdateUserService(int userId, int divCode, String strUserName, String strUserType, int userTypeCode, String stremail, String strUserMobile, String profile_pic, String s,String strFullName) {

        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(UsersActivity.this);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        UserUpdateItem userUpdateItem = new UserUpdateItem();
        userUpdateItem.setUsername(strUserName);
        userUpdateItem.setId(userId);
        Divisions divisions = new Divisions();
        divisions.setId(divCode);
        UserTypes userTypes = new UserTypes();
        userTypes.setId(userTypeCode);
        userUpdateItem.setDivisions(divisions);
        userUpdateItem.setUserTypes(userTypes);
        userUpdateItem.setEmailId(stremail);
        userUpdateItem.setFirstName(strFullName);
        userUpdateItem.setLastName(strFullName);
        userUpdateItem.setPhoneNum(strUserMobile);
        userUpdateItem.setPassword(s);
        userUpdateItem.setNatureList(getCreateUserItem());
        userUpdateItem.setStatus("Y");
        // userUpdateItem.set;

        Call<UserUpdateModel> call = service.updateUser(userUpdateItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UserUpdateModel>() {

            @Override
            public void onResponse(Call<UserUpdateModel> call, Response<UserUpdateModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        //qbUsersDbManager.saveOPHWCUser(usersArrayList.get(0));
                        getAllUsersService();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        Toast.makeText(UsersActivity.this,"User updated successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(UsersActivity.this,"Unable to update user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(UsersActivity.this,"Unable to update user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<UserUpdateModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to update user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_users_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.logout_user:
                // qbUsersDbManager.clearDB();
                sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, -1);
                finish();
                startActivity(new Intent(UsersActivity.this,SplashActivity.class));
                return true;
            default:
                return false;
        }
    }


    private void getAllProjectService(int isAdd ,int divId) {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        ProjectsItem projectsItem = new ProjectsItem();

        /*try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/
        OPHWCSharedPrefs ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(UsersActivity.this);
        projectsItem.setId(divId);

        Call<ProjectsModel> call = service.getAllProjects(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<ProjectsModel>() {

            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if(response.body().getPrjectList()!=null) {
                            projectList.clear();
                            projectList = response.body().getPrjectList();
                            if (isAdd == 1) {
                                showProjDialog();
                                // initMultiSpinner();
                            } else {
                                callUserProjectsService();
                            }
                        }else{

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }


    private void getAllNatureOfWorks() {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting Nature of projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<NatureOfWorkModel> call = service.getNatureOfWork();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<NatureOfWorkModel>() {

            @Override
            public void onResponse(Call<NatureOfWorkModel> call, Response<NatureOfWorkModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1){
                            natureWorkLists = response.body().getNatureWorkList();
                            if(natureWorkLists.size()>0){
                                getNaturkNames();
                            }

                        }else{
                            Toast.makeText(UsersActivity.this,response.body().getStatusMessage()+"",Toast.LENGTH_SHORT).show();


                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }
                getAllUserTypes();

            }



            @Override
            public void onFailure(Call<NatureOfWorkModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }
                getAllUserTypes();

                Toast.makeText(UsersActivity.this,"Unable to get Nature of work",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }


        });
    }
    private List<String> getUserTypeNames() {
        List<String> natureNames = new ArrayList<>();
        natureNames.clear();
        if(userTypesLists!=null) {
            for (int i=0; i<userTypesLists.size();i++) {
                if(userTypesLists.get(i).getTypeName()!=null) {
                    natureNames.add(userTypesLists.get(i).getTypeName());
                }
            }
        }

        return natureNames;
    }
    private void getAllUserTypes() {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting User types...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);

        Call<UsertypeModel> call = service.getUsertypes();
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<UsertypeModel>() {

            @Override
            public void onResponse(Call<UsertypeModel> call, Response<UsertypeModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:

                        long statusCode = response.body().getStatusCode();
                        if(statusCode==1){
                            if(response.body().getUserTypesList()!=null) {
                                userTypesLists = response.body().getUserTypesList();
                                if (userTypesLists.size() > 0) {
                                    /// getNaturkNames();

                                    // showAddUserDialog(1, getDivisionNames());
                                }else{
                                    Toast.makeText(UsersActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                                }
                            }else{
                                Toast.makeText(UsersActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                            }

                        }else{
                            Toast.makeText(UsersActivity.this,response.body().getStatusMessage()+"",Toast.LENGTH_SHORT).show();


                        }


                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<UsertypeModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to get User types",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }
    private void getAllProjectByNatureService(int isAdd , int divId, List<Long> NatureId) {
        ProgressDialog progressDialog = new ProgressDialog(UsersActivity.this);
        progressDialog.setMessage("Getting projects...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        AppApi service = retrofit.create(AppApi.class);
        GetProjectByDivNatureItem projectsItem = new GetProjectByDivNatureItem();

        /*try {
            id = getIntent().getStringExtra("divId");
        }catch (Exception e){};*/

        projectsItem.setId(divisionLists.get(divSpinner.getSelectedItemPosition()).getId());
        projectsItem.setUserId(selUserId);
        List<NatureOfProject> natureOfProjects = new ArrayList<>();
        for(int i=0;i<NatureId.size();i++){
            natureOfProjects.add(new NatureOfProject(NatureId.get(i)));
        }

        projectsItem.setNatureOfProject(natureOfProjects);


        Call<GetProjectsByNatureModel> call = service.assignGetProjectsByDivisionAndNature(projectsItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<GetProjectsByNatureModel>() {

            @Override
            public void onResponse(Call<GetProjectsByNatureModel> call, Response<GetProjectsByNatureModel> response) {
                int code = response.code();
                // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
                // String json = gson.toJson(response.body());
                Log.d("usercheck", code+"");
                Log.d("usercheck", "");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        if(response.body().getPrjectList()!=null) {
                            projectList = new ArrayList<>();
                            projectList.clear();
                            projectList = response.body().getPrjectList();
                            if (projectList != null && projectList.size() > 0) {
                                if (isAdd == 1) {
                                    //initMultiSpinner();

                                    if (progressDialog != null)
                                        progressDialog.dismiss();
                                    showProjDialog();
                                } else {
                                    // callUserProjectsService();
                                    StringBuffer sb = new StringBuffer();
                                    sb.append("Selected Projects\n");
                                    sb.append("\n");

                                    List<PrjectList> prjectLists = selUserObj.getProjList();
                                    selectedProjIds.clear();
                                    for (int i = 0; i < prjectLists.size(); i++) {
                                        selectedProjIds.add(prjectLists.get(i).getId());

                                        try {
                                            sb.append(prjectLists.get(i).getProjectName() + ", ");
                                            //  Toast.makeText(UsersActivity.this, "Selected Ids : " + selectedIds.get(i) + "\n" + "Selected Names : " + selectedNames.get(i) + "\n" + "DataString : " + dataString, Toast.LENGTH_SHORT).show();

                                        } catch (Exception e) {

                                        }

                                    }
                                    tvProjectSelect.setVisibility(View.VISIBLE);
                                    sb.append("\n");
                                    tvProjectSelect.setText(sb.toString());
                                    tvProjectSelect.setMaxLines(3);

                                    tvProjectSelect.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showProjDialog();
                                        }
                                    });

                                }
                            } else {
                                Toast.makeText(UsersActivity.this, "No Projects available for the selection", Toast.LENGTH_LONG).show();
                            }
                        }else{

                        }

                        //setAdapters();
                        // String status = response.body().getStatus();
                        if(progressDialog!=null)
                            progressDialog.dismiss();
                        //Toast.makeText(UsersActivity.this,"User created successfully",Toast.LENGTH_SHORT).show();


                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        // Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();
                        break;

                }

            }



            @Override
            public void onFailure(Call<GetProjectsByNatureModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(UsersActivity.this,"Unable to create user",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });
    }

}
