package com.ophwc.gemini.user;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ophwc.gemini.R;

import com.ophwc.gemini.model.UsersObj;
import com.quickblox.sample.groupchatwebrtc.utils.UiUtils;

import java.util.ArrayList;

/**
 * Created by htadimeti on 1/24/2018.
 */

class UsersCustomAdapter extends RecyclerView.Adapter<UsersCustomAdapter.MyViewHolder> {


    private ArrayList<UsersObj> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvUserName;
        TextView tvUserType;
        TextView tvUserMobile;
        ImageView imvUserIcon;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.tvUserName = (TextView) itemView.findViewById(R.id.rowUserName);
            this.tvUserType = (TextView) itemView.findViewById(R.id.rowUserType);
            this.imvUserIcon = (ImageView) itemView.findViewById(R.id.image_user_icon);
            //this.tvUserMobile = (TextView) itemView.findViewById(R.id.tvUserMobile);
        }
    }

    public UsersCustomAdapter(ArrayList<UsersObj> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_user_list_row, parent, false);



        MyViewHolder myViewHolder = new MyViewHolder(view);
        view.setOnClickListener(UsersActivity.myOnClickListener);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView tvName = holder.tvUserName;
        TextView tvMobile = holder.tvUserMobile;
        TextView tvType = holder.tvUserType;
        ImageView imvUser = holder.imvUserIcon;
        tvName.setText(dataSet.get(listPosition).getUserName());
       // tvMobile.setText(dataSet.get(listPosition).getMobileNo());
        Log.d("userinfo","Mobile "+dataSet.get(listPosition).getMobileNo());
        Log.d("userinfo","Name "+dataSet.get(listPosition).getUserName());
        imvUser.setBackgroundDrawable(UiUtils.getColorCircleDrawable(listPosition));
        imvUser.setImageResource(R.drawable.ic_person);
        tvType.setText(dataSet.get(listPosition).getUserType()+" , "+dataSet.get(listPosition).getMobileNo());
        imvUser.setTag(listPosition);

       // holder.setOnClickListener(UsersActivity.myOnClickListener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
