package com.quickblox.sample.groupchatwebrtc.activities;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ophwc.gemini.OPHWCSharedPrefs;
import com.ophwc.gemini.api.AppApi;
import com.ophwc.gemini.divisions.DivisionsActivity;
import com.ophwc.gemini.divsuperuser.DivisionSuperUserActivity;
import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.login.LoginItem;
import com.ophwc.gemini.model.login.LoginModel;
import com.ophwc.gemini.model.login.UserList;
import com.ophwc.gemini.offline.OfflineProjectsActivity;
import com.ophwc.gemini.superuser.SuperuserDivisions;
import com.ophwc.gemini.user.CreateUserActivity;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.core.helper.Utils;

import com.ophwc.gemini.R;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.sample.groupchatwebrtc.db.QbUsersDbManager;
import com.quickblox.sample.groupchatwebrtc.services.CallService;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.KeyboardUtils;
import com.quickblox.sample.groupchatwebrtc.utils.QBEntityCallbackImpl;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;
import com.quickblox.sample.groupchatwebrtc.utils.ValidationUtils;

import com.quickblox.users.model.QBUser;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText userNameEditText;
    private EditText chatRoomNameEditText;
    private EditText edtPassword;
    private Button btnlogintoroom;
    private RadioGroup radioUsergroup;
    private RadioButton rbAdmin,rbUser;
    int loginAs = -1;
    private OPHWCSharedPrefs ophwcSharedPrefs;

    private QBUser userForSave;
    private String divUsers[] = {"User1","User2","User3","User4","User5","User6","User7","User8","User1234","User2345"};
    private String adminUsers[] = {"Admin1","Admin2","Admin3","Admin4","Admin5"};

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_login_activity_layout);
           ophwcSharedPrefs =OPHWCSharedPrefs.getInstance(LoginActivity.this);
        initUI();
    }

    @Override
    protected View getSnackbarAnchorView() {
        return findViewById(R.id.root_view_login_activity);
    }
    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (Character.isWhitespace(source.charAt(i))) {
                    Toast.makeText(LoginActivity.this,"Space is not allowed here",Toast.LENGTH_SHORT).show();
                    return "";
                }
            }
            return null;
        }

    };
    private void initUI() {
        setActionBarTitle(R.string.title_login_activity);
        userNameEditText = findViewById(R.id.user_name);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        chatRoomNameEditText = findViewById(R.id.chat_room_name);
        btnlogintoroom = findViewById(R.id.btnlogintoroom);
        radioUsergroup = findViewById(R.id.radioUsergroup);
        rbAdmin = findViewById(R.id.rbAdmin);
        rbUser = findViewById(R.id.rbUser);
        btnlogintoroom.setOnClickListener(this);
        //userNameEditText.setFilters(new InputFilter[] { filter });
       // edtPassword.setFilters(new InputFilter[] { filter });
        AppCompatCheckBox cbShowPassword = findViewById(R.id.cbShowPassword);

        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

                }else{
                    edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        radioUsergroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_login_user_done:
                if (isEnteredUserNameValid()&&isUserExist(userNameEditText.getText().toString(), edtPassword.getText().toString())&&isPasswordValid()) {
                    hideKeyboard();
                    startSignUpNewUser(createUserWithEnteredData());
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isUserExist(String userName, String password) {
        QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(LoginActivity.this);
        try {
            UsersObj usersObj = qbUsersDbManager.getLoginUsersDetails();


            if (usersObj.getUserName().equals(userName) && usersObj.get_password().equals(password)) {
                return true;
            } else {
                Toast.makeText(LoginActivity.this, "Invalid user name or password", Toast.LENGTH_SHORT).show();
                return false;
            }
        }catch (Exception e ){
            return false;
        }

        //return false;
    } private boolean isPasswordValid() {
        String password = edtPassword.getText().toString();
        if(password.equals("1234")){
            return true;
     }else{
            Toast.makeText(LoginActivity.this,"Invalid password",Toast.LENGTH_SHORT).show();
            return false;
        }


        //return false;
    }

    private boolean isEnteredRoomNameValid() {
        return ValidationUtils.isRoomNameValid(this, chatRoomNameEditText);
    }

    private boolean isEnteredUserNameValid() {
        return ValidationUtils.isUserNameValid(this, userNameEditText);
    }
    private boolean isEnteredPasswordValid() {
        return ValidationUtils.isPasswordValid(this, edtPassword);
    }

    private void hideKeyboard() {
        KeyboardUtils.hideKeyboard(userNameEditText);
        KeyboardUtils.hideKeyboard(chatRoomNameEditText);
    }

    private void startSignUpNewUser(final QBUser newUser) {
        showProgressDialog(R.string.dlg_sign_in);
        requestExecutor.signUpNewUser(newUser, new QBEntityCallback<QBUser>() {
                    @Override
                    public void onSuccess(QBUser result, Bundle params) {
                        loginToChat(result);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Log.e("satuscode",""+e.getErrors().get(0)+"  -- "+e.getHttpStatusCode());
                        if (e.getHttpStatusCode() == Consts.ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS) {
                            signInCreatedUser(newUser, true);
                        } else {
                            hideProgressDialog();
                            registerUser();
                            Toast.makeText(mContext,R.string.sign_up_error,Toast.LENGTH_LONG);
                        }
                    }
                }
        );
    }

    private void loginToChat(final QBUser qbUser) {
        qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);

        userForSave = qbUser;
        startLoginService(qbUser);
        sharedPrefsHelper.saveQbUser(qbUser);
        sharedPrefsHelper.save(Consts.PREF_QBUSER_LOGIN,userNameEditText.getText().toString());
    }

    private void startOpponentsActivity() {
        /*Intent myIntent = new Intent(LoginActivity.this, CheckCallService.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),  0, myIntent, 0);

        AlarmManager alarmManager = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 5); // first time
        long frequency= 5 * 1000; // in ms
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), frequency, pendingIntent);
       */ //sharedPrefsHelper.saveQbUser(userForSave);
        QBSettings.getInstance().setEnablePushNotification(true);
        SubscribeService.subscribeToPushes(LoginActivity.this,true);
        if(((SharedPrefsHelper.getInstance()).get(Consts.PREF_LOGIN_AS,-1)==3)||loginAs==3){
            SharedPrefsHelper.getInstance().save(Consts.PREF_LOGIN_AS,3);
            startActivity(new Intent(LoginActivity.this, SuperuserDivisions.class));
        }else if(((SharedPrefsHelper.getInstance()).get(Consts.PREF_LOGIN_AS,-1)==4)||loginAs==4){
            SharedPrefsHelper.getInstance().save(Consts.PREF_LOGIN_AS,4);
            ophwcSharedPrefs.putLoginOffline(4);
            startActivity(new Intent(LoginActivity.this, DivisionSuperUserActivity.class));
        }else {
            startActivity(new Intent(LoginActivity.this, DivisionsActivity.class));
            ophwcSharedPrefs.putLoginOffline(0);
            SharedPrefsHelper.getInstance().save(Consts.PREF_LOGIN_AS,0);
        }
        //OpponentsActivity.start(LoginActivity.this, false);
        finish();
    }
    private void startUsersActivity() {
        (SharedPrefsHelper.getInstance()).save(Consts.PREF_LOGIN_AS,1);
        startActivity(new Intent(LoginActivity.this, UsersActivity.class));
        finish();
    }

    private void saveUserData(QBUser qbUser) {
        SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
        sharedPrefsHelper.save(Consts.PREF_CURREN_ROOM_NAME, Consts.CHAT_ROOM_NAME);
        sharedPrefsHelper.saveQbUser(qbUser);
    }

    private QBUser createUserWithEnteredData() {
        return createQBUserWithCurrentData(String.valueOf(userNameEditText.getText()),
                Consts.CHAT_ROOM_NAME);
    }

    private QBUser createQBUserWithCurrentData(String userName, String chatRoomName) {
        QBUser qbUser = null;
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(chatRoomName)) {
            StringifyArrayList<String> userTags = new StringifyArrayList<>();
            userTags.add(chatRoomName);

            qbUser = new QBUser();
            qbUser.setFullName(userName);
            qbUser.setLogin(getCurrentDeviceId());
            qbUser.setPassword(Consts.DEFAULT_USER_PASSWORD);
            qbUser.setTags(userTags);
        }

        return qbUser;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Consts.EXTRA_LOGIN_RESULT_CODE) {
            hideProgressDialog();
            boolean isLoginSuccess = data.getBooleanExtra(Consts.EXTRA_LOGIN_RESULT, false);
            String errorMessage = data.getStringExtra(Consts.EXTRA_LOGIN_ERROR_MESSAGE);

            if (isLoginSuccess) {
                saveUserData(userForSave);

                signInCreatedUser(userForSave, false);
            } else {
                Toast.makeText(mContext,getString(R.string.login_chat_login_error) + errorMessage,Toast.LENGTH_LONG).show();
                userNameEditText.setText(userForSave.getFullName());
                chatRoomNameEditText.setText(userForSave.getTags().get(0));
            }
        }
    }

    private void signInCreatedUser(final QBUser user, final boolean deleteCurrentUser) {
        requestExecutor.signInUser(user, new QBEntityCallbackImpl<QBUser>() {
            @Override
            public void onSuccess(QBUser result, Bundle params) {
                if (deleteCurrentUser) {
                    removeAllUserData(result);
                } else {

                        startOpponentsActivity();
                }
            }

            @Override
            public void onError(QBResponseException responseException) {
                hideProgressDialog();
                Toast.makeText(mContext,R.string.sign_up_error,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void removeAllUserData(final QBUser user) {
        requestExecutor.deleteCurrentUser(user.getId(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                UsersUtils.removeUserData(getApplicationContext());
                startSignUpNewUser(createUserWithEnteredData());
            }

            @Override
            public void onError(QBResponseException e) {
                hideProgressDialog();
                Toast.makeText(mContext,R.string.sign_up_error,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startLoginService(QBUser qbUser) {
        Intent tempIntent = new Intent(this, CallService.class);
        PendingIntent pendingIntent = createPendingResult(Consts.EXTRA_LOGIN_RESULT_CODE, tempIntent, 0);
        CallService.start(this, qbUser, pendingIntent);
    }

    private String getCurrentDeviceId() {
        return Utils.generateDeviceId(this);
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public void onClick(View v) {

        if(isNetworkAvailable()) {

            loginUser(userNameEditText.getText().toString(), edtPassword.getText().toString());
        }else{

            if(isUserExist(userNameEditText.getText().toString(), edtPassword.getText().toString())) {

                if(ophwcSharedPrefs.getLoginOffline()==0){
                    Toast.makeText(LoginActivity.this,"No internet, Loading offline projects",Toast.LENGTH_SHORT).show();

                    loginAs=0;
                sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 0);
                ophwcSharedPrefs.putLoginOffline(0);
                finish();
                startActivity(new Intent(LoginActivity.this, OfflineProjectsActivity.class));
                }
                else if(ophwcSharedPrefs.getLoginOffline()==4){
                    Toast.makeText(LoginActivity.this,"No internet, Loading offline projects",Toast.LENGTH_SHORT).show();

                    loginAs=4;
                    sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 4);
                    ophwcSharedPrefs.putLoginOffline(4);
                    finish();
                    startActivity(new Intent(LoginActivity.this, OfflineProjectsActivity.class));
                }else{
                    Toast.makeText(LoginActivity.this,"Please check internet connection",Toast.LENGTH_SHORT).show();

                }

            }else{
                Toast.makeText(LoginActivity.this,"Please check internet connection",Toast.LENGTH_SHORT).show();

            }
        }

        /*if (isEnteredUserNameValid() && isEnteredPasswordValid()&&isUserExist()&&isPasswordValid()) {
            hideKeyboard();
            //loginUser(userNameEditText.getText().toString(),edtPassword.getText().toString());
            *//*if (rbAdmin.isChecked())
                startUsersActivity();   // return;
            else
            startSignUpNewUser(createUserWithEnteredData());*//*
        }*/
    }
    private void loginUser(String username, String password){
        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Signing in...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        AppApi service = retrofit.create(AppApi.class);
        LoginItem loginItem = new LoginItem();
        loginItem.setUsername(username);
        loginItem.setPassword(password);
        Call<LoginModel> call = service.login(loginItem);
        //mLoginFormView.setVisibility(View.INVISIBLE);
        //showProgress(true);


        call.enqueue(new Callback<LoginModel>() {

            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                int code = response.code();
              // String rMsg = response.body().getStatus();
                Gson gson = new Gson();
               String json = gson.toJson(response.body());
                Log.e("usercheck", json+"");
                switch (code) {
                    // 200 is success and user is registered
                    case 200:
                        try{
                            long status = response.body().getStatusCode();
                            if(progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            if(status==1){
                                UserList loginUserList = response.body().getUserList();
                                //int userId  = response.body().getUserList();
                               // sharedPrefsHelper.save(Consts.PREF_LOGIN_USER,response.body().getUsername());
                                sharedPrefsHelper.save(Consts.PREF_LOGIN_USER,loginUserList.getUsername());
                                ophwcSharedPrefs = OPHWCSharedPrefs.getInstance(LoginActivity.this);
                                ophwcSharedPrefs.putUserId(loginUserList.getId());
                                ophwcSharedPrefs.putUserEmail(loginUserList.getEmailId());
                                ophwcSharedPrefs.putUserName(loginUserList.getUsername());
                                ophwcSharedPrefs.putUserFullName(loginUserList.getFirstName());
                                ophwcSharedPrefs.putMobileNo(loginUserList.getPhoneNum());


                                sharedPrefsHelper.save(Consts.PREF_LOGIN_USERID,loginUserList.getId());
/* User types 1= Admin,
 2= division user,
 3 = Super user (HO),
 4 = Div super user*/
                             int userTypecode = loginUserList.getUserTypes().getId();

                                QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(LoginActivity.this);
                                qbUsersDbManager.clearLoginUserDB();

                                if(userTypecode==1){
                                    loginAs=1;
                                    //sharedPrefsHelper.clearAllData();
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_USER,loginUserList.getFirstName());
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_USERID,loginUserList.getId());
                                   clearDB();
                                   try {
                                       UsersUtils.removeUserData(LoginActivity.this);
                                       CallService.logout(LoginActivity.this);
                                       SubscribeService.unSubscribeFromPushes(LoginActivity.this);
                                   }catch (Exception e){
                                       e.printStackTrace();
                                   }
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 1);
                                    startUsersActivity();
                                }else if(userTypecode==3){
                                    loginAs=3;
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 3);
                                    registerUser();
                                    //startActivity(new Intent(LoginActivity.this,SuperuserDivisions.class));
                                }else if(userTypecode==4){
                                    loginAs=4;
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 4);
                                    ophwcSharedPrefs.putLoginOffline(4);
                                    QbUsersDbManager.getInstance(LoginActivity.this).clearOfflineUserDB();

                                    qbUsersDbManager.saveLoginUser(loginUserList.getId(),username,password,loginUserList.getFirstName(),loginUserList.getEmailId(),
                                            loginUserList.getPhoneNum(),loginUserList.getUserTypes().getTypeName(),loginUserList.getUserTypes().getId());

                                    //Toast.makeText(LoginActivity.this,"Under construction",Toast.LENGTH_SHORT).show();
                                   registerUser();
                                    //startActivity(new Intent(LoginActivity.this,SuperuserDivisions.class));
                                }
                                else{

                                        QbUsersDbManager.getInstance(LoginActivity.this).clearOfflineUserDB();

                                      qbUsersDbManager.saveLoginUser(loginUserList.getId(),username,password,loginUserList.getFirstName(),loginUserList.getEmailId(),
                                            loginUserList.getPhoneNum(),loginUserList.getUserTypes().getTypeName(),loginUserList.getUserTypes().getId());

                                    loginAs=0;
                                    sharedPrefsHelper.save(Consts.PREF_LOGIN_AS, 0);
                                    ophwcSharedPrefs.putLoginOffline(0);
                                    registerUser();

                                }
                            }else{
                                Toast.makeText(LoginActivity.this,"Invalid User name or Password",Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            if(progressDialog!=null){
                                progressDialog.dismiss();
                            }
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this,"Unable to login at the moment",Toast.LENGTH_SHORT).show();

                        }

                        break;
                    case 2:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(LoginActivity.this,"Unable to login at the moment",Toast.LENGTH_SHORT).show();

                        break;
                    // code other than 200
                    default:
                        if(progressDialog!=null){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(LoginActivity.this,"Unable to login at the moment",Toast.LENGTH_SHORT).show();
break;

                }

            }



            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                String tMsg = t.toString();
                Log.d("check", tMsg);
                if(progressDialog!=null){
                    progressDialog.dismiss();
                }

                Toast.makeText(LoginActivity.this,"Unable to login at the moment",Toast.LENGTH_SHORT).show();

                // mLoginFormView.setVisibility(View.VISIBLE);
                //showProgress(false);

            }

        });

    }

    private void registerUser() {
        if(sharedPrefsHelper.hasQbUser()){

            if(sharedPrefsHelper.getQbUser().getFullName().equalsIgnoreCase(userNameEditText.getText().toString())) {
                /*clearDB();
                SubscribeService.unSubscribeFromPushes(LoginActivity.this);
                CallService.logout(LoginActivity.this);
                removeAllUserData(sharedPrefsHelper.getQbUser());*/
                startOpponentsActivity();
                //CallService.start(LoginActivity.this, sharedPrefsHelper.getQbUser());
                // startSignUpNewUser(createUserWithEnteredData());
            }
            else {
                clearDB();
                startSignUpNewUser(createUserWithEnteredData());
            }
        }else{
            clearDB();
            startSignUpNewUser(createUserWithEnteredData());
        }
    }

    private void clearDB() {
        QbUsersDbManager qbUsersDbManager = QbUsersDbManager.getInstance(LoginActivity.this);
        try {
            qbUsersDbManager.clearDB();
            qbUsersDbManager.clearDivUserDB();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
