package com.quickblox.sample.groupchatwebrtc.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;

import com.ophwc.gemini.App;
import com.ophwc.gemini.ExceptionHandler;
import com.ophwc.gemini.R;
import com.ophwc.gemini.VersionChecker;
import com.ophwc.gemini.divisions.DivisionsActivity;
import com.ophwc.gemini.divsuperuser.DivisionSuperUserActivity;
import com.ophwc.gemini.offline.OfflineProjectsActivity;
import com.ophwc.gemini.superuser.SuperuserDivisions;
import com.ophwc.gemini.user.UsersActivity;
import com.quickblox.sample.groupchatwebrtc.services.CallService;
import com.quickblox.sample.groupchatwebrtc.utils.Consts;
import com.quickblox.sample.groupchatwebrtc.utils.ErrorUtils;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.sample.groupchatwebrtc.utils.UsersUtils;
import com.quickblox.users.model.QBUser;

import java.util.concurrent.ExecutionException;

public class SplashActivity extends BaseActivity {
    private static final int SPLASH_DELAY = 1500;

    private static Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    private SharedPrefsHelper sharedPrefsHelper;
    private AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));


       // setContentView(R.layout.activity_splash);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        sharedPrefsHelper = SharedPrefsHelper.getInstance();



        if (checkConfigsWithSnackebarError()) {
            proceedToTheNextActivityWithDelay();
        }
    }

    @Override
    protected View getSnackbarAnchorView() {
        return null;
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    protected void proceedToTheNextActivity() {

        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String versionCode = info.versionName;
        UsersUtils.VERSION_NUMBER = versionCode;
        String versionName = "";
        VersionChecker versionChecker = new VersionChecker();
        try {
            versionName = versionChecker.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //TODO change this to not
        if (!versionCode.equals(versionName)&&isNetworkAvailable()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            StringBuffer sb = new StringBuffer();
            sb.append("A new version is available to update. Please update now to continue.");
            sb.append("\n");
            sb.append("\n");
            sb.append("Do you want to update now?");
            sb.append("\n");


            alertDialogBuilder.setMessage(sb.toString());
            alertDialogBuilder.setTitle("OPHWC Update");
            alertDialogBuilder.setIcon(getResources().getDrawable(R.drawable.ic_error_outline_black_24dp));
            alertDialogBuilder.setPositiveButton("Update",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (alertDialog.isShowing())
                                alertDialog.dismiss();
                            finish();
                            final String appPackageName = "com.ophwc.gemini"; // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }

                    });
            alertDialogBuilder.setNegativeButton("Not now",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (alertDialog.isShowing())
                                alertDialog.dismiss();
                           finish();
                        System.exit(0);
                        }

                    });
            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else {
            if (sharedPrefsHelper.get(Consts.PREF_LOGIN_AS, -1) == 0) {
                if (isNetworkAvailable()) {
                    if (sharedPrefsHelper.hasQbUser()) {
                        startLoginService(sharedPrefsHelper.getQbUser());
                        //startActivity(new Intent(SplashActivity.this, UsersActivity.class));
                        startOpponentsActivity();
                        finish();
                        return;
                    }
                } else {
                    finish();
                    startActivity(new Intent(SplashActivity.this, OfflineProjectsActivity.class));
                    return;

                }
            }
            if (sharedPrefsHelper.get(Consts.PREF_LOGIN_AS, -1) == 1) {
                startActivity(new Intent(SplashActivity.this, UsersActivity.class));
                finish();
                return;
            }
            if (sharedPrefsHelper.get(Consts.PREF_LOGIN_AS, -1) == 3) {
                startLoginService(sharedPrefsHelper.getQbUser());
                startActivity(new Intent(SplashActivity.this, SuperuserDivisions.class));
                finish();
                return;
            }
            if (sharedPrefsHelper.get(Consts.PREF_LOGIN_AS, -1) == 4) {
                if (isNetworkAvailable()) {
                    startLoginService(sharedPrefsHelper.getQbUser());
                    startActivity(new Intent(SplashActivity.this, DivisionSuperUserActivity.class));
                    finish();
                    return;
                } else {

                    finish();
                    startActivity(new Intent(SplashActivity.this, OfflineProjectsActivity.class));
                    return;


                }
            }
       /* if (sharedPrefsHelper.hasQbUser()) {
            startLoginService(sharedPrefsHelper.getQbUser());

        }*/

            LoginActivity.start(this);
            finish();
        }
    }

    protected void startLoginService(QBUser qbUser) {
        CallService.start(this, qbUser);
    }

    private void startOpponentsActivity() {
        startActivity(new Intent(SplashActivity.this,DivisionsActivity.class));
        //OpponentsActivity.start(SplashActivity.this, false);
        finish();
    }

    protected boolean sampleConfigIsCorrect(){
        return App.getInstance().getQbConfigs() != null;
    }

    protected void proceedToTheNextActivityWithDelay() {
        mainThreadHandler.postDelayed(this::proceedToTheNextActivity, SPLASH_DELAY);
    }

    protected boolean checkConfigsWithSnackebarError(){
        if (!sampleConfigIsCorrect()){
            showSnackbarErrorParsingConfigs();
            return false;
        }
        return true;
    }

    protected void showSnackbarErrorParsingConfigs(){
        ErrorUtils.showSnackbar(findViewById(R.id.layout_root), R.string.error_parsing_configs, R.string.dlg_ok, null);
    }
}