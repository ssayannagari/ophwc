package com.quickblox.sample.groupchatwebrtc.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.quickblox.sample.groupchatwebrtc.utils.Consts;

/**
 * Created by tereha on 17.05.16.
 */
public class DbHelper extends SQLiteOpenHelper {

    private String TAG = DbHelper.class.getSimpleName();

    private static final String DB_NAME = "groupchatwebrtcDB";

    public static final String DB_TABLE_NAME = "users";
    public static final String DB_COLUMN_ID = "ID";
    public static final String DB_COLUMN_USER_FULL_NAME = "userFullName";
    public static final String DB_COLUMN_USER_LOGIN = "userLogin";
    public static final String DB_COLUMN_USER_ID = "userID";
    public static final String DB_COLUMN_USER_PASSWORD = "userPass";
    public static final String DB_COLUMN_USER_TAG = "userTag";

    // ------------------ User table column keys-----------------

    public static final String TABLE_USERS = "usersList";
    public static final String KEY_ID = "_id";
    public static final String KEY_USERID = "userId";
    public static final String KEY_USERNAME = "userName";
    public static final String KEY_USERTYPE = "userType";
    public static final String KEY_USERTYPE_ID = "userTypeId";
    public static final String KEY_EMAIL_ID = "userEmailId";
    public static final String KEY_USER_MOBILENO = "userMobileNo";
    public static final String KEY_PROFILE_PIC = "profilePic";
    public static final String KEY_PASSWORD = "password";
//---------------------------- Login User details ----------------
public static final String TABLE_LOGIN_USER = "loginUser";
    public static final String KEY_LASTNAME = "lastName";
    public static final String KEY_FIRSTNAME = "firstName";

    // ---------------- division list ---------------
    public static final String TABLE_DIVISION = "divisions";
    public static final String KEY_DIV_NAME = "divisionName";
    public static final String KEY_DIV_ID = "divisionId";

    // ---------------- project list ---------------
    public static final String TABLE_PROJECTS = "projects";
    public static final String KEY_PROJ_NAME = "projectName";
    public static final String KEY_PROJ_DEF = "projectDef";
    public static final String KEY_PROJ_LOC = "projectLoc";
    public static final String KEY_PROJ_ID = "projectId";

    //------ Offline Image capture--------------
    public static final String TABLE_OFF_IMAGES = "offImages";
    // column names

    public static final String KEY_NAME = "image_name";
    public static final String KEY_IMAGE = "image_data";


    public DbHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "--- onCreate database ---");
        db.execSQL("create table " + DB_TABLE_NAME + " ("
                + DB_COLUMN_ID + " integer primary key autoincrement,"
                + DB_COLUMN_USER_ID + " integer,"
                + DB_COLUMN_USER_LOGIN + " text,"
                + DB_COLUMN_USER_PASSWORD + " text,"
                + DB_COLUMN_USER_FULL_NAME + " text,"
                + DB_COLUMN_USER_TAG + " text"
                + ");");
        // create user table
        db.execSQL("create table " + TABLE_USERS + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_USERID + " integer,"
                + KEY_USERNAME + " text,"
                + KEY_USERTYPE+ " text,"
                + KEY_USERTYPE_ID + " integer,"
                + KEY_USER_MOBILENO + " text,"
                + KEY_EMAIL_ID + " text,"
                + KEY_PROFILE_PIC + " text,"
                + KEY_PASSWORD + " text,"
                + KEY_FIRSTNAME + " text"
                + ");");
        // create login user table
        db.execSQL("create table " + TABLE_LOGIN_USER + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_USERID + " integer,"
                + KEY_USERNAME + " text,"
                + KEY_FIRSTNAME + " text,"
                + KEY_LASTNAME + " text,"
                + KEY_USERTYPE+ " text,"
                + KEY_USERTYPE_ID + " integer,"
                + KEY_USER_MOBILENO + " text,"
                + KEY_EMAIL_ID + " text,"
                + KEY_PASSWORD + " text,"
                + KEY_PROFILE_PIC + " text"
                + ");");
        // create division table
        db.execSQL("create table " + TABLE_DIVISION + " ("
                + KEY_ID + " integer primary key autoincrement,"
                + KEY_DIV_ID + " integer,"
                + KEY_DIV_NAME + " text"
                + ");");
        // create proj table
        db.execSQL("CREATE TABLE " + TABLE_PROJECTS + " ("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_PROJ_ID + " INTEGER,"
                + KEY_PROJ_LOC + " TEXT,"
                + KEY_PROJ_DEF + " TEXT,"
                + KEY_PROJ_NAME + " TEXT"
                + ");");

         String CREATE_TABLE_IMAGE = "CREATE TABLE " + TABLE_OFF_IMAGES + "("+
                 KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_PROJ_ID + " INTEGER," +
                KEY_DIV_ID + " INTEGER," +
                KEY_NAME + " TEXT," +
                KEY_IMAGE + " BLOB);";
         db.execSQL(CREATE_TABLE_IMAGE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
