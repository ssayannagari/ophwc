package com.quickblox.sample.groupchatwebrtc.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.ophwc.gemini.model.UsersObj;
import com.ophwc.gemini.model.project.PrjectList;
import com.ophwc.gemini.offline.offlineProjObj;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;
import com.quickblox.users.model.QBUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tereha on 17.05.16.
 */
public class QbUsersDbManager {
    private static String TAG = QbUsersDbManager.class.getSimpleName();

    private static QbUsersDbManager instance;
    private Context mContext;

    private QbUsersDbManager(Context context) {
        this.mContext = context;
    }

    public static QbUsersDbManager getInstance(Context context) {
        if (instance == null) {
            instance = new QbUsersDbManager(context);
        }

        return instance;
    }

    public ArrayList<QBUser> getAllUsers() {
        ArrayList<QBUser> allUsers = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_TAG);

            do {
                QBUser qbUser = new QBUser();

                qbUser.setFullName(c.getString(userFullNameColIndex));
                qbUser.setLogin(c.getString(userLoginColIndex));
                qbUser.setId(c.getInt(userIdColIndex));
                qbUser.setPassword(c.getString(userPassColIndex));

                StringifyArrayList<String> tags = new StringifyArrayList<>();
                tags.add(c.getString(userTagColIndex));
                qbUser.setTags(tags);

                SharedPrefsHelper spf = SharedPrefsHelper.getInstance();
                String fullName = spf.get("qb_user_full_name");
                if (!fullName.equals(c.getString(userFullNameColIndex))){
                    allUsers.add(qbUser);
            }
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers;
    }

    public QBUser getUserById(Integer userId) {
        QBUser qbUser = null;
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.DB_TABLE_NAME, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            int userIdColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_ID);
            int userLoginColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_LOGIN);
            int userPassColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_PASSWORD);
            int userFullNameColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_FULL_NAME);
            int userTagColIndex = c.getColumnIndex(DbHelper.DB_COLUMN_USER_TAG);

            do {
                if (c.getInt(userIdColIndex) == userId) {
                    qbUser = new QBUser();
                    qbUser.setFullName(c.getString(userFullNameColIndex));
                    qbUser.setLogin(c.getString(userLoginColIndex));
                    qbUser.setId(c.getInt(userIdColIndex));
                    qbUser.setPassword(c.getString(userPassColIndex));

                    StringifyArrayList<String> tags = new StringifyArrayList<>();
                    tags.add(c.getString(userTagColIndex).split(","));
                    qbUser.setTags(tags);
                    return qbUser;
                }
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return qbUser;
    }

    public void saveAllUsers(ArrayList<QBUser> allUsers, boolean needRemoveOldData) {
        if (needRemoveOldData) {
            clearDB();
        }

        for (QBUser qbUser : allUsers) {
            Log.e("QBUSER",qbUser.getFullName());
            Log.e("QBUSER",qbUser.getFileId()+"");
            Log.e("QBUSER",qbUser.getExternalId()+"");
            Log.e("QBUSER",qbUser.getFileId()+"");

            saveUser(qbUser);
        }
        Log.d(TAG, "saveAllUsers");
    }

    public void saveUser(QBUser qbUser) {
        ContentValues cv = new ContentValues();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelper.DB_COLUMN_USER_FULL_NAME, qbUser.getFullName());
        cv.put(DbHelper.DB_COLUMN_USER_LOGIN, qbUser.getLogin());
        cv.put(DbHelper.DB_COLUMN_USER_ID, qbUser.getId());
        cv.put(DbHelper.DB_COLUMN_USER_PASSWORD, qbUser.getPassword());
        cv.put(DbHelper.DB_COLUMN_USER_TAG, qbUser.getTags().getItemsAsString());

        db.insert(DbHelper.DB_TABLE_NAME, null, cv);
        dbHelper.close();
    }

    // ------------------  OPHWC USERS --------------------------
    public void saveOPHWCUser(UsersObj user) {
        ContentValues cv = new ContentValues();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelper.KEY_USERNAME, user.getUserName());
        cv.put(DbHelper.KEY_EMAIL_ID, user.getEmailId());
        cv.put(DbHelper.KEY_PROFILE_PIC, user.getProfilepic());
        cv.put(DbHelper.KEY_USERTYPE, user.getUserType());
        cv.put(DbHelper.KEY_USERTYPE_ID, user.getUserTypeCode());
        cv.put(DbHelper.KEY_USER_MOBILENO, user.getMobileNo());
        cv.put(DbHelper.KEY_USERID, user.getUserId());
        cv.put(DbHelper.KEY_FIRSTNAME, user.getFullName());
        cv.put(DbHelper.KEY_PASSWORD, user.get_password());


        db.insert(DbHelper.TABLE_USERS, null, cv);
        dbHelper.close();
    }

    public void saveAlluserProjects(List<PrjectList> allProjects){
        ContentValues cv = new ContentValues();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        for(PrjectList prjectList : allProjects) {
            cv.put(DbHelper.KEY_PROJ_NAME, prjectList.getProjectName());
            cv.put(DbHelper.KEY_PROJ_ID, prjectList.getId());
            cv.put(DbHelper.KEY_PROJ_DEF, prjectList.getProjectDefination());
            cv.put(DbHelper.KEY_PROJ_LOC,"");

            db.insert(DbHelper.TABLE_PROJECTS, null, cv);
        }
        dbHelper.close();
    }

    public void saveLoginUser(int userId,String userName, String password, String fullName, String emailId, String mobileNo,String userType, int typeId){

        ContentValues cv = new ContentValues();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DbHelper.KEY_USERNAME, userName);
        cv.put(DbHelper.KEY_FIRSTNAME, fullName);
        cv.put(DbHelper.KEY_LASTNAME, fullName);
        cv.put(DbHelper.KEY_EMAIL_ID, emailId);
        cv.put(DbHelper.KEY_PROFILE_PIC, "");
        cv.put(DbHelper.KEY_USERTYPE, userType);
        cv.put(DbHelper.KEY_USERTYPE_ID, typeId);
        cv.put(DbHelper.KEY_USER_MOBILENO,mobileNo);
        cv.put(DbHelper.KEY_PASSWORD,password);
        cv.put(DbHelper.KEY_USERID, userId);


        db.insert(DbHelper.TABLE_LOGIN_USER, null, cv);
        dbHelper.close();
    }

    // -------------- Get login user details ------
    public UsersObj getLoginUsersDetails() {
        ArrayList<UsersObj> allUsers = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.TABLE_LOGIN_USER, null, null, null, null, null, null);

        if (c.moveToFirst()) {

            do {
                allUsers.add(new UsersObj(c.getInt(c.getColumnIndex(DbHelper.KEY_USERID)),c.getString(c.getColumnIndex(DbHelper.KEY_USERNAME)),
                        c.getString(c.getColumnIndex(DbHelper.KEY_USERTYPE)),c.getInt(c.getColumnIndex(DbHelper.KEY_USERTYPE_ID)),
                        c.getString(c.getColumnIndex(DbHelper.KEY_EMAIL_ID)),c.getString(c.getColumnIndex(DbHelper.KEY_USER_MOBILENO)),
                        "", c.getString(c.getColumnIndex(DbHelper.KEY_PASSWORD)),c.getString(c.getColumnIndex(DbHelper.KEY_FIRSTNAME))));
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers.size()>0?allUsers.get(0):null;

    }

    // ------------ Get All ophwc users --------------
    public ArrayList<UsersObj> getAllOphwcUsers() {
        ArrayList<UsersObj> allUsers = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DbHelper.TABLE_USERS, null, null, null, null, null, null);

        if (c.moveToFirst()) {

            do {
                allUsers.add(new UsersObj(c.getInt(1),c.getString(2),
                        c.getString(3),c.getInt(4),
                        c.getString(6),c.getString(5),
                        c.getString(7), c.getString(8), c.getString(9)));
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allUsers.size()>0?allUsers:null;

    }

    //get projects
    public List<PrjectList> getAllProjects() {
        List<PrjectList> allProj = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(DbHelper.TABLE_PROJECTS, null, null, null, null, null, null);

        String selectQuery = "SELECT  * FROM " + DbHelper.TABLE_PROJECTS;
        //SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor      = db.rawQuery(selectQuery, null);
        int cursorco = cursor.getCount();
        Log.d("Count", cursorco+"");

        if (c.moveToFirst()) {


            do {
                PrjectList prjectList = new PrjectList();
                prjectList.setId(c.getInt(c.getColumnIndex(DbHelper.KEY_PROJ_ID)));
                prjectList.setProjectLocation(c.getString(c.getColumnIndex(DbHelper.KEY_PROJ_LOC)));
                prjectList.setProjectName(c.getString(c.getColumnIndex(DbHelper.KEY_PROJ_NAME)));
                prjectList.setProjectDefination(c.getString(c.getColumnIndex(DbHelper.KEY_PROJ_DEF)));
                allProj.add(prjectList);
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allProj.size()>0?allProj:null;
    }
    public List<offlineProjObj> getOfflineImage() {

        List<offlineProjObj> allProj = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(DbHelper.TABLE_OFF_IMAGES, null, null, null, null, null, null);

        if (c.moveToFirst()) {
            do {
                allProj.add(new offlineProjObj(c.getInt(c.getColumnIndex(DbHelper.KEY_ID)),c.getInt(c.getColumnIndex(DbHelper.KEY_PROJ_ID)),
                        c.getInt(c.getColumnIndex(DbHelper.KEY_DIV_ID)),
                        c.getBlob(c.getColumnIndex(DbHelper.KEY_IMAGE)),
                        c.getString(c.getColumnIndex(DbHelper.KEY_NAME))));
            } while (c.moveToNext());
        }

        c.close();
        dbHelper.close();

        return allProj.size()>0?allProj:null;
    }

    public void DeleteImage(int imageByte){
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(DbHelper.TABLE_OFF_IMAGES, DbHelper.KEY_ID+" = "+imageByte,null);
        database.close();

    }
// ---- add offline image ------
public void addOfflineImage( int projID,int divId,  byte[] image,String name) throws SQLiteException {
    DbHelper dbHelper = new DbHelper(mContext);
    SQLiteDatabase database = dbHelper.getWritableDatabase();
    ContentValues cv = new  ContentValues();
    cv.put(DbHelper.KEY_NAME,    name);
    cv.put(DbHelper.KEY_PROJ_ID,   projID);
    cv.put(DbHelper.KEY_IMAGE,   image);
    cv.put(DbHelper.KEY_DIV_ID,   divId);
    database.insert( DbHelper.TABLE_OFF_IMAGES, null, cv );
}

    public void clearDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
       // db.delete(DbHelper.DB_TABLE_NAME, null, null);
        db.delete(DbHelper.TABLE_USERS, null, null);
        dbHelper.close();
    }
    public void clearDivUserDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
       db.delete(DbHelper.DB_TABLE_NAME, null, null);
        //db.delete(DbHelper.TABLE_USERS, null, null);
        dbHelper.close();
    }
    public void clearAllProjDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
       db.delete(DbHelper.TABLE_PROJECTS, null, null);
        //db.delete(DbHelper.TABLE_USERS, null, null);
        dbHelper.close();
    }

    public void clearLoginUserDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
       db.delete(DbHelper.TABLE_LOGIN_USER, null, null);
        //db.delete(DbHelper.TABLE_USERS, null, null);
        dbHelper.close();
    }

    public ArrayList<QBUser> getUsersByIds(List<Integer> usersIds) {
        ArrayList<QBUser> qbUsers = new ArrayList<>();

        for (Integer userId : usersIds) {
            if (getUserById(userId) != null) {
                qbUsers.add(getUserById(userId));
            }
        }

        return qbUsers;
    }

    public void clearOfflineUserDB() {
        DbHelper dbHelper = new DbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DbHelper.TABLE_PROJECTS, null, null);
        //db.delete(DbHelper.TABLE_USERS, null, null);
        dbHelper.close();
    }
}

