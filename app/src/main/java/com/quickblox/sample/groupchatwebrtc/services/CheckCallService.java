package com.quickblox.sample.groupchatwebrtc.services;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.quickblox.sample.groupchatwebrtc.utils.SharedPrefsHelper;

public class CheckCallService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
       // Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.e("Service", "Running service");
        if(SharedPrefsHelper.getInstance().hasQbUser()) {
            CallService.start(this,SharedPrefsHelper.getInstance().getQbUser(),null);

            /*CallService.start(this,SharedPrefsHelper.getInstance().getQbUser());*/
        }
        //Toast.makeText(this, " MyService Started", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

}