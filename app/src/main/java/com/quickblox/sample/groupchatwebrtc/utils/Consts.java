package com.quickblox.sample.groupchatwebrtc.utils;

import android.Manifest;

/**
 * QuickBlox team
 */
public interface Consts {

    String QB_CONFIG_FILE_NAME = "qb_config.json";
  //  String BASE_URL = "http://192.168.12.154:9090";
    //String BASE_URL = "http://183.82.125.183:9090";
    /*UAT*/
     String BASE_URL = "http://164.100.141.154:8080";
    /*Production*/
  // String BASE_URL = "http://164.100.141.155:8080";
    //String BASE_URL = "http://ophwc.org";
  /**production*/
 // String BASE_URL = "http://115.248.252.66:9090";

    String DEFAULT_USER_PASSWORD = "x6Bt0VDy5";
    String CHAT_ROOM_NAME = "OPHWCROOM";



    int CALL_ACTIVITY_CLOSE = 1000;

    int ERR_LOGIN_ALREADY_TAKEN_HTTP_STATUS = 422;
    int ERR_MSG_DELETING_HTTP_STATUS = 401;

    //CALL ACTIVITY CLOSE REASONS
    int CALL_ACTIVITY_CLOSE_WIFI_DISABLED = 1001;
    String WIFI_DISABLED = "wifi_disabled";

    String OPPONENTS = "opponents";
    String CONFERENCE_TYPE = "conference_type";
    String EXTRA_TAG = "currentRoomName";
    int MAX_OPPONENTS_COUNT = 6;

    String PREF_LOGIN_AS = "logged_in_as";
    String PREF_LOGIN_USER = "logged_in_user";
    String PREF_LOGIN_USERID = "logged_in_userid";
    String PREF_QBUSER_LOGIN = "qb_user_login";

    String PREF_CURREN_ROOM_NAME = "current_room_name";
    String PREF_CURRENT_TOKEN = "current_token";
    String PREF_TOKEN_EXPIRATION_DATE = "token_expiration_date";

    String EXTRA_QB_USER = "qb_user";

    String EXTRA_USER_ID = "user_id";
    String EXTRA_USER_LOGIN = "user_login";
    String EXTRA_USER_PASSWORD = "user_password";
    String EXTRA_PENDING_INTENT = "pending_Intent";

    String EXTRA_CONTEXT = "context";
    String EXTRA_OPPONENTS_LIST = "opponents_list";
    String EXTRA_CONFERENCE_TYPE = "conference_type";
    String EXTRA_IS_INCOMING_CALL = "conversation_reason";

    String EXTRA_LOGIN_RESULT = "login_result";
    String EXTRA_LOGIN_ERROR_MESSAGE = "login_error_message";
    int EXTRA_LOGIN_RESULT_CODE = 1002;

    String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
    String[] LOCATION_PERMISSIONS = {Manifest.permission_group.LOCATION, Manifest.permission_group.STORAGE};

    String EXTRA_COMMAND_TO_SERVICE = "command_for_service";
    int COMMAND_NOT_FOUND = 0;
    int COMMAND_LOGIN = 1;
    int COMMAND_LOGOUT = 2;
    String EXTRA_IS_STARTED_FOR_CALL = "isRunForCall";
    String ALREADY_LOGGED_IN = "You have already logged in chat";

    enum StartConversationReason {
        INCOME_CALL_FOR_ACCEPTION,
        OUTCOME_CALL_MADE
    }
}
