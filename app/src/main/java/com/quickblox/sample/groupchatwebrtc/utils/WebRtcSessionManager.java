package com.quickblox.sample.groupchatwebrtc.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.quickblox.sample.groupchatwebrtc.activities.OpponentsActivity;
import com.quickblox.sample.groupchatwebrtc.activities.SplashActivity;
import com.quickblox.videochat.webrtc.QBRTCSession;
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacksImpl;

/**
 * Created by tereha on 16.05.16.
 */
public class WebRtcSessionManager extends QBRTCClientSessionCallbacksImpl {
    private static final String TAG = WebRtcSessionManager.class.getSimpleName();

    private static WebRtcSessionManager instance;
    private Context context;

    private static QBRTCSession currentSession;

    private WebRtcSessionManager(Context context) {
        this.context = context;
    }

    public static WebRtcSessionManager getInstance(Context context){
        if (instance == null){
            instance = new WebRtcSessionManager(context);
        }

        return instance;
    }

    public QBRTCSession getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(QBRTCSession qbCurrentSession) {
        currentSession = qbCurrentSession;
    }

    @Override
    public void onReceiveNewSession(QBRTCSession session) {
        Log.d("OFFCALL", "onReceiveNewSession to WebRtcSessionManager");

        if (currentSession == null){
            setCurrentSession(session);
            SharedPrefsHelper sharedPrefsHelper = SharedPrefsHelper.getInstance();
            //if((sharedPrefsHelper).get(Consts.PREF_LOGIN_AS,-1)==0)
            OpponentsActivity.start(context, true);
            //else
               // context.startActivity(new Intent(context,SplashActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK));
        }
    }

    @Override
    public void onSessionClosed(QBRTCSession session) {
        Log.d(TAG, "onSessionClosed WebRtcSessionManager");

        if (session.equals(getCurrentSession())){
            setCurrentSession(null);
        }
    }
}
